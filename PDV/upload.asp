﻿<!--#include file="global.asp"-->
<!--#include file="upload.lib.asp"-->
<%
if request.QueryString("ex") = "s" then

	endereco =  Server.MapPath(".")
	endereco = replace(endereco, "\PDV", "\Upload\"&request.QueryString("nome"))
	Set objFSO = Server.CreateObject("Scripting.FileSystemObject") 
	objFSO.DeleteFile(""&endereco&"") 
	Set objFSO = Nothing 

	if request.QueryString("dest") = "" then
		response.Redirect("principal.asp?lg=y&cx=a")
	else
		if request.QueryString("dest") = "fclj" then
			response.Redirect("lj_fechamento_conc.asp?lg=y&cx="&response.Write(CxStatus())&"&datacx="&ConvData(date())&"")
		end if
	end if

end if
%>

<html>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Sys PDV">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Thiago Moskito e Jaime Agostinho">

    <title>Diferença Devedora</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <style type="text/css">
   body{margin-top:5px;}
   .glyphicon { margin-right:6px; }
   .card-block a{text-decoration: none;}
   .collapse .card-block.disabled a{color:#c5c5c5;}
   .collapse .card-block.disabled, .collapse .card-block.disabled span{color:#c5c5c5;}
    </style>

<body>


<%
if request("up") = "s" then
'Response.Charset = "utf-8"
Dim Form : Set Form = New ASPForm
Server.ScriptTimeout = 1440 ' Limite de 24 minutos de execução de código, o upload deve acontecer dentro deste tempo ou então ocorre erro de limite de tempo.
Const MaxFileSize = 1200000 ' Limite de 1,2 Mb de arquivo.
If Form.State = 0 Then
 For each Field in Form.Files.Items
 ' # Field.Filename : Nome do Arquivo que chegou.
 ' # Field.ByteArray : Dados binários do arquivo, útil para subir em blobstore (MySQL).
strnome = request.QueryString("nome")& "." & right(Field.FileName, 3)
endereco =  Server.MapPath(".")
endereco = replace(endereco, "\PDV", "\Upload")

' Field.SaveAs Server.MapPath(".") & "\" & strnome
 Field.SaveAs (endereco & "\" & strnome )
 Next
End If
	if request.QueryString("dest") = "" then
		response.Redirect("principal.asp?lg=y&cx=a")
	else
		if request.QueryString("dest") = "fclj" then
			response.Redirect("lj_fechamento_conc.asp?lg=y&cx="&response.Write(CxStatus())&"&datacx="&ConvData(date())&"")
		end if
	end if
End If
%>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
              <div class="card-header" role="tab" id="headingOne">
                <h5 class="mb-0">
                    <span class="glyphicon glyphicon-cloud"></span> Upload de Arquivo! 
                </h5>
              </div>
             <form method="post" enctype="multipart/form-data" action="upload.asp?lg=<%=request.QueryString("lg")%>&cx=<%=request.QueryString("cx")%>&up=s&nome=<%=request.QueryString("nome")%>&dest=<%=request.QueryString("dest")%>">
              <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
                        <div class="row">
                            <div class="col-xs-12  btn-group-sm">
                                <input type="hidden" name="origem" id="origem" value="daonde vim" >
                                <input type="file" name="arquivo" />
                            </div>
                        </div>
                    </div>
	                <div class="card-block">
                        <div class="row">
                            <div class="col-xs-6  btn-group-sm"><a class="btn btn-primary btn-lg btn-block" href="principal.asp?lg=<%=request.QueryString("lg")%>&cx=<%=request.QueryString("cx")%>" role="button">Cancelar</a></div>
                            <div class="col-xs-6  btn-group-sm"><input class="btn btn-primary btn-lg btn-block" type="submit"  value="Enviar Arquivo" /></div>
					</div>
				</div>
			 </div>
             </form>
		    </div>
	      </div>
        </div>
      </div>
    </div>



</body>
</html>