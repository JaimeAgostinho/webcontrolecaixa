﻿<!--#include file="global.asp"-->
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Sys PDV">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Thiago Moskito e Jaime Agostinho">

    <title>Controle de Caixa</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <style type="text/css">
   body{margin-top:10px;}
   .glyphicon { margin-right:6px; }
   .card-block a{text-decoration: none;}
   .collapse .card-block:hover{background-color: #f0f0f0;}
   .collapse .card-block.disabled a{color:#c5c5c5;}
   .collapse .card-block.disabled, .collapse .card-block.disabled span{color:#c5c5c5;}
    </style>

<SCRIPT LANGUAGE="JavaScript">
	redirTime = "10000";// 25000 equivale a 25 segundos
	redirURL = "rel_movimento.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>&dt=<%=mxdtcx%>&blq=<%=request("blq")%>";
	function redirTimer() { self.setTimeout("self.location.href = redirURL;",redirTime); }
</script>
  </head>
<%
response.Cookies("InfoCx")("calc") = "0.00"
DtCaixa = funcaoPesquisa("SELECT if(date(data) is NULL, 0, date(data)) as Resposta FROM financeiro f where date(data) <= date(now()) and std = 'A' and local='"&request.Cookies("Info")("chavej")&"'")
nVCaixa = funcaoPesquisa("SELECT if(date(data) is NULL, 0, date(data)) as Resposta FROM financeiro f where date(data) = date(now()) and local='"&request.Cookies("Info")("chavej")&"'")
mxdtcx = funcaoPesquisa("SELECT if(date(max(data)) is NULL, 0, date(max(data))) as Resposta FROM financeiro f where local='"&request.Cookies("Info")("chavej")&"'")
mxdtcx = replace(mxdtcx, "-", "")
FuncChaveJ = funcaoPesquisa("SELECT funcao as Resposta FROM usuario_operador where chavej = '"&request.Cookies("Info")("chavej")&"'")
%>
<% if DtCaixa <> "" then%>
	<BODY onLoad="redirTimer()">
<%end if%>

<%	if 	request.Cookies("Info")("datacx") <> ConvData(date()) and request.Cookies("Info")("datacx") <> "00000000" then %>
<div class="alert alert-danger" align="center">
	<h4>Terminal nao foi fechado!</h4>
</div>

<% 	end if %>
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-md-3">
          <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
              <div class="card-header" role="tab" id="headingOne">
                <h5 class="mb-0">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <span class="glyphicon glyphicon-cloud"></span> Controle de Caixa - ACB
                  </a>
                </h5>
              </div>
              <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
                <% if nVCaixa = ""  and DtCaixa = "" then%>
                    <div class="card-block">
                     <span class="glyphicon text-primary glyphicon-tags"></span><a href="cx_abrir.asp?lg=y&cx=f"> Abrir de Caixa</a>
                    </div>
				<% end if %>

                <% if nVCaixa <> ""  or DtCaixa <> "" then%>
                <div class="card-block disabled">
                  <span class="glyphicon glyphicon-tags"></span>Abrir de Caixa
                </div>
				<% end if %>

                <div class="card-block">
                  <span class="glyphicon glyphicon-th-list text-success"></span><a href="rel_movimento.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>"> Relatorio de Caixa</a>
                </div>
                
                <% if DtCaixa <> "" then%>
                <div class="card-block">
                  <span class="glyphicon glyphicon-usd text-info"></span><a href="retirada_caixa.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>"> Retirada de Caixa</a>
                </div>
                <% Else %>
                <div class="card-block disabled">
                  <span class="glyphicon glyphicon-usd"></span> Retirada de Caixa
                </div>
                <% end if%>

                <% if DtCaixa <> "" then%>
                <div class="card-block">
                  <span class="glyphicon glyphicon-minus text-info"></span><a href="dif_devedora.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>"> Larçar Diferença Devedora</a>
                </div>
                <% Else %>
                <div class="card-block disabled">
                  <span class="glyphicon glyphicon-minus"></span> Larçar Diferença Devedora
                </div>
                <% end if%>

                <% if DtCaixa <> "" then%>
                <div class="card-block">
                  <span class="glyphicon glyphicon-plus text-info"></span><a href="dif_credora.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>"> Larçar Diferença Credora</a>
                </div>
                <% Else %>
                <div class="card-block disabled">
                  <span class="glyphicon glyphicon-plus"></span> Larçar Diferença Credora
                </div>
                <% end if%>

                <% if DtCaixa <> "" then%>
                <div class="card-block">
                  <span class="glyphicon glyphicon-saved text-success"></span><a href="cx_fechamento.asp?lg=&amp;cx="> Fechar de Caixa</a>
                </div>
                <% end if%>

                <% if DtCaixa = "" then%>
                <div class="card-block disabled">
                  <span class="glyphicon glyphicon-saved "></span> Fechar de Caixa 
                </div>
                <% end if%>
				<% 
                if FuncChaveJ = "23" or FuncChaveJ = "25"  then
                %>
                    <div class="card-block">
                      <span class="glyphicon glyphicon-list-alt"></span><a href="rel_saldoloja.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>">Relatório de Saldo da Loja</a>
                    </div>
				<%
                end if
                %>
              </div>
            </div>

            <div class="card">
              <div class="card-header" role="tab" id="headingTwo">
                <h5 class="mb-0">
                    <span class="glyphicon glyphicon-book"></span> <a href="lj_fechamento.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>">Fechar Caixa da Loja - <%=request.Cookies("Info")("lojabb")%></a>
                </h5>
              </div>
              <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="card-block">
                  <span class="glyphicon glyphicon-list-alt"></span><a href="rel_saldoloja.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>">Relatório de Saldo da Loja</a>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <span class="glyphicon glyphicon-user"></span> Usuário
                  </a>
                </h5>
              </div>
              <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="card-block">
                  <span class="glyphicon  glyphicon-cog"></span><a href="trocar_senha.asp?chavej=<%=request.Cookies("Info")("chavej")%>&troc=s&lg=y">Trocar Senha</a>
                </div>
                <div class="card-block">
                  <span class="glyphicon text-success glyphicon-off"></span><a href="loguin.asp?chavej=<%=request.Cookies("Info")("chavej")%>&convenio=<%=request.Cookies("Info")("convenio")%>&lg=n&lojabb=<%=request.Cookies("Info")("lojabb")%>">Sair</a>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header" role="tab" id="heading4">
                <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                    <span class="glyphicon glyphicon-file"></span> Gerencial
                  </a>
                </h5>
              </div>
              <div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="heading4">
                <div class="card-block">
                  <span class="glyphicon text-danger glyphicon-warning-sign"></span><a href="#">Informações de Sistema</a>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>
    
    <!-- jQuery first, then Tether, then Bootstrap JS. -->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
-->  </body>
</html>