﻿<!--#include file="global.asp"-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Sys PDV">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Thiago Moskito e Jaime Agostinho">

    <title>Diferença Devedora</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <style type="text/css">
   body{margin-top:5px;}
   .glyphicon { margin-right:6px; }
   .card-block a{text-decoration: none;}
   .collapse .card-block.disabled a{color:#c5c5c5;}
   .collapse .card-block.disabled, .collapse .card-block.disabled span{color:#c5c5c5;}
    </style>

<script type="text/javascript">
	function enviar(){
		if(	document.getElementById("valorAb").value != "" && document.getElementById("senhaGR").value != "" && document.getElementById("obs").value != "" )
		{
				document.getElementById("bt").disabled=true;
				document.getElementById("acesso").submit()
		} else {
			alert('Ha campos nao informados!')	
		}
	}

function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
	if(document.getElementById("valorAb").readOnly == false){
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
	}
    return false;
}
</script>
<script type="text/javascript">
	redirTime = "300000";// 300000 equivale a 5 Minutos
	redirURL = "rel_movimento.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>&blq=<%=request("blq")%>&v=<%=v%>&Pesq=s";
	function redirTimer() { self.setTimeout("self.location.href = redirURL;",redirTime); }
</script>

</head>
<BODY onLoad="redirTimer()">
<%
if request.QueryString("log") = "s" then

	BollOperador = funcaoPesquisa("SELECT if(count(codigo) > 0, 'true', 'false') as resposta FROM usuario_operador where chavej='"&request.Cookies("Info")("chavej")&"' and senha='"&Request("senhaGR")&"'")
		
		if BollOperador = "true" then

    if request.Form("motivo") = "Erro Operacional" then
      Locc = request.Cookies("Info")("chavej")
    else
      Locc = "Loja"
    end if
					
			sql = "INSERT INTO dif_caixa ( local, lojabb, data, datacx, tipo, motivo, obs, valor, status, origem) VALUES ('"&Locc&"', '"&request.Cookies("Info")("lojabb")&"', CURRENT_TIMESTAMP, date(CURRENT_TIMESTAMP), 'Devedor', '"&request.Form("motivo")&"', '"&request("obs")&"', '"&BdValor(request.Form("valorAb"))&"', 'Lancado', 'Loja')"
			MysqlInsert(sql)

			response.Redirect("principal.asp?lg="&request("lg")&"&cx="&response.Write(CxStatus())&" ")
		else
			response.Write("<script>alert('Senha do Gerente Incorreta!')</script>")
		end if
end if
%>

    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
              <div class="card-header" role="tab" id="headingOne">
                <h5 class="mb-0">
                    <span class="glyphicon glyphicon-cloud"></span> Relatório 
                </h5>
              </div>
              <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
                    <div class="row">
                        <div class="col-xs-2">Data:</div>	
                        <div class="col-xs-5">Motivo:</div>	
                        <div class="col-xs-3" style="text-align: left">Valor:</div>	
                        <div class="col-xs-2"></div>	
					</div>
                    <div class="row">
	                    <div class="col-xs-12"></hr></div>	
					</div>
                    <div class="row">
					<%
                    Set objconnA = Server.CreateObject("ADODB.Connection")
                    objconnA.open = StringConexaoBDCons()
                    instrucao_sqlA = "SELECT * FROM dif_caixa where lojabb='"&request.Cookies("Info")("lojabb")&"' and (local='Loja' or origem='Loja') and tipo='Devedor' and status = 'Lancado'"
                    'response.Write(instrucao_sqlA)
					set dadosA = objconnA.execute(instrucao_sqlA)
                    if dadosA.EOF then
                    else
                    dadosA.MoveFirst
                    While Not dadosA.EOF
                    %>
                        <div class="col-xs-2" style="font-size: 12px; white-space: nowrap"><%=mid(dadosA("datacx"),1,5)%></div>	
                        <div class="col-xs-5" style="font-size: 12px; white-space: nowrap"><%=dadosA("motivo")%></div>	
                        <div class="col-xs-3" style="font-size: 12px; white-space: nowrap; text-align: left"><%=formatcurrency(dadosA("valor"),2)%></div>	
                        <div class="col-xs-2">
						<%
                        Set ScriptObject = Server.CreateObject("Scripting.FileSystemObject")
						endereco =  Server.MapPath(".")
						endereco = replace(endereco, "\PDV", "\Upload\difdev-"&dadosA("codigo")&".pdf")
                        if ScriptObject.FileExists(endereco) then
						std = ArquivoFAF(dadosA("codigo"), dadosA("lojabb"), "s")
						%>
						    <a href="../<%=Response.Write("Upload\difdev-"&dadosA("codigo")&".pdf")%>" target="new"><img src="../imagem/pdf.png" width="14" height="14"> </a> 
						<%
						else
						std = ArquivoFAF(dadosA("codigo"), dadosA("lojabb"), "n")
						%>
                            <a href="upload.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>&nome=difdev-<%=dadosA("codigo")%>"><img src="../imagem/bot_playl_salvar_p.gif" alt="Anexar Arquivo PDF!" width="14" height="14"> </a> 
						<%end if%>
                        
                        </div>	
                    <%
                    dadosA.MoveNext
                    Wend
                    end if
                    %>
					</div>
				</div>
			 </div>
		    </div>
	      </div>
        </div>
      </div>
    </div>
<hr/>

<form id="acesso" name="acesso" method="post" action="dif_devedoraLoja.asp?log=s&cx=<%=request("cx")%>&lg=<%=request("lg")%>">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
              <div class="card-header" role="tab" id="headingOne">
                <h5 class="mb-0">
                    <span class="glyphicon glyphicon-cloud"></span> Diferença Devedora - Caixa
                </h5>
              </div>
              <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
                    <div class="row">
					  <div class="col-xs-6">Loja:</div>
                      <div class="col-xs-6">Data:</div>
					</div>
                    <div class="row">
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" placeholder="<%=request.Cookies("Info")("lojabb")%>" id="loja" disabled aria-describedby="sizing-addon2"></div>
                      <div class="col-xs-6 input-group-sm"><input type="email" class="form-control input-sm" id="datacx" value="<%=response.Write(mid(request.Cookies("Info")("datacx"), 7,2)&"/"&mid(request.Cookies("Info")("datacx"), 5,2)&"/"&mid(request.Cookies("Info")("datacx"), 1,4))%>" disabled></div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 ">Motivo:</div>
                      <div class="col-md-8  input-group-sm">
                        <select class="form-control" id="motivo" name="motivo">
                          <option>Erro Operacional</option>
                          <option>Sinistro</option>
                        </select>                      
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">Observação:</div>
                      <div class="col-md-8  input-group-sm"><textarea class="form-control" rows="3" id="obs"  name="obs"  placeholder="Informe com riqueza de telhas a descrição do ocorrido!"></textarea></div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">Valor:</div>
                      <div class="col-md-8  input-group-sm"><input type="text" class="form-control input-sm" id="valorAb" name="valorAb"  value="" onKeyPress="return(MascaraMoeda(this,'.',',',event))" /></div>
                    </div>
                    <div class="row">
                      <div class="col-xs-6">Senha Gerente:</div>
					</div>
                    <div class="row">
                      <div class="col-xs-6"><input type="password" class="form-control input-sm" value="" id="senhaGR" name="senhaGR" /></div>
					</div>
                    <div class="row">
                      <div class="col-md-4"> </br></div>
                      <div class="col-xs-6  btn-group-sm"><a class="btn btn-primary btn-lg btn-block" href="principal.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>" role="button">Cancelar</a></div>
                      <div class="col-xs-6  btn-group-sm"><input class="btn btn-primary btn-lg btn-block" type="button" name="bt" id="bt" onClick="enviar()" value="Gravar" /></div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-12"></div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</form>



  </body>
</html>