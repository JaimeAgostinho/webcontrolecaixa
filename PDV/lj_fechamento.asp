﻿<!--#include file="global.asp"-->
<%
LimitCofreAdm = funcaoPesquisa("SELECT limite_cofre_adm as Resposta FROM cad_lojabb where lojabb= '"&request.Cookies("Info")("lojabb")&"'")
TemCofreAdm = funcaoPesquisa("SELECT cofre_adm as Resposta FROM cad_lojabb where lojabb= '"&request.Cookies("Info")("lojabb")&"'")
if LimitCofreAdm <> "" then
	LimitCofreAdm = formatnumber(LimitCofreAdm,2)
else
	LimitCofreAdm = "0,00"
end if

FcLoja = funcaoPesquisa("SELECT std_cx as Resposta FROM financeiro_lj where datacx='"&ConvData(date())&"' and lojabb='"&request.Cookies("Info")("lojabb")&"'")
if FcLoja = "Anexar" or FcLoja = "Fechado" then
	response.Redirect("lj_fechamento_conc.asp?lg=y&cx="&response.Write(CxStatus())&"&datacx="&ConvData(date())&"&std="&FcLoja&"")
end if
if FcLoja = "Conferido" then
	response.Redirect("lj_fechamento_conc.asp?lg=y&cx="&response.Write(CxStatus())&"&datacx="&ConvData(date())&"&std="&FcLoja&"")
end if

%>

<html lang="pt-br">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Sys PDV">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Jaime Agostinho">

    <title></title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <style type="text/css">
   body{margin-top:5px;}
   .glyphicon { margin-right:6px; }
   .card-block a{text-decoration: none;}
   .collapse .card-block.disabled a{color:#c5c5c5;}
   .collapse .card-block.disabled, .collapse .card-block.disabled span{color:#c5c5c5;}
    </style>
<script type="text/javascript">
	function enviar(){
		if(	document.getElementById("senhaGR").value != ""  )
		{
				document.getElementById("bt").disabled=true;
				document.getElementById("acesso").submit()
		} else {
			alert('Ha campos nao informados!')	
		}
	}

function MascaraMoedaB(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
	var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
	Func_Error();
    return false;
}


function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
	var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
	Func_podersuper();
    return false;
}

function init(){
	formRespGer(document.acesso.podersuper,'.',',',event);
}

function formRespGer(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
	objTextBox = document.acesso.podersuper;
	document.acesso.podersuper.value = document.acesso.podersuper.value.replace(".",",")
	var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
}


function Func_podersuper()
{
	var s1 = document.getElementById("saldo_in").value.replace(".", "");
	s1 = parseFloat(s1.replace(",","."));

	var s2 = document.getElementById("rec").value.replace(".", "");
	s2 = parseFloat(s2.replace(",","."));

	var s3 = document.getElementById("pag").value.replace(".", "");
	s3 = parseFloat(s3.replace(",","."));
	
	var s4 = document.getElementById("alivio").value.replace(".", "");
	s4 = parseFloat(s4.replace(",","."));
	
	var s5 = document.getElementById("difcx").value.replace(".", "");
	s5 = parseFloat(s5.replace(",","."));

	var s6 = document.getElementById("gtv").value.replace(".", "");
	s6 = parseFloat(s6.replace(",","."));
	
	var s7 = document.getElementById("difcxMaior").value.replace(".", "");
	s7 = parseFloat(s7.replace(",","."));
	
	
	
	var calc = ((((((s1+s2)+s3)-s4)-s5)-s6)+s7);
	calc = calc.toFixed(2);
	
	document.acesso.podersuper.value = converteFloatMoeda(calc);
	Func_Error()
}


function Func_Error()
{
	var x1 = document.getElementById("compusafe").value.replace(".", "");
	x1 = parseFloat(x1.replace(",","."));

	var x2 = document.getElementById("cofreadm").value.replace(".", "");
	x2 = parseFloat(x2.replace(",","."));

	var x3 = document.getElementById("bocalobo").value.replace(".", "");
	x3 = parseFloat(x3.replace(",","."));

	//var x4 = document.getElementById("saldopdv").value.replace(".", "");
	//x4 = parseFloat(x4.replace(",","."));

	var x5 = document.getElementById("podersuper").value.replace(".", "");
	x5 = parseFloat(x5.replace(",","."));

	//SldCofAdm
	BDCofreAdm = <%=BdValor(LimitCofreAdm)%>;

<% if LimitCofreAdm <> "0,00" then%>
	if(x2 > BDCofreAdm){
	document.getElementById("SldCofAdm").innerHTML = 'Cofre Adm deve ser menor de <%=formatcurrency(LimitCofreAdm,2)%>!';
	document.getElementById("bt").disabled=true;
	}
	if(x2 <= BDCofreAdm){
	document.getElementById("SldCofAdm").innerHTML = '';
	document.getElementById("bt").disabled=false;
	}
<% end if %>
	
	//var Xcalc = ((((x1+x2)+x3)+x4)-x5);
	var Xcalc = ((((x1+x2)+x3))-x5);
	Xcalc = Xcalc.toFixed(2);
	document.getElementById("error").innerHTML = Xcalc;
   	document.getElementById("bt").disabled=true;
	if(Xcalc == '0.00'){
		document.getElementById("error").innerHTML = '';
    	document.getElementById("bt").disabled=false;
	}
	if(Xcalc == '-0.00'){
		document.getElementById("error").innerHTML = '';
    	document.getElementById("bt").disabled=false;
	}
}


 function converteFloatMoeda(valor){
      var inteiro = null, decimal = null, c = null, j = null;
      var aux = new Array();
      valor = ""+valor;
      c = valor.indexOf(".",0);
      //encontrou o ponto na string
      if(c > 0){
         //separa as partes em inteiro e decimal
         inteiro = valor.substring(0,c);
         decimal = valor.substring(c+1,valor.length);
      }else{
         inteiro = valor;
      }
      
      //pega a parte inteiro de 3 em 3 partes
      for (j = inteiro.length, c = 0; j > 0; j-=3, c++){
         aux[c]=inteiro.substring(j-3,j);
      }
      
      //percorre a string acrescentando os pontos
      inteiro = "";
      for(c = aux.length-1; c >= 0; c--){
         inteiro += aux[c]+'.';
      }
      //retirando o ultimo ponto e finalizando a parte inteiro
      
      inteiro = inteiro.substring(0,inteiro.length-1);
      
      decimal = parseInt(decimal);
      if(isNaN(decimal)){
         decimal = "00";
      }else{
         decimal = ""+decimal;
//         if(decimal.length === 1){
//            decimal = decimal+"0";
         if(decimal < 10){
            decimal = "0"+decimal;


         }
      }
      valor = inteiro+","+decimal;
      return valor;
   }

</script>


<%
if request.QueryString("log") = "s" then

senhaGer = "-"&funcaoPesquisa("SELECT senha as Resposta FROM usuario_operador where lojabb='"&request.Cookies("Info")("lojabb")&"' and funcao='23' limit 0,1")
senhaGer = senhaGer&"-"&funcaoPesquisa("SELECT senha as Resposta FROM usuario_operador where lojabb='"&request.Cookies("Info")("lojabb")&"' and funcao='23' limit 1,1")
senhaGer = senhaGer&"-"&funcaoPesquisa("SELECT senha as Resposta FROM usuario_operador where lojabb='"&request.Cookies("Info")("lojabb")&"' and funcao='23' limit 2,1")
senhaGer = senhaGer&"-"&funcaoPesquisa("SELECT senha as Resposta FROM usuario_operador where lojabb='"&request.Cookies("Info")("lojabb")&"' and funcao='25' limit 0,1")
senhaGer = senhaGer&"-"&funcaoPesquisa("SELECT senha as Resposta FROM usuario_operador where lojabb='"&request.Cookies("Info")("lojabb")&"' and funcao='25' limit 1,1")
senhaGer = senhaGer&"-"&funcaoPesquisa("SELECT senha as Resposta FROM usuario_operador where lojabb='"&request.Cookies("Info")("lojabb")&"' and funcao='25' limit 2,1")
senhaGer = senhaGer&"-"&funcaoPesquisa("SELECT senha as Resposta FROM usuario_operador where lojabb='"&request.Cookies("Info")("lojabb")&"' and funcao='25' limit 3,1")
intVal = InStr(senhaGer, request.form("senhaGR"))	

	if intVal > 1 then
					
	'sql = "INSERT INTO financeiro_lj ( std_cx, lojabb, gerente, data, datacx, saldo_in, rec, pag, alivio, saldo_parcial, tot_gtv, obs_gtv, tot_difcxmenor, obs_difcxmenor, saldo_loja, saldo_pdv, saldo_comp, saldo_cofreadm, saldo_bocadelobo, obs) VALUES ('Anexar', '"&request.Cookies("Info")("lojabb")&"', '"&request.Cookies("Info")("NomeOperadora")&"', CURRENT_TIMESTAMP, '"&ConvData(date())&"', '"&BdValor(request.form("saldo_in"))&"', '"&BdValor(request.form("rec"))&"', '"&BdValor(request.form("pag"))&"', '"&BdValor(request.form("alivio"))&"', '"&BdValor(request.form("saldobb"))&"', '"&BdValor(request.form("gtv"))&"', 'obs_gtv', '"&BdValor(request.form("difcx"))&"', 'obs_difcxmenor', '"&BdValor(request.form("podersuper"))&"', '"&BdValor(request.form("saldopdv"))&"', '"&BdValor(request.form("compusafe"))&"', '"&BdValor(request.form("cofreadm"))&"', '"&BdValor(request.form("bocalobo"))&"', '"&request.form("obs")&"')"
	sql = "INSERT INTO financeiro_lj ( std_cx, lojabb, gerente, data, datacx, saldo_in, rec, pag, alivio, saldo_parcial, tot_gtv, obs_gtv, tot_difcxmenor, obs_difcxmenor, saldo_loja, saldo_pdv, saldo_comp, saldo_cofreadm, saldo_bocadelobo, obs, tot_difcxmaior) VALUES ('Anexar', '"&request.Cookies("Info")("lojabb")&"', '"&request.Cookies("Info")("NomeOperadora")&"', CURRENT_TIMESTAMP, '"&ConvData(date())&"', '"&BdValor(request.form("saldo_in"))&"', '"&BdValor(request.form("rec"))&"', '"&BdValor(request.form("pag"))&"', '"&BdValor(request.form("alivio"))&"', '"&BdValor(request.form("saldobb"))&"', '"&BdValor(request.form("gtv"))&"', 'obs_gtv', '"&BdValor(request.form("difcx"))&"', 'obs_difcxmenor', '"&BdValor(request.form("podersuper"))&"', '0.00', '"&BdValor(request.form("compusafe"))&"', '"&BdValor(request.form("cofreadm"))&"', '"&BdValor(request.form("bocalobo"))&"', '"&request.form("obs")&"', '"&BdValor(request.form("difcxMaior"))&"')"
	MysqlInsert(sql)

	response.Redirect("lj_fechamento_conc.asp?lg=y&cx="&response.Write(CxStatus())&"&datacx="&ConvData(date())&"")

	else
		response.Write("<script>alert('Senha do Gerente Incorreta!')</script>")
	end if
end if
%>
<script type="text/javascript">
	redirTime = "300000";// 300000 equivale a 5 Minutos
	redirURL = "rel_movimento.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>&blq=<%=request("blq")%>&v=<%=v%>&Pesq=s";
	function redirTimer() { self.setTimeout("self.location.href = redirURL;",redirTime); }
</script>

</head>

<BODY onLoad="redirTimer()">
<%
'"&request.Cookies("Info")("datacx")&"
Set objconnAC = Server.CreateObject("ADODB.Connection")
objconnAC.open = StringConexaoBDCons()
'instrucao_sqlAC = "SELECT f.*, (SELECT if(sum(valor) > 0, sum(valor), 0) as Resp FROM dif_caixa where lojabb=f.lojabb and tipo='Devedor' and status='Lancado') as difcx, @maxcd:=(select max(codigo) from tot_posicao where lojabb=f.lojabb and posicao=f.datacx) as CdPos, (select alivio from tot_posicao where codigo=@maxcd) as alivio, (select if(sum(saldo_fn) > 0, sum(saldo_fn), 0) from financeiro where lojabb=f.lojabb and datacx=f.datacx and local <> f.lojabb ) as SldCx, (SELECT if(sum(deb) > 0, sum(deb), 0) FROM financeiro_mov f WHERE LOJA = f.lojabb and val='N') as Gtv FROM financeiro f where datacx='"&ConvData(date())&"' and local='"&int(request.Cookies("Info")("lojabb"))&"'"
instrucao_sqlAC = "SELECT f.*, (SELECT if(sum(valor) > 0, sum(valor), 0) as Resp FROM dif_caixa where lojabb=f.lojabb and tipo='Devedor' and status='Lancado') as difcx, (SELECT if(sum(valor) > 0, sum(valor), 0) as Resp FROM dif_caixa where lojabb=f.lojabb and tipo='Credora' and status='Lancado') as difcxCr, @maxcd:=(select max(codigo) from tot_posicao where lojabb=f.lojabb and posicao=f.datacx and convenio = '49093') as CdPos, (select alivio from tot_posicao where codigo=@maxcd) as alivio, (select if(sum(saldo_fn) > 0, sum(saldo_fn), 0) from financeiro where lojabb=f.lojabb and datacx=f.datacx and local <> f.lojabb ) as SldCx, (SELECT if(sum(deb) > 0, sum(deb), 0) FROM financeiro_mov f WHERE LOJA = f.lojabb and val='N' and datacx='"&ConvData(date())&"') as Gtv FROM financeiro f where datacx='"&ConvData(date())&"' and local='"&int(request.Cookies("Info")("lojabb"))&"'"
'response.Write(instrucao_sqlAC)
set dadosAC = objconnAC.execute(instrucao_sqlAC)
if dadosAC.EOF then
else
dadosAC.MoveFirst
While Not dadosAC.EOF
A1 = formatnumber(dadosAC("saldo_in"),2)
A2 = formatnumber(dadosAC("rec"),2)
A3 = formatnumber(dadosAC("pag"),2)
A4 = formatnumber(dadosAC("alivio"),2)
A5 = formatnumber(dadosAC("difcx"),2)
A6 = formatnumber(dadosAC("Gtv"),2)
B5 = formatnumber(dadosAC("difcxCr"),2)

calculado = ( (((((A1*1) + (A2*1)+ (B5*1)) + (A3*1) - (A4*1))-(A5*1))-(A6*1)) )
calculado = formatnumber(calculado,2)

B1 = formatnumber(dadosAC("saldo_fn"),2)
B2 = formatnumber(dadosAC("alivio"),2)

SaldoBB = ((B1*1)-(B2*1))
SaldoBB = formatnumber(SaldoBB,2)

%>

<%
Liberar = "true"

TodFaltaArq = funcaoPesquisa("SELECT count(codigo) as Resposta FROM dif_caixa where tipo='Devedor' and arquivo='n' and valor >= '20.00' and status='Lancado' and lojabb = '"&int(request.Cookies("Info")("lojabb"))&"' and date(data) >= '20170101'")
if TodFaltaArq <> "" and TodFaltaArq <> "0" then
Chavej = funcaoPesquisa("SELECT local as Resposta FROM dif_caixa where tipo='Devedor' and arquivo='n' and valor >= '20.00' and status='Lancado' and lojabb = '"&int(request.Cookies("Info")("lojabb"))&"' and date(data) >= '20170101' limit 0,1")
	response.Write("<br><br>OBS.: Existe Lançamento Diferenca a Menor sem anexo de arquivo! ChaveJ:"&Chavej)
	Liberar = "false"
end if

Set objconnA = Server.CreateObject("ADODB.Connection")
objconnA.open = StringConexaoBDCons()
instrucao_sqlA = "SELECT f.*, CHARACTER_LENGTH(local) as nr  FROM financeiro f where lojabb = '"&int(request.Cookies("Info")("lojabb"))&"' and datacx <= date(now()) and std='A' having nr > 5"
set dadosA = objconnA.execute(instrucao_sqlA)
if dadosA.EOF then
else
dadosA.MoveFirst
While Not dadosA.EOF
Liberar = "false"


if request.Cookies("Info")("chavej") = "J9610534" then
	Liberar = "true"
else
	response.Write("<br>ChaveJ: "&dadosA("local")&" - Aberto<br>")
end if

dadosA.MoveNext
Wend
response.Write("<br>")
end if
%>

<form id="acesso" name="acesso" method="post" action="lj_fechamento.asp?log=s&cx=<%=request("cx")%>&lg=<%=request("lg")%>">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">
              <div class="card-header" role="tab" id="headingOne">
                <h6 class="mb-0">
                    <div align="center"> Fechamento de Loja </div>
                </h6>
              </div>
              <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
                    <div class="row">
                      <div class="col-md-4">Loja / Responsavel:</div>
                      <div class="col-md-8  input-group-sm"><input type="text" class="form-control input-sm" id="LojaResp" name="LojaResp" disabled value="<%=request.Cookies("Info")("lojabb")%> - <%=request.Cookies("Info")("NomeOperadora")%>" onKeyPress="return(MascaraMoeda(this,'.',',',event))" /></div>
                    </div>
                    <div class="row">
					  <div class="col-xs-6">Saldo Reman.:</div>
                      <div class="col-xs-6">Data:</div>
					</div>
                    <div class="row">
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" readonly name="saldo_in" id="saldo_in" value="<%=formatnumber(dadosAC("saldo_in"),2)%>" > </div>
                      <div class="col-xs-6 input-group-sm"><input type="text" class="form-control input-sm" id="datacx" value="<%=response.Write(date())%>" readonly></div>
                    </div>
                    <div class="row">
					  <div class="col-xs-6">Recebimento:</div>
                      <div class="col-xs-6">Pagamento:</div>
					</div>
                    <div class="row">
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(dadosAC("rec"),2)%>" readonly name="rec" id="rec"  aria-describedby="sizing-addon2"></div>
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(dadosAC("pag"),2)%>" readonly name="pag" id="pag"   aria-describedby="sizing-addon2"></div>
                    </div>
                    <div class="row">
					  <div class="col-xs-6">Alivio:</div>
                      <div class="col-xs-6">Saldo&nbsp;Parcial:</div>
					</div>
                    <div class="row">
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(dadosAC("alivio"),2)%>" readonly name="alivio" id="alivio"  aria-describedby="sizing-addon2"></div>
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=SaldoBB%>" readonly name="SaldoBB" id="SaldoBB"  aria-describedby="sizing-addon2"></div>
                    </div>
                    <div class="row">
					  <div class="col-xs-6">Dif. Caixa Menor:</div>
                      <div class="col-xs-6">Dif. Caixa Maior:</div>
					</div>
                    <div class="row">
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" readonly value="<%=formatnumber(dadosAC("difcx"),2)%>" name="difcx" id="difcx" onKeyPress="return(MascaraMoeda(this,'.',',',event))"  aria-describedby="sizing-addon2"></div>
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" readonly value="<%=formatnumber(dadosAC("difcxCr"),2)%>"  name="difcxMaior" id="difcxMaior"  onKeyPress="return(MascaraMoeda(this,'.',',',event))"  aria-describedby="sizing-addon2"></div>
                    </div>
                    <div class="row">
					  <div class="col-xs-6">GTV:</div>
                      <div class="col-xs-6"></div>
					</div>
                    <div class="row">
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(dadosAC("Gtv"),2)%>" name="gtv" id="gtv" onKeyPress="return(MascaraMoeda(this,'.',',',event))"  aria-describedby="sizing-addon2"></div>
					  <div class="col-xs-6 input-group-sm"></div>
                    </div>
                      <div class="col-md-4">Saldo em Poder do Supervisor:</div>
                      <div class="col-md-8  input-group-sm"><input type="text" class="form-control input-sm" id="podersuper" name="podersuper"  readonly value="<%=response.Write(calculado)%>" /></div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">Justificativa:</div>
                    </div>
<!--                    <div class="row">
					  <div class="col-xs-6" align="right">Saldo PDV:</div>
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%'=formatnumber(dadosAC("sldcx"),2)%>" readonly  id="saldopdv" name="saldopdv"  aria-describedby="sizing-addon2"></div>
                    </div>
-->                    <div class="row">
					  <div class="col-xs-6" align="right">CompuSafe:</div>
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="0,00" onKeyPress="return(MascaraMoedaB(this,'.',',',event))" id="compusafe" name="compusafe" aria-describedby="sizing-addon2"></div>
                    </div>
                    <div class="row">
					  <div class="col-xs-6" align="right">Cofre Adm.:</div>
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(dadosAC("sldcx"),2)%>" onKeyPress="return(MascaraMoedaB(this,'.',',',event))" id="cofreadm" name="cofreadm" aria-describedby="sizing-addon2"></div>
                      <div id="SldCofAdm" style="color:#F00" align="right"> </div>
                    
                    </div>
                    <div class="row">
					  <div class="col-xs-6" align="right">Boca de Lobo:</div>
					  <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="0,00" onKeyPress="return(MascaraMoedaB(this,'.',',',event))" id="bocalobo" name="bocalobo" aria-describedby="sizing-addon2"></div>
                    </div>
                    <div class="row">
                      <div class="col-xs-6" align="left">Observação:</div>
                      <div class="col-xs-6" align="right"><div id="error" style="color:#F00"> </div></div>
                    </div>
                    <div class="row">
                      <div class="col-md-8  input-group-sm"><textarea class="form-control" rows="3" id="obs"  name="obs"  placeholder="Informe com riqueza de detalhes a descrição do ocorrido!"></textarea></div>
                    </div>
                    <div class="row">
					  <div class="col-xs-6">Senha Gerente:</div>
    					</div>
                    <div class="row">
					  <div class="col-xs-6"><input type="password" class="form-control input-sm" value="" id="senhaGR" name="senhaGR" /></div>
    				</div>
                    <div class="row">
                      <div class="col-md-4"> </br></div>
                      <div class="col-xs-6  btn-group-sm"><a class="btn btn-primary btn-lg btn-block" href="principal.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>" role="button">Cancelar</a></div>
					<% if Liberar = "true" then%>
                      <div class="col-xs-6  btn-group-sm"><input class="btn btn-primary btn-lg btn-block" type="button" name="bt" id="bt" disabled onClick="enviar()" value="Gravar" /></div>
					
					<% else %>
                      <div class="col-xs-6  btn-group-sm"><input class="btn btn-danger btn-lg btn-block" type="button" name="bt" id="bt" disabled  value="Gravar" /></div>
	                      
					<% end if %>                    
                    </div>
                    <div class="row">
                      <div class="col-md-12"></div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</form>
<%
dadosAC.MoveNext
Wend
end if
%>



  </body>
</html>