﻿<!--#include file="global.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Sys PDV">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Thiago Moskito e Jaime Agostinho">

    <title>Controle de Caixa</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <style type="text/css">
   body{margin-top:10px;}
   .glyphicon { margin-right:6px; }
   .card-block a{text-decoration: none;}
   .collapse .card-block:hover{background-color: #f0f0f0;}
   .collapse .card-block.disabled a{color:#c5c5c5;}
   .collapse .card-block.disabled, .collapse .card-block.disabled span{color:#c5c5c5;}
    </style>

<link href="root.css"  rel="stylesheet" type="text/css"/>
<%
DtCaixa = funcaoPesquisa("SELECT if(date(data) is NULL, 0, date(data)) as Resposta FROM financeiro f where date(data) <= date(now()) and std = 'A' and local='"&request.Cookies("Info")("chavej")&"'")
nVCaixa = funcaoPesquisa("SELECT if(date(data) is NULL, 0, date(data)) as Resposta FROM financeiro f where date(data) = date(now()) and local='"&request.Cookies("Info")("chavej")&"'")
mxdtcx = funcaoPesquisa("SELECT if(date(max(data)) is NULL, 0, date(max(data))) as Resposta FROM financeiro f where local='"&request.Cookies("Info")("chavej")&"'")
mxdtcx = replace(mxdtcx, "-", "")

FuncChaveJ = funcaoPesquisa("SELECT funcao as Resposta FROM usuario_operador where chavej = '"&request.Cookies("Info")("chavej")&"'")

%>
<SCRIPT LANGUAGE="JavaScript">
	redirTime = "25000";// 25000 equivale a 25 segundos
	redirURL = "rel_movimento.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>&dt=<%=mxdtcx%>&blq=<%=request("blq")%>";
	function redirTimer() { self.setTimeout("self.location.href = redirURL;",redirTime); }
</script>
</head>

<title>Saldo da Loja</title>
</head>
<BODY onLoad="redirTimer()">

<% if (FuncChaveJ = "23" or FuncChaveJ = "25" )  then%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><strong>RELATÓRIO SALDO DA LOJA - <%=date()%></strong></td>
  </tr>
</table>

<%
TempRelOper = funcaoPesquisa("SELECT EXTRACT(DAY_MINUTE FROM (TIMEDIFF(now(), t.data ))) as Resposta FROM tot_posicao t where codigo = (SELECT max(codigo) FROM tot_posicao where lojabb='"&request.Cookies("Info")("lojabb")&"' limit 0,1)")

if Cint(TempRelOper) > 600 then
%>
    <br><span class="bloqueado">
    ::ATENÇÃO::<br> Para ter acesso ao relatório de saldo da Loja, emita o relatorio no ACB:</br></br>
    ACB >><br>
    Transação do Operador >><br>
    Consulta >><br>
    Gerenciais >><br> 
    Posição Diária Detalhada!<br> </br>
    </span>
<%
else
%>
  <div class="col-xs-12  btn-group-sm"><a class="btn btn-primary btn-lg btn-block" href="retirada_gtv.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>" role="button">REGISTRO GTV</a></div>

<%
'or FuncChaveJ = "25"
'and request.Cookies("Info")("lojabb") = "11" 
SaldoRet = 0
Set objconn = Server.CreateObject("ADODB.Connection")
objconn.open = StringConexaoBD()
instrucao_sql = "SELECT t.*, if((SELECT sum(valor) FROM acbfull where dh_operacao > t.data and lojabb='"&request.Cookies("Info")("lojabb")&"') > 0, ((t.recebimento - t.pagamento) + (SELECT sum(valor) FROM acbfull where dh_operacao > t.data and lojabb='"&request.Cookies("Info")("lojabb")&"')), (t.recebimento - t.pagamento)) SldArrec FROM tot_posicao t where codigo = (SELECT max(codigo) FROM tot_posicao where lojabb='"&request.Cookies("Info")("lojabb")&"' and posicao = date(now()) limit 0,1)"
'response.Write(instrucao_sql)
set dados = objconn.execute(instrucao_sql)
if dados.EOF then
else
	ValorInicLoja = dados("saldo_ini")
	
	ValorArrecLoja = dados("SldArrec")

	AlivioBB = dados("alivio")
	
end if
SaldoRet = cDbl(ValorInicLoja) + cDbl(ValorArrecLoja)
SaldoRet = SaldoRet - cDbl(AlivioBB)
%>

<table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td colspan="2"><strong>SALDO DA LOJA - <%=date()%></strong></td>
  </tr>
  <tr>
    <td align="right">Saldo Remanescente:</td>
    <td align="right"><%=formatcurrency(ValorInicLoja)%></td>
  </tr>
  <tr>
    <td align="right">REC / PAG do Dia:</td>
    <td align="right"><%=formatcurrency(ValorArrecLoja)%></td>
  </tr>
  <tr>
    <td align="right">Alivio no BB:</td>
    <td align="right"><%=formatcurrency(AlivioBB)%></td>
  </tr>
  <tr>
    <td align="right">Saldo na Loja:</td>
    <td align="right"><%=formatcurrency(SaldoRet)%></td>
  </tr>
  <tr>
    <td align="right"></td>
    <td align="right"></td>
  </tr>
  <tr>
    <td colspan="2" align="right"><hr></td>
  </tr>
  <tr>
    <td colspan="2" align="left"><strong>SALDO NO CAIXA - <%=response.Write(mid(now(), 12,8))%></strong></td>
  </tr>
  <tr>
    <td colspan="2" align="right">
    <table width="70%" border="0" cellpadding="0" cellspacing="1">
      <tr>
        <td align="left" nowrap="nowrap">PDV</td>
        <td align="left" nowrap="nowrap">OPERADOR</td>
        <td align="left" nowrap="nowrap">STATUS</td>
        <td align="right" nowrap="nowrap">SALDO CAIXA</td>
        </tr>
<%
SldCxUltCv = 0
TotSld = 0
Set objconnAb = Server.CreateObject("ADODB.Connection")
objconnAb.open = StringConexaoBD()
instrucao_sqlAb = "SELECT u.chavej, (SELECT pdv FROM status_mac where chavej=u.chavej order by codigo desc limit 0,1) as pdvbb, (SELECT std FROM financeiro where local=u.chavej and lojabb=u.lojabb order by codigo desc limit 0,1) as Std, (SELECT saldo_fn FROM financeiro where local=u.chavej and lojabb=u.lojabb order by codigo desc limit 0,1) as SaldoCx FROM usuario_operador u where lojabb='"&request.Cookies("Info")("lojabb")&"' order by pdvbb"
'SELECT u.chavej, (SELECT pdv FROM status_mac where chavej=u.chavej order by codigo desc limit 0,1) as pdvbb, (SELECT saldo_fn FROM financeiro where datacx=date(now()) and local=u.chavej) as SaldoCx FROM usuario_operador u where lojabb='"&request.Cookies("Info")("lojabb")&"' order by pdvbb"
set dadosAb = objconnAb.execute(instrucao_sqlAb)
if dadosAb.EOF then
else
dadosAb.MoveFirst
While Not dadosAb.EOF
statd = ""
if dadosAb("Std") = "A" then
 statd = "Aberto"
end if
if dadosAb("Std") = "F" then
 statd = "Fechado"
end if

%>
  <tr>
    <td align="left"><%=dadosAb("pdvbb")%></td>
    <td align="left"><%=dadosAb("chavej")%></td>
    <td align="left"><%=statd%></td>
    <td align="right">
	<%
	if dadosAb("SaldoCx") <> "" then
	response.Write(formatcurrency(dadosAb("SaldoCx")))
	TotSld = TotSld + formatcurrency(dadosAb("SaldoCx"))

	else
	SldCxUltCv = funcaoPesquisa("SELECT saldo_fn as Resposta FROM financeiro where local = '"&dadosAb("chavej")&"' and datacx=(SELECT max(datacx) FROM financeiro where local = '"&dadosAb("chavej")&"')")
	
	if SldCxUltCv <> "" then
		response.Write(formatcurrency(SldCxUltCv))
		TotSld = TotSld + formatcurrency(SldCxUltCv)
	else
		response.Write(formatcurrency(0))
	end if
	
	end if
	%></td>
    </tr>
<%
dadosAb.MoveNext
Wend
end if
SaldoRet = SaldoRet - TotSld
%>
      <tr>
        <td colspan="3"><hr></td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Total:</td>
        <td><%=formatcurrency(TotSld)%></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2" align="right"><HR></td>
  </tr>
  <tr>
    <td colspan="2"><strong>GTV - Guia de Trafego de Valores</strong></td>
  </tr>
  <tr>
    <td colspan="2" align="right"><table width="80%" border="0" cellpadding="0" cellspacing="1">
      <tr>
        <td align="left" nowrap="nowrap">Data Hora</td>
        <td align="left" nowrap="nowrap">Nr.GTV</td>
        <td align="left" nowrap="nowrap">STATUS</td>
        <td align="right" nowrap="nowrap">VALOR</td>
      </tr>
<%
totgtv = 0
Set objconnAb = Server.CreateObject("ADODB.Connection")
objconnAb.open = StringConexaoBD()
instrucao_sqlAb = "select * from financeiro_mov where chavej='"&request.Cookies("Info")("lojabb")&"' and (datacx = date(now()) or val='N')"
'response.Write(instrucao_sqlAb)
set dadosAb = objconnAb.execute(instrucao_sqlAb)
if dadosAb.EOF then
else
dadosAb.MoveFirst
While Not dadosAb.EOF
%>
      <tr>
        <td align="left"><%=mid(dadosAb("data"), 1,5)&" "&mid(dadosAb("data"), 12,5)%></td>
        <td align="left"><%=dadosAb("n_doc")%></td>
        <td align="left"><%=dadosAb("val")%></td>
        <td align="right"><%=formatcurrency(dadosAb("total"))%></td>
      </tr>
<%
if dadosAb("val") = "N" then
totgtv = totgtv + formatcurrency(dadosAb("total"))
end if
dadosAb.MoveNext
Wend
end if
SaldoRet = SaldoRet - totgtv
%>
      <tr>
        <td colspan="4"><hr /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>Total:</td>
        <td align="right"><%=formatcurrency(totgtv,2)%>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2" align="right"><HR></td>
  </tr>
  <tr>
    <td align="right">Saldo Retaguarda:</td>
    <td align="right"><%=formatcurrency(SaldoRet)%></td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
</table>

<%
end if
end if
%>
<a href="principal.asp?lg=<%=request("lg")%>&cx=<%=response.Write(CxStatus())%>">[VOLTAR]</a>
</body>
</html>
