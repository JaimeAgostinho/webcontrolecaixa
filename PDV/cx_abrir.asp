﻿<!--#include file="global.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript">
	function enviar(){
		if(document.getElementById("valorAb").value != "" ){
			document.getElementById("acesso").submit()
		} else {
			alert('Informe o valor de Abertura de Caixa!')	
		}
		
	}


function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
	if(document.getElementById("valorAb").readOnly == false){
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
	}
    return false;
}

</script>

<%
if request("log") = "s" then
	
	
	sqlAbertura = "INSERT INTO financeiro (local, saldo_in, rec, pag, saldo_fn, retirada, datacx, dh_abe, lojabb) VALUES ('"&request.Cookies("Info")("chavej")&"', '"&BdValor(request("valorAb"))&"', '0', '0', '0', '0', '"&ConvData(date())&"', CURRENT_TIMESTAMP, '"&request.Cookies("Info")("lojabb")&"')"
	response.Write(sqlAbertura)
	MysqlInsert(sqlAbertura)

	response.Cookies("Info")("CxAberto") = "a"
	response.Cookies("Info")("datacx") = ConvData(date())
	
	response.Write("<script>alert('Abertura realizada com sucesso')</script>")
	response.Redirect("principal.asp?lg=y")

end if
%>


<link href="root.css"  rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Caixa</title>
<body>
<%
TempRelOper = funcaoPesquisa("SELECT EXTRACT(DAY_MINUTE FROM (TIMEDIFF(now(), t.data ))) as Resposta FROM tot_posicao t where codigo = (SELECT max(codigo) FROM tot_posicao where lojabb='"&request.Cookies("Info")("lojabb")&"' and posicao = date(now()) and date(data)= date(now()) order by codigo desc limit 0,1)")
if TempRelOper = "" then
	TempRelOper = 200
end if
if Cint(TempRelOper) > 15 then
	MsgAlerta = MsgAlerta &"<br><br>OBS.: Antes de Abrir o Caixa, emita um relatorio de SALDO DA LOJA!; <br><br>MENU: ACB >> <br>TRANSAÇÕES DO OPERADOR >> <br>CONSULTAS >> GERENCIAIS >> <br>POSIÇÃO DIÁRIA DETALHADA<br><br>Data Atual!"
	BollFechar = "false"
else
	BollFechar = "true"
end if
%>

<% if BollFechar = "true" then%>
<form id="acesso" name="acesso" method="post" action="cx_abrir.asp?log=s">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="7" align="center"><strong>ABERTURA DE CAIXA</strong></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">Chave J:</td>
    <td colspan="4"><input name="chavej" type="text" disabled="disabled" id="chavej" value="<%=Request.Cookies("Info")("chavej")%>" maxlength="8"  /></td>
  </tr>
  <tr>
    <td colspan="3">Data da Abertura:</td>
    <td colspan="4"><input name="data" type="text" disabled="disabled" id="data" value="<%=response.Write(date())%>" maxlength="8"  /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">Valor Inicial do Caixa:</td>
    <td colspan="4">
	<%
	sld_fex = funcaoPesquisa("SELECT saldo_fn as Resposta FROM financeiro f where codigo=(SELECT max(codigo) FROM financeiro f where std='F' and local='"&Request.Cookies("Info")("chavej")&"')")
	%>    
    <% if sld_fex = "" then%>
    <input name="valorAb" type="text" id="valorAb" onKeyPress="return(MascaraMoeda(this,'.',',',event))" value="" size="20" maxlength="8"  />
    <%else%>
    <input name="valorAb" type="text" id="valorAb" onKeyPress="return(MascaraMoeda(this,'.',',',event))" value="<%=sld_fex%>" size="20" maxlength="8" readonly  />
    <%end if%>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="7" align="center"><input type="button" name="bt" onClick="enviar()" value="Registrar Abertura de Caixa"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

</form>
<%else%>
<strong><font color="#FF0000"><%=response.Write(MsgAlerta)%></font></strong>
</br></br></br>
<input type="button" value="Voltar" onClick="history.go(-1)"> 
<%end if%>
</body>
</html>