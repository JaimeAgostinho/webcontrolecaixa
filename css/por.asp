﻿
<%
if request("forma_con") <> "" then
	session("forma_con") = request("forma_con")
end if
%>
<select name="forma_con" id="forma_con">
	<option value="ca.codigo_agente" <% if session("forma_con") = "ca.codigo_agente" then%>selected<%end if%>>Código Agente</option>
	<option value="ca.mci" <% if session("forma_con") = "ca.mci" then%>selected<%end if%>>Mci</option>
	<option value="ca.convenio" <% if session("forma_con") = "ca.convenio" then%>selected<%end if%>>Convenio</option>
	<option value="ca.nome_fantasia" <% if session("forma_con") = "ca.nome_fantasia" then%>selected<%end if%>>Nome Fantasia</option>
	<option value="ca.razao_social" <% if session("forma_con") = "ca.razao_social" then%>selected<%end if%> >Razão Social</option>
	<option value="ca.cnpj" <% if session("forma_con") = "ca.cnpj" then%>selected<%end if%>>CNPJ</option>
	<option value="ca.email" <% if session("forma_con") = "ca.email" then%>selected<%end if%> >E-mail</option>
	<option value="ca.telefone" <% if session("forma_con") = "ca.telefone" then%>selected<%end if%>>Telefone</option>
	<option value="ca.cidade" <% if session("forma_con") = "ca.cidade" then%>selected<%end if%>>Cidade</option>
	<option value="ca.bairro" <% if session("forma_con") = "ca.bairro" then%>selected<%end if%>>Bairro</option>
	<option value="ca.estado" <% if session("forma_con") = "ca.estado" then%>selected<%end if%>>Estado</option>
	<option value="ca.status" <% if session("agente.status") = "ca.status" or request("forma_con") = "ca.status" then%>selected<%end if%>>Status</option>
	<option value="ca.origem" <% if session("forma_con") = "ca.origem" then%>selected<%end if%>>Origem (Ic - Zaz)</option>
	<option value="ca.tipo_ag" <% if session("forma_con") = "ca.tipo_ag" then%>selected<%end if%>>Sistema</option>
	<option value="-" <% if session("forma_con") = "-" then%>selected<%end if%>></option>
</select>
