<%
if request("gv") = "s" then
	sql = "INSERT INTO dif_caixa (local, lojabb, data, datacx, tipo, motivo, obs, valor, status) VALUES ('"&request("chavej")&"', '"&int(request("lojabb"))&"', '"&ConvData(request("data"))&"', '"&ConvData(request("data"))&"', 'Devedor', '"&request("motivo")&"', '"&request("obs")&"', '"&BdValor(request("Valor"))&"', '"&request("std")&"')"
	MysqlInsert(sql)
	response.Redirect("adm.asp?url=dif_devedora&Orb=var")
end if
%>


<script type="text/javascript">
	function enviar(){
		if(document.getElementById("lojabb").value != "" && document.getElementById("chavej").value != "" && document.getElementById("data").value != "" && document.getElementById("Valor").value != ""  && document.getElementById("motivo").value != "" ){
			document.getElementById("acesso").submit()
		} else {
			alert('Ha campos nao informados!')	
		}
		
	}
	
	
function MascaraGaveta(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
	if(document.getElementById("Valor").readOnly == false){
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
	}
    return false;
}

</script>

<div class="row">
    <div class="col-sm-12">
		<%=CabecalhoUrl("Lançar Dif. A Menor")%>
    </div>
</div>

<form id="acesso" name="acesso" method="post" action="adm.asp?url=cad_difamenor&Orb=var&log=a&gv=s">
 <div class="form-group">
     <div class="container">
      <div class="row" align="center">
        <div class="col-xs-6 ">
          <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">

              <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
              <div class="form-group row"></br></div>
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Loja:
                </div>
		        <div class="col-lg-8 input-group-sm">
                
                <input class="form-control input-sm"  type="text" name="lojabb"  id="lojabb" value=""/>
                </div>
              </div>
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Operador:
                </div>
		        <div class="col-lg-8 input-group-sm"><input class="form-control input-sm"  type="text" name="chavej"  id="chavej" value=""/>
                </div>
              </div>
			
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Data do Caixa:
                </div>
		        <div class="col-lg-8 input-group-sm"><input name="data"  type="text" class="form-control input-sm"  id="data" onKeyPress="MascaraForm(this,Data);" onKeyDown="MascaraForm(this,Data);" onKeyUp="MascaraForm(this,Data);" value="" maxlength="10"/>
                </div>
              </div>

             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Motivo:
                </div>
		        <div class="col-lg-8 input-group-sm"><input class="form-control input-sm"  type="text" name="motivo"  id="motivo" value=""/>
                </div>
              </div>
              
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Observação:
                </div>
		        <div class="col-lg-8 input-group-sm">
		          <textarea class="form-control" rows="3" id="obs"  name="obs"   placeholder=""></textarea>
                </div>
              </div>

             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Valor:
                </div>
		        <div class="col-lg-8 input-group-sm"><input class="form-control input-sm"  type="text" onKeyPress="return(MascaraGaveta(this,'.',',',event))" name="Valor"  id="Valor" value=""/>
                </div>
              </div>

             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Status:
                </div>
		        <div class="col-lg-8 input-group-sm">
                    <select class="form-control" id="std" name="std">
                          <option value="Lancado">Lançado Pelo PDV</option>
                          <option value="Valor a prejuízo" >Valor a prejuízo</option>
                          <option value="Valor em processo" >Valor em processo</option>
                          <option value="Desconto em folha" >Desconto em folha</option>
                          <option value="Sinistro" >Sinistro</option>
                          <option value="Improcedente" >Improcedente</option>
                        </select>                      
                </div>
              </div>
             <div class="form-group row">
                        <div class="col-lg-4 input-group-sm" align="right"></div>
                      <div class="col-lg-8  btn-group-sm"><input class="btn btn-primary btn-lg btn-block" type="button"  onclick="enviar()" name="bt" id="bt" value="Gravar" /></div>
                    </div>

            </div>
		  </div>
	    </div>
	  </div>
	</div>
	</div>
</div>
</div>
</form>
