<div class="row">
    <div class="col-sm-12">
		<%=CabecalhoUrl("Relatório de Lojas BB")%>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="acesso" name="acesso" method="post" action="adm.asp?url=rel_lojabb&Orb=var&log=s">
        <div class="row">
            <div class="col-sm-2">
                <input class="form-control input-sm" type="text" name="lojabb"  id="lojabb" placeholder="Codigo da Loja:" value="<%=request("lojabb")%>"/>
            </div>
            <div class="col-sm-4">
                <input class="form-control input-sm" type="text" name="nome"  id="nome" placeholder="Nome da Loja:" value="<%=request("nome")%>"/>
            </div>
            <div class="col-sm-2">
                    <select class="form-control input-sm" id="tipo_loja" name="tipo_loja">
                          <option value="" <% if request.cookies("tipo_loja") = "" then%>selected<%end if%>>Todos</option>
					      <option value="lojas de rua"  <% if request.cookies("tipo_loja") = "lojas de rua" then%>selected<%end if%>>lojas de rua</option>
					      <option value="Poupatempo"  <% if request.cookies("tipo_loja") = "Poupatempo" then%>selected<%end if%>>Poupatempo</option>
					      <option value="DETRAN"  <% if request.cookies("tipo_loja") = "DETRAN" then%>selected<%end if%>>DETRAN</option>
					      <option value="FORUNS"  <% if request.cookies("tipo_loja") = "FORUNS" then%>selected<%end if%>>FORUNS</option>
                          </select>                      

            </div>
            <div class="col-sm-1 btn-group-sm">
                <input class="btn btn-primary btn-lg btn-block" type="submit" name="bt" id="bt" value="Pesquisar" />
            </div>
        
        </div>
        </form>
   </div>
</div>
<br>
<% if request("log") = "s" then%>

<%
where = ""
	where = where&" nome like '%"&request.Form("nome")&"%'"
if request.form("lojabb") <> "" then
	where = where&" and lojabb = '"&request.Form("lojabb")&"'"
end if 
if request.form("tipo_loja") <> "" then
	where = where&" and tipo_loja = '"&request.Form("tipo_loja")&"'"
end if 

%>

<div class="row">
<div class="col-sm-11">
    <table class="table table-striped table-bordered ">
        <tr> 
            <th>loja</th>	
            <th>Nome Loja</th>	
            <th>Tipo de Loja</th>	
            <th>Cidade/UF</th>	
            <th>Telefone</th>	
            <th>Regional</th>	
            <th>Transp. Valor</th>	
            <th>Menu</th>	
        </tr>
<%
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT * FROM cad_lojabb where "&where
'response.Write(instrucao_sqlACi)
set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF
%>
        <tr>
            <td><%=dadosACi("lojabb")%></td>	
            <td><%=ucase(dadosACi("nome"))%></td>	
            <td><%=dadosACi("tipo_loja")%></td>	
            <td><%=ucase(dadosACi("cidade"))%> - <%=ucase(dadosACi("uf"))%></td>	
            <td align="right"><%=dadosACi("telefone")%></td>	
            <td><%=dadosACi("regional")%></td>	
            <td><%=dadosACi("transp_valor")%></td>	
            <td> 
        <%
		BuscRes = PesqResp("SELECT stat as Resp FROM m_controle where codigo_l = '205' and codigo_u='"&request.cookies("sismy")("cd_usu")&"'")
		 if BuscRes = "s" then
		 %>
        <a href="adm.asp?url=rep_lojabb&Orb=var&edit=s&cd=<%=dadosACi("codigo")%>&ed=y">
          <span class="glyphicon glyphicon-cog"></span>
        </a>	&nbsp;&nbsp;
        <% end if %>			
        <a href="adm.asp?url=rep_lojabb&Orb=var&edit=s&cd=<%=dadosACi("codigo")%>">
          <span class="glyphicon glyphicon-list-alt"></span>
        </a>
            </td>	
        </tr>
<%
dadosACi.MoveNext
Wend
end if
%>	

    </table>
</div>
</div>
<% end if %>

