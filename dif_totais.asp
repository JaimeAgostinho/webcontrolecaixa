
<div class="row">
    <div class="col-sm-12">
		<%=CabecalhoUrl("Diferença Totais - Pendência")%>
    </div>
</div>
<%

 if request("log") <> "a" then

%>
<div class="row">
    <div class="col-sm-12">
        <form id="acesso" name="acesso" method="post" action="adm.asp?url=dif_totais&Orb=var&log=s">
        <div class="row">
        
            <div class="col-sm-3 btn-group-sm">
                <input class="btn btn-primary btn-lg btn-block" type="submit" name="bt" id="bt" value="Consultar Relatório" />
            </div>
        
        </div>
        </form>
   </div>
</div>
<br>
<% if request("log") = "s" then%>

<%
sql = "SELECT lojabb, nome, (select if(sum(valor) > 0,sum(valor), 0) from dif_caixa where status='Lancado' and tipo = 'Credora' and lojabb=c.lojabb) as DifCredor, (select if(sum(valor) > 0,sum(valor), 0) from dif_caixa where status='Lancado' and tipo = 'Devedor' and lojabb=c.lojabb) as DifDevedor FROM cad_lojabb c order by lojabb"

%>

<script>
angular.module("Orbistec").controller("Relatorio", function ($scope)
{
	$scope.DifCaixa = <%=FuncJson1(Sql)%>;
	
	$scope.SumA = 0;
  $scope.SumB = 0;
	angular.forEach($scope.DifCaixa, function(value, key) {
	$scope.SumA += parseFloat(value.DifDevedor);
  $scope.SumB += parseFloat(value.DifCredor);
	});
});
</script>
<div class="row">
<div ng-controller="Relatorio">
<div class="col-sm-1"></div>
<div class="col-sm-6">

    <table class="table table-striped table-bordered ">
        <tr> 
            <th>loja</th>	
            <th>Nome Operador</th>	
            <th>Diferença a Menor</th>	
            <th>Diferença Credora</th>	
        </tr>
        <tr ng-repeat="registro in DifCaixa | filter:criterioDeBusca">
            <td>{{registro.lojabb}}</td>	
            <td>{{registro.nome}}</td>	
            <td align="right">{{registro.DifDevedor | currency:'R$ '}}</td>	
            <td align="right">{{registro.DifCredor | currency:'R$ '}}</td>  
        </tr>
        <tr>
            <td></td> 
            <td></td>  
            <td align="right">{{SumA | currency:'R$ '}}</td>  
            <td align="right">{{SumB | currency:'R$ '}}</td>  
        </tr>
    </table>

</div>
</div>
</div>
<% end if %>
<% end if %>
<% if request("log") = "a" then%>

<%
if request("gv") = "s" then
	sql = "UPDATE dif_caixa d SET status='"&request.Form("std")&"' where codigo='"&request("cd")&"'"
	MysqlInsert(sql)

  response.write(HistRetaguarda("DD"&request("cd")&"", "USUARIO: "&request.cookies("sismy")("loguin")&"; STATUS:"&request.Form("std")&" ;"))

	response.Redirect("adm.asp?url=dif_devedora&Orb=var")
end if


Set objconnAb = Server.CreateObject("ADODB.Connection")
objconnAb.open = StringConexaoBDCons()
instrucao_sqlAb = "SELECT * FROM dif_caixa where codigo='"&request("cd")&"'"
set dadosAb = objconnAb.execute(instrucao_sqlAb)
if dadosAb.EOF then
else
dadosAb.MoveFirst
While Not dadosAb.EOF
%>

<form id="acesso" name="acesso" method="post" action="adm.asp?url=dif_devedora&Orb=var&log=a&cd=<%=request("cd")%>&gv=s">
 <div class="form-group">
     <div class="container">
      <div class="row" align="center">
        <div class="col-xs-6 ">
          <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">

              <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
              <div class="form-group row"></br></div>
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Loja:
                </div>
		        <div class="col-lg-8 input-group-sm">
                
                <input class="form-control input-sm" readonly type="text" name="loja"  id="loja" value="<%=dadosAb("lojabb")%>"/>
                </div>
              </div>
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Operador:
                </div>
		        <div class="col-lg-8 input-group-sm"><input class="form-control input-sm" readonly type="text" name="loja"  id="loja" value="<%=dadosAb("local")%> - <%=PesqChaveJ(dadosAb("local"), "nome")%>"/>
                </div>
              </div>
			
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Data do Caixa:
                </div>
		        <div class="col-lg-8 input-group-sm"><input class="form-control input-sm" readonly type="text" name="loja"  id="loja" value="<%=dadosAb("datacx")%>"/>
                </div>
              </div>

             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Motivo:
                </div>
		        <div class="col-lg-8 input-group-sm"><input class="form-control input-sm" readonly type="text" name="loja"  id="loja" value="<%=dadosAb("motivo")%>"/>
                </div>
              </div>
              
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Observação:
                </div>
		        <div class="col-lg-8 input-group-sm">
		          <textarea class="form-control" rows="3" id="obs"  name="obs"  readonly placeholder="<%=dadosAb("obs")%>"></textarea>
                </div>
              </div>

             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Valor:
                </div>
		        <div class="col-lg-8 input-group-sm"><input class="form-control input-sm" readonly type="text" name="loja"  id="loja" value="<%=formatcurrency(dadosAb("valor"),2)%>"/>
                </div>
              </div>

             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Status:
                </div>
		        <div class="col-lg-8 input-group-sm">
                    <select class="form-control" id="std" name="std">
                          <option value="Lancado" <% if dadosAb("status") = "Lancado" then%>selected<%end if%>>Lançado Pelo PDV</option>
                          <option value="Valor a prejuízo" <% if dadosAb("status") = "Valor a prejuízo" then%>selected<%end if%>>Valor a prejuízo</option>
                          <option value="Valor em processo" <% if dadosAb("status") = "Valor em processo" then%>selected<%end if%>>Valor em processo</option>
                          <option value="Desconto em folha" <% if dadosAb("status") = "Desconto em folha" then%>selected<%end if%>>Desconto em folha</option>
                          <option value="Sinistro" <% if dadosAb("status") = "Sinistro" then%>selected<%end if%>>Sinistro</option>
                          <option value="Improcedente" <% if dadosAb("status") = "Improcedente" then%>selected<%end if%>>Improcedente</option>
                          <option value="Alívio indevido" <% if dadosAb("status") = "Alívio indevido" then%>selected<%end if%>>Alívio indevido</option>
                          <option value="Alívio sensibilizado" <% if dadosAb("status") = "Alívio sensibilizado" then%>selected<%end if%>>Alívio sensibilizado</option>
                        </select>                      
                </div>
              </div>
             <div class="form-group row">
                        <div class="col-lg-4 input-group-sm" align="right"></div>
                      <div class="col-lg-8  btn-group-sm"><input class="btn btn-primary btn-lg btn-block" type="submit" name="bt" id="bt" value="Gravar" /></div>
                    </div>

 <div class="row" align="left">
<div class="col-lg-4" align="left"></div>
<div class="col-lg-8" align="left">
<br>
HISTORICO DE LANCAMENTOS:<BR>
<%
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT * FROM hist_retaguarda where funcao = 'DD"&dadosAb("codigo")&"'"
 set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF
response.write(dadosACi("data")&" - "&dadosACi("descricao")&"</br>")
dadosACi.MoveNext
Wend
end if
%>     
</div>
 </div>

            </div>
		  </div>
	    </div>
	  </div>
	</div>
	</div>
</div>
</div>
</form>
<%
dadosAb.MoveNext
Wend
end if
%>
<%end if%>