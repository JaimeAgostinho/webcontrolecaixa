
<div class="row">
    <div class="col-sm-12">
		<%=CabecalhoUrl("Diferença de Caixa - A Menor")%>
    </div>
</div>
<%

	if request("ex") = "s" then
		MysqlInsert("DELETE FROM dif_caixa WHERE codigo='"&request("cd")&"' and lojabb='"&request("lojabb")&"'")
		response.Redirect("adm.asp?url=dif_devedora&Orb=var")
	end if

 if request("log") <> "a" then
  if request("log") <> "b"  then

	if request("log") = "s" then
		response.cookies("lojabb") = request.Form("loja")
		response.cookies("chavej") = request.Form("chavej")
		response.cookies("status") = request.Form("status")
	end if
%>
<div class="row">
    <div class="col-sm-12">
        <form id="acesso" name="acesso" method="post" action="adm.asp?url=dif_devedora&Orb=var&log=s">
        <div class="row">
        
            <div class="col-sm-2">
                <input class="form-control input-sm" type="text" name="data" maxlength="10" onKeyDown="MascaraForm(this,Data);" onKeyPress="MascaraForm(this,Data);" onKeyUp="MascaraForm(this,Data);" id="data" placeholder="Data:" value="<% if request("log") = "s" then%><%=request.form("data")%><%end if%>"/>
            </div>
            <div class="col-sm-2">
                <input class="form-control input-sm" type="text" name="loja"  id="loja" placeholder="Loja:" value="<%=request.cookies("lojabb")%>"/>
            </div>
            <div class="col-sm-2">
                <input class="form-control input-sm" type="text" name="chavej" id="chavej" placeholder="ChaveJ:" value="<%=request.cookies("chavej")%>"/>
            </div>
            <div class="col-sm-2">
                    <select class="form-control input-sm" id="status" name="status">
                          <option value="todos" <% if request.cookies("status") = "todos" then%>selected<%end if%>>Todos</option>
                          <option value="Lancado" <% if request.cookies("status") = "Lancado" then%>selected<%end if%>>Lançado Pelo PDV</option>
                          <option value="Valor a prejuízo" <% if request.cookies("status") = "Valor a prejuízo" then%>selected<%end if%>>Valor a prejuízo</option>
                          <option value="Valor em processo" <% if request.cookies("status") = "Valor em processo" then%>selected<%end if%>>Valor em processo</option>
                          <option value="Desconto em folha" <% if request.cookies("status") = "Desconto em folha" then%>selected<%end if%>>Desconto em folha</option>
                          <option value="Sinistro" <% if request.cookies("status") = "Sinistro" then%>selected<%end if%>>Sinistro</option>
                          <option value="Improcedente" <% if request.cookies("status") = "Improcedente" then%>selected<%end if%>>Improcedente</option>
                          <option value="Alívio indevido" <% if request.cookies("status") = "Alívio indevido" then%>selected<%end if%>>Alívio indevido</option>
                          <option value="Alívio sensibilizado" <% if request.cookies("status") = "Alívio sensibilizado" then%>selected<%end if%>>Alívio sensibilizado</option>
                        </select>                      

            </div>
            <div class="col-sm-1 btn-group-sm">
                <input class="btn btn-primary btn-lg btn-block" type="submit" name="bt" id="bt" value="Pesquisar" />
            </div>
        
        </div>
        </form>
   </div>
</div>
<br>
<% if request("log") = "s" then%>

<%
where = ""
if request.form("data") <> "" then
	where = where&" and datacx = '"&ConvData(request.Form("data"))&"'"
end if 
if request.form("loja") <> "" then
	where = where&" and lojabb = '"&request.Form("loja")&"'"
end if 
if request.form("chavej") <> "" then
	where = where&" and local = '"&request.Form("chavej")&"'"
end if 
if request.form("status") <> "" then
	if request.form("status") <> "todos" then
		where = where&" and status = '"&request.Form("status")&"'"
	end if 
end if 

Sql = "SELECT d.codigo, d.local, d.lojabb, d.data, d.datacx, d.tipo, replace(d.valor, ',','.') as valor, d.motivo, if(d.status = 'Lancado', 'Lançado', d.status ) as status, (select nome from usuario_operador where chavej=d.local limit 0,1) as nome FROM dif_caixa d where tipo='Devedor' "&where
%>
<script>
angular.module("Orbistec").controller("Relatorio", function ($scope)
{
	$scope.DifCaixa = <%=FuncJson(Sql)%>;
	
	$scope.Sum = 0;
	angular.forEach($scope.DifCaixa, function(value, key) {
	$scope.Sum += parseFloat(value.valor);
	});
});
</script>
<div class="row">
<div ng-controller="Relatorio">
<div class="col-sm-11">
    <table class="table table-striped table-bordered ">
        <tr> 
            <th>loja</th>	
            <th>ChaveJ</th>	
            <th>Nome Operador</th>	
            <th>Data do Caixa</th>	
            <th>Motivo</th>	
            <th>Valor</th>	
            <th>Status</th>	
            <th>Menu</th>	
        </tr>
        <tr ng-repeat="registro in DifCaixa | filter:criterioDeBusca">
            <td>{{registro.lojabb}}</td>	
            <td>{{registro.local}}</td>	
            <td>{{registro.nome}}</td>	
            <td>{{registro.datacx | date:'dd/MM/yyyy'}}</td>	
            <td>{{registro.motivo}}</td>	
            <td align="right">{{registro.valor | currency:'R$ '}}</td>	
            <td>{{registro.status}}</td>	
            <td> 

            <a href="adm.asp?url=dif_devedora&Orb=var&log=a&cd={{registro.codigo}}"><span class="glyphicon glyphicon-cog"></span></a>
			<a href="../Upload/difdev-{{registro.codigo}}.pdf" target="new"><img src="../imagem/pdf.png" width="14" height="14"> </a>  
<!--            <a href="adm.asp?url=dif_devedora&Orb=var&ab=s&pesq=s&ex=s&cd={{registro.codigo}}&lojabb={{registro.lojabb}}"><img src="imagem/icon_trashcan.gif" width="12" height="12" border="0"></a>                      
-->            </td>	
        </tr>
        <tr>
            <td align="right" colspan="5">Total:</td>
            <td align="right">{{Sum | currency:'R$ '}}</td>
            <td align="right" colspan="2">
              <%
                if request.form("loja") <> "" then
                  if request.form("status") = "Lancado" then
                  %>
                    <div class="col-xs-12  btn-group-sm" table-bordered align="center"><a class="btn btn-success" href="adm.asp?url=dif_devedora&Orb=var&log=b&lojabb=<%=request.Form("loja")%>" role="button"> Mudar Status de Todos!</a></div>
                  <%
                  end if
              end if
              %>           
            </td>
        </tr>

    </table>
    Data:[<%=request.Form("data")%>] - Loja:[<%=request.Form("loja")%>] - Chavej:[<%=request.Form("chavej")%>] - Status:[<%=request.Form("status")%>]
</div>
</div>
</div>
<% end if %>
<% end if %>
<% end if %>


<% if request("log") = "a" then%>

<%
if request("gv") = "s" then
	sql = "UPDATE dif_caixa d SET status='"&request.Form("std")&"' where codigo='"&request("cd")&"'"
	MysqlInsert(sql)

  response.write(HistRetaguarda("DD"&request("cd")&"", "USUARIO: "&request.cookies("sismy")("loguin")&"; STATUS:"&request.Form("std")&" ;"))

	response.Redirect("adm.asp?url=dif_devedora&Orb=var")
end if


Set objconnAb = Server.CreateObject("ADODB.Connection")
objconnAb.open = StringConexaoBDCons()
instrucao_sqlAb = "SELECT * FROM dif_caixa where codigo='"&request("cd")&"'"
set dadosAb = objconnAb.execute(instrucao_sqlAb)
if dadosAb.EOF then
else
dadosAb.MoveFirst
While Not dadosAb.EOF
%>

<form id="acesso" name="acesso" method="post" action="adm.asp?url=dif_devedora&Orb=var&log=a&cd=<%=request("cd")%>&gv=s">
 <div class="form-group">
     <div class="container">
      <div class="row" align="center">
        <div class="col-xs-6 ">
          <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">

              <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
              <div class="form-group row"></br></div>
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Loja:
                </div>
		        <div class="col-lg-8 input-group-sm">
                
                <input class="form-control input-sm" readonly type="text" name="loja"  id="loja" value="<%=dadosAb("lojabb")%>"/>
                </div>
              </div>
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Operador:
                </div>
		        <div class="col-lg-8 input-group-sm"><input class="form-control input-sm" readonly type="text" name="loja"  id="loja" value="<%=dadosAb("local")%> - <%=PesqChaveJ(dadosAb("local"), "nome")%>"/>
                </div>
              </div>
			
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Data do Caixa:
                </div>
		        <div class="col-lg-8 input-group-sm"><input class="form-control input-sm" readonly type="text" name="loja"  id="loja" value="<%=dadosAb("datacx")%>"/>
                </div>
              </div>

             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Motivo:
                </div>
		        <div class="col-lg-8 input-group-sm"><input class="form-control input-sm" readonly type="text" name="loja"  id="loja" value="<%=dadosAb("motivo")%>"/>
                </div>
              </div>
              
             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Observação:
                </div>
		        <div class="col-lg-8 input-group-sm">
		          <textarea class="form-control" rows="3" id="obs"  name="obs"  readonly placeholder="<%=dadosAb("obs")%>"></textarea>
                </div>
              </div>

             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Valor:
                </div>
		        <div class="col-lg-8 input-group-sm"><input class="form-control input-sm" readonly type="text" name="loja"  id="loja" value="<%=formatcurrency(dadosAb("valor"),2)%>"/>
                </div>
              </div>

             <div class="form-group row">
		        <div class="col-lg-4 input-group-sm" align="right">Status:
                </div>
		        <div class="col-lg-8 input-group-sm">
                    <select class="form-control" id="std" name="std">
                          <option value="Lancado" <% if dadosAb("status") = "Lancado" then%>selected<%end if%>>Lançado Pelo PDV</option>
                          <option value="Valor a prejuízo" <% if dadosAb("status") = "Valor a prejuízo" then%>selected<%end if%>>Valor a prejuízo</option>
                          <option value="Valor em processo" <% if dadosAb("status") = "Valor em processo" then%>selected<%end if%>>Valor em processo</option>
                          <option value="Desconto em folha" <% if dadosAb("status") = "Desconto em folha" then%>selected<%end if%>>Desconto em folha</option>
                          <option value="Sinistro" <% if dadosAb("status") = "Sinistro" then%>selected<%end if%>>Sinistro</option>
                          <option value="Improcedente" <% if dadosAb("status") = "Improcedente" then%>selected<%end if%>>Improcedente</option>
                          <option value="Alívio indevido" <% if dadosAb("status") = "Alívio indevido" then%>selected<%end if%>>Alívio indevido</option>
                          <option value="Alívio sensibilizado" <% if dadosAb("status") = "Alívio sensibilizado" then%>selected<%end if%>>Alívio sensibilizado</option>
                        </select>                      
                </div>
              </div>
             <div class="form-group row">
                        <div class="col-lg-4 input-group-sm" align="right"></div>
                      <div class="col-lg-8  btn-group-sm"><input class="btn btn-primary btn-lg btn-block" type="submit" name="bt" id="bt" value="Gravar" /></div>
                    </div>

 <div class="row" align="left">
<div class="col-lg-4" align="left"></div>
<div class="col-lg-8" align="left">
<br>
HISTORICO DE LANCAMENTOS:<BR>
<%
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT * FROM hist_retaguarda where funcao = 'DD"&dadosAb("codigo")&"'"
 set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF
response.write(dadosACi("data")&" - "&dadosACi("descricao")&"</br>")
dadosACi.MoveNext
Wend
end if
%>     
</div>
 </div>

            </div>
		  </div>
	    </div>
	  </div>
	</div>
	</div>
</div>
</div>
</form>
<%
dadosAb.MoveNext
Wend
end if
%>
<%end if%>



<% if request("log") = "b" then%>
<%
if request("gv") = "s" then


Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT * FROM dif_caixa where tipo='Devedor' and status='Lancado' and lojabb='"&request("lojabb")&"'"
 set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF

  MysqlInsert("UPDATE dif_caixa d SET status='"&request.Form("std")&"' where codigo='"&dadosACi("codigo")&"' and tipo='Devedor' and status='Lancado' and lojabb='"&request("lojabb")&"'")

  response.write(HistRetaguarda("DD"&dadosACi("codigo")&"", "USUARIO: "&request.cookies("sismy")("loguin")&"; STATUS:"&request.Form("std")&" ;Mudança em Massa!"))

dadosACi.MoveNext
Wend
end if

  response.Redirect("adm.asp?url=dif_devedora&Orb=var")
end if

%>

<form id="acesso" name="acesso" method="post" action="adm.asp?url=dif_devedora&Orb=var&log=b&gv=s">
 <div class="form-group">
     <div class="container">
      <div class="row" align="center">
        <div class="col-xs-6 ">
          <div id="accordion" role="tablist" aria-multiselectable="true">
            <div class="card">

              <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="card-block">
              <div class="form-group row"></br></div>
             <div class="form-group row">
            <div class="col-lg-4 input-group-sm" align="right">Loja:
                </div>
            <div class="col-lg-8 input-group-sm">
                
                <input class="form-control input-sm" readonly type="text" name="loja"  id="loja" value="<%=request("lojabb")%>"/>
                </div>
              </div>

             <div class="form-group row">
            <div class="col-lg-4 input-group-sm" align="right">Status:
                </div>
            <div class="col-lg-8 input-group-sm">
                    <select class="form-control" id="std" name="std">
                          <option value="Lancado" >Lançado Pelo PDV</option>
                          <option value="Valor a prejuízo" >Valor a prejuízo</option>
                          <option value="Valor em processo" >Valor em processo</option>
                          <option value="Desconto em folha" >Desconto em folha</option>
                          <option value="Sinistro" >Sinistro</option>
                          <option value="Improcedente" >Improcedente</option>
                          <option value="Alívio indevido" >Alívio indevido</option>
                          <option value="Alívio sensibilizado" >Alívio sensibilizado</option>
                        </select>                      
                </div>
              </div>
             <div class="form-group row">
                        <div class="col-lg-4 input-group-sm" align="right"></div>
                      <div class="col-lg-8  btn-group-sm"><input class="btn btn-primary btn-lg btn-block" type="submit" name="bt" id="bt" value="Gravar Todos os Lançamentos" /></div>
                    </div>

 <div class="row" align="left">
<div class="col-lg-4" align="left"></div>
<div class="col-lg-8" align="left">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>


<%end if%>
