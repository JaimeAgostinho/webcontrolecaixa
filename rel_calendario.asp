<%
'Function to Return the number of Days in a month
function findMonth(strDate, strYear)
	dim days
	if strDate = 4 or strDate = 6 or strDate = 9 or strDate = 11 then
		days = 30
	elseif strDate = 2 AND strYear/4 = int(strYear/4) then
		days = 29
	elseif strDate = 2 then
		days = 28
	else
		days = 31
	end if
	findMonth = days
end function
  vlr_ind_t = formatnumber(0,0)
  vlr_op = formatnumber(0,0)
  vlr_ind = formatnumber(0,0)

'Function will return the numeric value last or Next Month
function fnChangeMonth(strMonth, strDirection)
	if strDirection = "previous" then
		if strMonth = 1 then
			tempstrMonth = 12
		else
			tempstrMonth = strMonth - 1
		end if
	else
		if strMonth > 11 then
			tempstrMonth = 1
		else
			tempstrMonth = strMonth + 1
		end if
	end if
	fnChangeMonth = tempstrMonth
end function

'Function will return a date format from the qstring dd
'I use querystring called dd in this format 01012000 this just makes that
'into a date format
function formatQstring(strQstring)
	ddLength = Len(strQstring)
	tempYear = Right(strQstring,4)
	tempDay = Right(strQstring,6)
	tempDay = Left(tempday,2)
	tempMonth = Left(strQstring,ddLength - 6)
	
	strQstring = tempMonth & "/" & tempDay & "/" & tempYear
	formatQstring = formatdatetime(strQstring,2)
end function

'Find the numeric value of the first day in the month (Monday = 2...)
function formatFirstDay(strFirstDay)
	strFirstDay = WeekDay(Left(strFirstDay,2) & "/1/" & Right(strFirstDay,4))
	formatFirstDay = strFirstDay
end function

'Make the Hyperlink for Previous or Next Month
function makeLink(strDate, strLinkType)
	if strdate = "" then
		strdate = Month(DisplayDate) & "01" & Year(DisplayDate)
	end if
	
	theLength = len(strdate)
	theYear = Right(strdate,4) 
	theMonth = Left(strdate, theLength-6)
	
	if strLinkType = "previous" then
		theMonth = fnChangeMonth((Left(theMonth,2)),"previous")
		if theMonth = 12 then
			theYear = Right(strDate,4) - 1
		else
			theYear = Right(strDate,4)
		end if	
	else
		theMonth = fnChangeMonth((Left(theMonth,2)),"Next")
		if theMonth = 1 then
			theYear = Right(strDate,4) + 1
		else
			theYear = Right(strDate,4)
		end if	
	end if

	if len(theMonth) <> 2 then
		theMonth = "0" & theMonth
	end if
	
	strdate = theMonth & "01" & theYear
	makelink = strdate
end function

'Determine if there is a Calendar Request to show a month otherwise show this month
if Request("dd") = "" then
	DisplayDate = Date()
	ShowYear = Year(Date)
	FirstDayofMonth = WeekDay(Month(Date) & "/1/" & ShowYear)
else
'	ShowYear = Right(Request("dd"),4)
'	DisplayDate = formatQstring(Request("dd"))
'	FirstDayofMonth = WeekDay(DisplayDate)

	ShowYear = Right(Request("dd"),4)
	DisplayDate = formatQstring(Request("dd"))
	FirstDayofMonth = WeekDay(DisplayDate)

end if

previousMonth = findMonth(fnChangeMonth(Month(DisplayDate),"previous"), ShowYear) - FirstDayofMonth + 1
thisMonth = 0
nextMonth = 0
weekdaynum = 0

DisplayMonth = Month(DisplayDate)
If len(DisplayMonth) <> 2 then
	DisplayMonth = "0" & DisplayMonth
end if
DisplayYear  = Right((DisplayDate),4)
%>
<p align="center"><strong></strong></p>
<TABLE BORDER=1 align="center" CELLPADDING=0 CELLSPACING=0 bordercolor="#CCCCCC">
  <TR bgcolor="#CCCCCC"> 
    <td colspan="8"> 
      <div align="center"><b> <%=UCase(MonthName(month(DisplayDate), 0)) %>&nbsp;<%=ShowYear %></b></div></td>
  </TR><%=vbcr%>
<TR>
    <TD align="center" class="date" width="40" height="40">DOM</TD>
    <TD width="40" height="40" align="center" class="date">SEG</TD>
    <TD width="40" height="40" align="center" class="date">TER</TD>
    <TD width="40" height="40" align="center" class="date">QUA</TD>
    <TD width="40" height="40" align="center" class="date">QUI</TD>
    <TD width="40" height="40" align="center" class="date">SEX</TD>
    <TD width="40" height="40" align="center" class="date">SAB</TD>
    <TD width="40" height="40" align="center" class="date">Geral</TD>

  </TR>
<%
for tablecell = 1 to 42
	if weekdaynum = 7 then
		weekdaynum = 0
	end if
	weekdaynum = weekdaynum + 1
	inc = inc + 1
	if inc < FirstDayofMonth then
		previousMonth = previousMonth + 1
%>
    <TD width="60" height="40" valign="top" class="dateother"><%=previousMonth%><br>
      <em><%

	
					mes	= DisplayMonth
					'mes	=month(date())
					mes	= mes - 1
					if mes =< 9 then
						mes = "0"&mes
					end if
					
					data	= DisplayYear&"-"&mes&"-"&previousMonth
				'	response.Write "|"&data&"|"
					Set objconn_cons = Server.CreateObject("ADODB.Connection")
					objconn_cons.open = StringConexaoBD()
					instrucao_sql_cons = "select count(codigo) as quant from movimfull where dt_trans = '"&data&"' "
					'instrucao_sql_cons = "select count(codigo) as quant from movimentacao where data_transmissao = '2005-11-7' "
					set dados_cons = objconn_cons.execute(instrucao_sql_cons)
					'response.Write "|"&instrucao_sql_cons&"|"
						if dados_cons.EOF then 
						else
							if dados_cons("quant") <> "0" then
								response.write(dados_cons("quant"))
							end if
						end if
					objconn_cons.close
					set dados_cons = nothing 
					%>
      </em></TD>
    <%=vbcr%>
<%	elseif thisMonth < findMonth(DisplayMonth, ShowYear) then
		thisMonth = thisMonth + 1
%>
    <TD width="60" height="40" valign="top" class="date">
		        <span class="calendario2"><%=thisMonth%></span>
		<br>
				<%
					dia	= thisMonth
					if dia =< 9 then
						dia = "0"&dia
					end if
					%>
     				 <%
					data	= DisplayYear&"-"&DisplayMonth&"-"&thisMonth
					'response.Write "|"&data&"|"
					Set objconn_cons = Server.CreateObject("ADODB.Connection")
					objconn_cons.open = StringConexaoBD()
					instrucao_sql_cons = "select count(m.codigo) as quant, count(distinct(m.coban)) as ag from movimfull m, cad_coban c where c.codigo_coban=m.coban and m.dt_trans = '"&data&"' "
					'instrucao_sql_cons = "select count(codigo) as quant from movimentacao where data_transmissao = '2005-11-7' "		
'								response.Write "|"&instrucao_sql_cons&"|"
						set dados_cons = objconn_cons.execute(instrucao_sql_cons)
						if dados_cons.EOF then 
						else
							if dados_cons("quant") <> "0" then
							vlr_t = ( int(vlr_t) + formatnumber(dados_cons("quant")) )
							%>
								      <br><a href="adm.asp?k=<%=request.cookies("key")("chave")%>&url=&fun=con&pg=0&ago=1&d=tr&forma_con=-&data=<%=dia%>/<%=DisplayMonth%>/<%=DisplayYear%>" > <%=dados_cons("quant")%></a><br>
								      Ativo:<%=AtivosDia(data)%><br>Aut.:
	  <%=dados_cons("ag")%>
                                      
<%
if AtivosDia(data) > 10 then
vlr_ind_t = vlr_ind_t + 1
	response.Write("<br>"&formatnumber((formatnumber(dados_cons("ag")) / formatnumber(AtivosDia(data)))*100, 2)&"%")
	vlr_ind = ((formatnumber(vlr_ind)*1) + formatnumber(formatnumber((formatnumber(dados_cons("ag")) / formatnumber(AtivosDia(data)))*100, 2)))
	vlr_op = ((formatnumber(vlr_op)) + (formatnumber(dados_cons("ag"))*1) )
'response.Write("["&vlr_op&"]")   
end if
							end if
						end if
					objconn_cons.close
					set dados_cons = nothing 
%>
	</TD><%=vbcr%>
<%	else
		nextMonth = nextMonth + 1
%>
    <TD width="60" height="40" valign="top" class="dateother"><%=nextMonth%> <br>
      <%
					mesa	= DisplayMonth
					mesa	= mesa + 1
					if mesa =< 9 then
						mesa = "0"&mesa
					end if
					data	= DisplayYear&"-"&mesa&"-"&nextMonth
					Set objconn_cons = Server.CreateObject("ADODB.Connection")
					objconn_cons.open = StringConexaoBD()
					instrucao_sql_cons = "select count(codigo) as quant from movimfull where dt_trans = '"&data&"' "
					'instrucao_sql_cons = "select count(codigo) as quant from movimentacao where data_transmissao = '2005-11-7' "
					set dados_cons = objconn_cons.execute(instrucao_sql_cons)
						if dados_cons.EOF then 
						else
							if dados_cons("quant") <> "0" then
								response.write(dados_cons("quant"))
							end if
						end if
					objconn_cons.close
					set dados_cons = nothing 
					%>
    </TD>
    <%=vbcr%>
<%	end if
	if tablecell/7 = int(tablecell/7) then
%>
    <TD width="60" height="40" valign="top" class="dateother"><div align="center"><BR>
        <%
  if vlr_t <> 0 then
    vlr_tot = vlr_tot + vlr_t 
	  response.Write(vlr_t)
  end if	
  vlr_t = formatnumber(0,0)
  
  %>
      </div></TD>
		</tr><tr><%=vbcr%>
<%	end if
Next
%>

<TR>
    <TD colspan="8" height="40">
        <table width="100%"  border="0" align="center">
        <tr> 
          <td>&nbsp;</td>
          <td align="right"><strong> 
          
			<%	  
            response.Write("Total: "&formatnumber(vlr_tot,0)&"<br>Aut.: "&formatnumber(vlr_op/(vlr_ind_t),0)&"<br>"&formatnumber((vlr_ind/(vlr_ind_t)),2)&"%")
			
            vlr_tot = ""
            %>
            </strong></td>
        </tr>
        <tr> 
          <td width="159"><div align="center"><a href="adm.asp?k=<%=request.cookies("key")("chave")%>&url=<%=request("url")%>&dd=
<%
Adia	= mid(request("dd"),1,2)
Ames = mid(request("dd"),3,2)
Aano = mid(request("dd"), 5,4)
Adata	= Adia&"/"&Ames&"/"&Aano

Bdia	= mid(DateSerial(Year(Adata),Ames - 1,1),1,2)
Bmes	= mid(DateSerial(Year(Adata),Ames - 1,1),4,2)
Bano	= mid(DateSerial(Year(Adata),Ames - 1,1),7,4)
Bdate	= Bdia&Bmes&Bano

response.Write(Bdate)
%>
			
			
"> Anterior</A> </div></td>
          <td width="159"><div align="center"><a href="adm.asp?k=<%=request.cookies("key")("chave")%>&url=<%=request("url")%>&dd=
			<%
Jdia	= mid(request("dd"),1,2)
Jmes = mid(request("dd"),3,2)
Jano = mid(request("dd"), 5,4)
Jdata	= Jdia&"/"&Jmes&"/"&Jano

Kdia	= mid(DateSerial(Year(Jdata),Jmes + 1,1),1,2)
Kmes	= mid(DateSerial(Year(Jdata),Jmes + 1,1),4,2)
Kano	= mid(DateSerial(Year(Jdata),Jmes + 1,1),7,4)
Kdate	= Kdia&Kmes&Kano

response.Write(Kdate)
%>">Proximo</A></div></td>
        </tr>
      </table>
    </TD>
  <TR>
</TABLE>
