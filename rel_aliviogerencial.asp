<!--#include file="global.asp"-->

<%
if request("gr") = "s" then
'MysqlInsert	

	MysqlInsert("REPLACE INTO financeiro_lj_gtv ( data, lojabb, datacx, total_gtv, valor_apurado, diferenca, deposito_agencia, deposito_cso, residuo, alivio, diferenca_alivio, observacao, std, protocolo) VALUES (CURRENT_TIMESTAMP, '"&request.querystring("LojaBB")&"', '"&ConvData(request.querystring("Data"))&"', '"&BdValor(request.form("total_gtv"))&"', '"&BdValor(request.form("valor_apurado"))&"', '"&request.form("diferenca")&"', '"&BdValor(request.form("deposito_agencia"))&"', '"&BdValor(request.form("deposito_cso"))&"', '"&BdValor(request.form("residuo"))&"', '"&BdValor(request.form("alivio"))&"', '"&BdValor(request.form("diferenca_alivio"))&"', '"&request.form("observacao")&"', '"&request.form("std")&"', '"&request.form("protocolo")&"')" )
 	response.Redirect("rel_aliviogerencial.asp?data="&request.querystring("Data")&"&lojabb="&request.querystring("LojaBB")&"&resp=s")
end if
%>

<html lang="pt-br">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Sys PDV">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">

    <title>Fechamento de Caixa Loja</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <style type="text/css">
   body{margin-top:10px;}
   .glyphicon { margin-right:6px; }
   .card-block a{text-decoration: none;}
   .collapse .card-block.disabled a{color:#c5c5c5;}
   .collapse .card-block.disabled, .collapse .card-block.disabled span{color:#c5c5c5;}
    </style>
</head>
<body>

<script type="text/javascript">
function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
	var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
    return false;
}


function MascApurado(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
	objTextBox = document.acesso.valor_apurado;
	document.acesso.valor_apurado.value = document.acesso.valor_apurado.value.replace(".",",")
	var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
    Func_CalcDifApu();
    return false;
}

function Func_CalcDifApu()
{
	var s1 = document.getElementById("total_gtv").value.replace(".", "");
	s1 = parseFloat(s1.replace(",","."));

	var s2 = document.getElementById("valor_apurado").value.replace(".", "");
	s2 = parseFloat(s2.replace(",","."));

	var calc = (s2 - s1);
	calc = calc.toFixed(2);
	
	document.acesso.diferenca.value = calc;
}

</script>

<%
'Analisa se ja existe registro na tabela

	Set objconnAcons = Server.CreateObject("ADODB.Connection")
	objconnAcons.open = StringConexaoBDCons()
	instrucao_sqlAcons = "SELECT * FROM financeiro_lj_gtv f where datacx='"&ConvData(request.querystring("Data"))&"' and lojabb='"&request.querystring("LojaBB")&"'"
	set dadosAcons = objconnAcons.execute(instrucao_sqlAcons)
	if dadosAcons.EOF then
        cd = 0
		total_gtv = PesqResp("SELECT tot_gtv as Resp FROM financeiro_lj where lojabb='"&request.querystring("LojaBB")&"' and datacx='"&ConvData(request.querystring("Data"))&"'")
		valor_apurado = "0"
		diferenca = (valor_apurado - formatcurrency(total_gtv))
		deposito_agencia = "0"
		deposito_cso  = "0"
		residuo = "0"
		alivio = PesqResp("SELECT sum(valor) as Resp FROM cbf801repasse  where mci = '211217963' and lojabb='"&request.querystring("LojaBB")&"' and data > '"&ConvData(request.querystring("Data"))&"' group by data order by data limit 0,1")
		if alivio <> "" then
			alivio = formatnumber(alivio,2)
		else
			alivio = formatnumber("0",2)
		end if

		diferenca_alivio  = "0"
		observacao = ""
        std = ""
        protocolo = ""

	else
        cd = dadosAcons("codigo")
		total_gtv =dadosAcons("total_gtv")
		valor_apurado =dadosAcons("valor_apurado")
		diferenca =dadosAcons("diferenca")
		deposito_agencia =dadosAcons("deposito_agencia")
		deposito_cso  =dadosAcons("deposito_cso")
		residuo =dadosAcons("residuo")
		alivio =dadosAcons("alivio")
		diferenca_alivio  =dadosAcons("diferenca_alivio")
		observacao =dadosAcons("observacao")
        std = dadosAcons("std")
        protocolo = dadosAcons("protocolo")

	end if
diferenca = formatnumber(diferenca,2)
diferenca = replace(diferenca, ".", "")
diferenca = replace(diferenca, ",", ".")

%>

<% if request("resp") ="s" then%>
<div class="alert alert-success">
  <strong>Sucesso!</strong> Informações gravada com sucesso!
</div>

<%end if%>

<form id="acesso" name="acesso" method="post" action="rel_aliviogerencial.asp?data=<%=request.querystring("Data")%>&lojabb=<%=request.querystring("LojaBB")%>&gr=s">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-9 table-bordered">
        <div class="row text-xs-center"><b>CONCILIAR GTV</b></div>
        <div class="row">
            <div class="col-lg-12 table-bordered"><b>Loja:</b> <%=request.querystring("LojaBB")%> - <%=response.write(NomeLojaBB(""&request.querystring("LojaBB")&"", "nome"))%></div>
        </div>
        <div class="row">
            <div class="col-lg-12 table-bordered"><b>Data do Caixa:</b> <%=request.querystring("Data")%></div>
        </div>
        <div class="row">
            <div class="col-lg-12"><hr></div>
        </div>
        <div class="row table-bordered">
            <div class="col-xs-1 "></div>
            <div class="col-xs-5 ">TOTAL DE GTV:</div>
            <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(total_gtv,2)%>" readonly name="total_gtv" id="total_gtv"   aria-describedby="sizing-addon2"></div>
        </div>
        <div class="row table-bordered">
            <div class="col-xs-1 "></div>
            <div class="col-xs-5 " nowrap>VALOR APURADO:</div>
            <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(valor_apurado,2)%>"  name="valor_apurado" id="valor_apurado"   aria-describedby="sizing-addon2" onKeyPress="return(MascApurado(this,'.',',',event))" ></div>
        </div>
        <div class="row table-bordered">
            <div class="col-xs-1 "></div>
            <div class="col-xs-5 ">DIFERENÇA:</div>
            <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=diferenca%>" readonly name="diferenca" id="diferenca"   aria-describedby="sizing-addon2" ></div>
        </div>
        <div class="row">
            <div class="col-lg-12"><hr></div>
        </div>
        <div class="row table-bordered">
            <div class="col-xs-1 "></div>
            <div class="col-xs-5 " nowrap>PROTOCOLO:</div>
            <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=protocolo%>"  name="protocolo" id="protocolo"   aria-describedby="sizing-addon2"  ></div>
        </div>
        <div class="row table-bordered">
            <div class="col-xs-1 "></div>
            <div class="col-xs-5 " nowrap>DEPÓSITO (AGÊNCIA):</div>
            <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(deposito_agencia,2)%>"  name="deposito_agencia" id="deposito_agencia"   aria-describedby="sizing-addon2" onKeyPress="return(MascaraMoeda(this,'.',',',event))" ></div>
        </div>
        <div class="row table-bordered">
            <div class="col-xs-1 "></div>
            <div class="col-xs-5 " nowrap>DEPÓSITO (CSO):</div>
            <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(deposito_cso,2)%>"  name="deposito_cso" id="deposito_cso"   aria-describedby="sizing-addon2" onKeyPress="return(MascaraMoeda(this,'.',',',event))" ></div>
        </div>
        <div class="row table-bordered">
            <div class="col-xs-1 "></div>
            <div class="col-xs-5 " nowrap>RESIDUO FINANCEIRO:</div>
            <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(residuo,2)%>"  name="residuo" id="residuo"   aria-describedby="sizing-addon2" onKeyPress="return(MascaraMoeda(this,'.',',',event))" ></div>
        </div>
        <div class="row">
            <div class="col-lg-12"><hr></div>
        </div>
        <div class="row table-bordered">
            <div class="col-xs-1 "></div>
            <div class="col-xs-5 " nowrap>ALÍVIO SENSIBILIZADO:</div>
            <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(alivio,2)%>" readonly name="alivio" id="alivio"   aria-describedby="sizing-addon2"></div>
        </div>
        <div class="row table-bordered">
            <div class="col-xs-1 "></div>
            <div class="col-xs-5 " nowrap>DIFERENÇA DE ALÍVIO:</div>
            <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=formatnumber(diferenca_alivio,2)%>"  name="diferenca_alivio" id="diferenca_alivio"   aria-describedby="sizing-addon2" onKeyPress="return(MascaraMoeda(this,'.',',',event))"></div>
        </div>
        <div class="row table-bordered">
            <div class="col-xs-1 "></div>
            <div class="col-xs-5 " nowrap>Status Conciliação:</div>
            <div class="col-xs-6 input-group-sm">
                <select class="form-control" name="std" id="std">
                  <option value="Alívio Pendente" <% if std = "Alívio Pendente" then%>selected<%end if%>>Alívio Pendente</option>
                  <option value="Divergência" <% if std= "Divergência" then%>selected<%end if%>>Divergência</option>
                  <option value="Sensibilizado" <% if std = "Sensibilizado" then%>selected<%end if%>>Sensibilizado</option>
                  <option value="Regularizado" <% if std = "Regularizado" then%>selected<%end if%>>Regularizado</option>
                </select>
            </div>
        </div>
        <div class="row table-bordered">
            <div class="col-xs-1 "></div>
            <div class="col-xs-5 ">OBSERVAÇÃO:</div>
            <div class="col-xs-6 input-group-sm"><input type="text" class="form-control" value="<%=observacao%>"  name="observacao" id="observacao"   aria-describedby="sizing-addon2"></div>
        </div>
        <div class="row">
            <div class="col-lg-12"><br></div>
        </div>
        <div class="row table-bordered">
		 <div class="col-xs-12  btn-group-sm"><input class="btn btn-success btn-lg btn-block" type="submit" name="bt" id="bt" value="Gravar Conferência!" /></div>   
        </div>




        </div>
        </div>
        </div>


<br>
HISTORICO DE LANCAMENTOS:<BR>
<%
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT * FROM hist_retaguarda where funcao = 'Gtv"&cd&"'"
 set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF
response.write(dadosACi("data")&" - "&dadosACi("descricao")&"</br>")
dadosACi.MoveNext
Wend
end if
%>     

</form>
</body>
</html>



