<script type="text/javascript">
	function enviar(){
		if(document.getElementById("lojabb").value != "" ){
			document.getElementById("acesso").submit()
		} else {
			alert('Informe uma loja!')	
		}
		
	}


function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
	if(document.getElementById("valor").readOnly == false){
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
	}
    return false;
}

</script>

<%=CabecalhoUrl("Relatório de Operações Recorrentes")%>
<form id="acesso" name="acesso" method="post" action="adm.asp?url=rel_fluxofinanceiro&Orb=var&gr=v">
<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="4"><table width="100%" border="1" cellspacing="0" cellpadding="3" bordercolor="#999999">
      <tr>
        <td><table width="100%" border="0" cellspacing="3" cellpadding="0">
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>Loja:</td>
            <td><label for="lojabb"></label>
              <input name="lojabb" type="text" id="lojabb" value="<%=request("lojabb")%>" size="10" maxlength="6" /> Todas:%</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right">&nbsp;</td>
          </tr>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">&nbsp;</td>
          </tr>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">Data inicio:</td>
            <td nowrap="nowrap"><label for="textfield"></label>
              <input name="dataA" type="text" id="dataA" onkeypress="mascaraGeral(this, '##/##/####');return onlyDigit(event)"  value="<% if request("dataA") <> "" then%><%=request("dataA")%><%else%><%=date()-1%><%end if%>" size="10" maxlength="10"  />             
              &nbsp; </td>
            <td nowrap="nowrap">Data Final</td>
            <td nowrap="nowrap"><input name="dataB"  type="text" id="dataB" onkeypress="mascaraGeral(this, '##/##/####');return onlyDigit(event)"  value="<% if request("dataB") <> "" then%><%=request("dataB")%><%else%><%=date()-1%><%end if%>" size="10" maxlength="10"  />
              &nbsp;</td>
            <td nowrap="nowrap"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="6" align="center"><input type="button" name="bt" onclick="enviar()" value="Consultar Operações"/></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>

<% if request("gr") = "v" then%>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr align="right">
    <td><a href="javascript:imprimir()"><img src="imagem/fc_pri.gif" alt="Imprimir Relat&oacute;rio" width="16" height="16" border="0"></a> <% =response.write("<a href='ArquivoTemp/FluxoFinantemp_"&request.cookies("sismy")("loguin")&".csv'><img src=""imagem/excel.jpg"" border=""0"" Title = ""Criado em " & now() & """></a>")%></td>
  </tr>
</table>
<table width="80%" align="center" cellpadding="0" cellspacing="1" class="sortable" id="playlist">
<%
session("208") = "Depósito em dinheiro"
session("210") = "Depósito em dinheiro e cheque"
session("216") = "Saque cartão"
session("296") = "Pagamento de benefício INSS"
session("284") = "Saque poupança"

session("T208") = "DP"
session("T210") = "DP"
session("T216") = "SQ"
session("T296") = "SQ"
session("T284") = "SQ"

%>



<%
tot = 0
nome = ""
DataInici = mid(request("dataA"), 7,4)&mid(request("dataA"), 4,2)&mid(request("dataA"), 1,2)
DataFim = mid(request("dataB"), 7,4)&mid(request("dataB"), 4,2)&mid(request("dataB"), 1,2)

Set objconn = Server.CreateObject("ADODB.Connection")
objconn.open = StringConexaoBDCons()
'instrucao_sql = "SELECT dh_operacao as DH_Operacao, dt_movim as DT_Movimento, chavej as ChaveJ, nsu as NrAut, produto as Servico, valor as Valor, cdbarra FROM cbf801full c where produto in ('208','210','216', '296', '284') and dt_trans >= '"&DataInici&"' and dt_trans <= '"&DataFim&"' and mci='211217963' and lojabb = '"&request("lojabb")&"' order by dh_operacao"
instrucao_sql = "SELECT c.dh_operacao as DH_Operacao, c.dt_movim as DT_Movimento, c.chavej as ChaveJ, c.nsu as NrAut, c.produto as Servico, c.valor as Valor, c.cdbarra as CdBarra , c.lojabb, (select cdbarra from acbfull a where a.dt_trans=c.dt_trans and a.chavej=c.chavej and a.convenio='49093' and a.lojabb=c.lojabb and a.nsu=c.nsu) as Dados FROM cbf801full c where  c.produto in ('208','210','216', '296', '284') and c.dt_trans >= '"&DataInici&"' and c.dt_trans <= '"&DataFim&"' and c.mci='211217963' and c.lojabb like '"&request("lojabb")&"' order by dh_operacao"
'response.Write(instrucao_sql)
set dados = objconn.execute(instrucao_sql)
arquivo_excel= "ArquivoTemp/FluxoFinantemp_"&request.cookies("sismy")("loguin")&".csv"
set fso = createobject("scripting.filesystemobject")
Set act = fso.CreateTextFile(server.mappath(arquivo_excel), true)
act.WriteLine("DH_Operacao;DT_Movimento;Lojabb;ChaveJ;NrAut;TIPO;Servico;Valor;NOME")
%>
  <thead>
    <tr bgcolor="#CCCCCC">
      <th align="left" id="<%=dados.fields(0).name%>"> <a href="#" class="sortheader" onclick="ts_resortTable(this);return false;"><%=dados.fields(0).name%></a></th>
      <th align="left" id="<%=dados.fields(1).name%>"> <a href="#" class="sortheader" onclick="ts_resortTable(this);return false;"><%=dados.fields(1).name%></a></th>
      <th align="left" id="<%=dados.fields(7).name%>"> <a href="#" class="sortheader" onclick="ts_resortTable(this);return false;"><%=dados.fields(7).name%></a></th>
      <th align="left" id="<%=dados.fields(2).name%>"> <a href="#" class="sortheader" onclick="ts_resortTable(this);return false;"><%=dados.fields(2).name%></a></th>
      <th align="right" id="<%=dados.fields(3).name%>"> <a href="#" class="sortheader" onclick="ts_resortTable(this);return false;"><%=dados.fields(3).name%></a></th>
      <th align="right" id="<%=dados.fields(4).name%>"><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">TIPO</a></th>
      <th align="right" id="<%=dados.fields(4).name%>"> <a href="#" class="sortheader" onclick="ts_resortTable(this);return false;"><%=dados.fields(4).name%></a></th>
      <th align="right" id="<%=dados.fields(5).name%>"> <a href="#" class="sortheader" onclick="ts_resortTable(this);return false;"><%=dados.fields(5).name%></a></th>
      <th align="right" id="<%=dados.fields(6).name%>"><a href="#" class="sortheader" onclick="ts_resortTable(this);return false;">NOME</a></th>

    </tr>
  </thead>
<% if dados.EOF then %>
    <tr valign="middle" bgcolor="<%=CorLinhaRelatorio()%>">
      <td colspan="9">Não existe Informações para esta pesquisa!</td>
    </tr>
<% else %>
<% dados.MoveFirst %>
<tbody>
<% 
While Not dados.EOF 
Dd = ""
%>
    <tr valign="middle" bgcolor="<%=CorLinhaRelatorio()%>">
      <td nowrap="nowrap"><%=dados("DH_Operacao")%></td>
      <td nowrap="nowrap"><%=dados("DT_Movimento")%></td>
      <td nowrap="nowrap"><%=dados("lojabb")%></td>
      <td nowrap="nowrap"><%=dados("ChaveJ")%></td>
      <td align="right" valign="middle" nowrap="nowrap"><%=dados("NrAut")%></td>
      <td align="center" valign="middle" nowrap="nowrap"><%=session("T"&dados("Servico")&"")%></td>
      <td align="left" valign="middle" nowrap="nowrap"><%=dados("Servico")%> - <%=session(""&dados("Servico")&"")%></td>
      <td align="left" nowrap="nowrap"><%=formatcurrency(dados("valor"),2)%></td>
      <td align="left" nowrap="nowrap">
	  <% if dados("Dados") <> "" then%>
	  	<%
		Dd = dados("Dados")
		response.Write(dados("Dados"))
		%>
	  <%else%>
	  	<%
		Dd=dados("CdBarra")
		response.Write(dados("CdBarra"))
		%>
	  <%end if%>
      
      </td>
    </tr>
<%
act.WriteLine(""&dados("DH_Operacao")&";"&dados("DT_Movimento")&";"&dados("lojabb")&";"&dados("ChaveJ")&";"&dados("NrAut")&";"&session("T"&dados("Servico")&"")&";"&dados("Servico")&"-"&session(""&dados("Servico")&"")&";"&formatcurrency(dados("valor"),2)&";"&Dd&";")       
dados.MoveNext
Wend
act.close
end if
%>
<tfoot>
    <tr valign="middle" bgcolor="<%=CorLinhaRelatorio()%>">
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>

    </tr>
</tfoot>    
  </tbody>
</table>


<%end if%>