<%
'******************************
'var app = angular.module('Orbistec', []);
'app.controller('customersCtrl', function($scope, $http, $timeout) {
'    $scope.getData = function(){
'	$http.get("request.asp?id=1").then(function(response) {
'        $scope.RelLojas = response.data.records;
'    });
'	};
'
'  // Function to replicate setInterval using $timeout service.
'  $scope.intervalFunction = function(){
'    $timeout(function() {
'      $scope.getData();
'      $scope.intervalFunction();
'    }, 1000)
'  };
'
'  // Kick off the interval
'  $scope.intervalFunction();
'  
'});
'******************************
%>

       <!--Importa as bibliotecas necessárias-->
        <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
        <script src="http://tablesorter.com/__jquery.tablesorter.min.js" type="text/javascript"></script> 

<% if request("log") = "s" then%>
<script>
var app = angular.module('Orbistec', []);
app.controller('customersCtrl', function($scope, $http, $timeout) {
	var TimerPesq = 1000;
    $scope.getData = function(){
	$http.get("request.asp?id=1&data=<%=ConvData(request.Form("data"))%>&orderby=<%=request("orderby")%>").then(function(response) {
        $scope.RelLojas = response.data;
		TimerPesq = 60000;
    });
	};

	$scope.ordenarPor = function (campo) {
		$scope.criterioDeOrdenacao = campo;
		$scope.direcaoDaOrdenacao = !$scope.direcaoDaOrdenacao;
	};

	$scope.$watchCollection('RelLojas',function() {
	  $scope.Sum_saldo_in = 0;  
	  $scope.Sum_rec = 0;  
	  $scope.Sum_pag = 0;  
	  $scope.Sum_alivio = 0;  
	  $scope.Sum_SldAntCon = 0;  
	  $scope.Sum_Gtv = 0;  
	  $scope.Sum_SldCx = 0;  
	  $scope.Sum_DifDev = 0;  
	  $scope.Sum_DifCre = 0;  
	  $scope.Sum_Retag = 0; 
	  $scope.Sum_FCsaldo_comp = 0; 
	  $scope.Sum_FCsaldo_cofreadm = 0; 
	  $scope.Sum_FCsaldo_bocadelobo = 0; 
	  $scope.SumRR = 0;
	  
	  angular.forEach($scope.RelLojas, function(value, key) {
		$scope.Sum_saldo_in += parseFloat(value.saldo_in);
		$scope.Sum_rec += parseFloat(value.rec);
		$scope.Sum_pag += parseFloat(value.pag);
		$scope.Sum_alivio += parseFloat(value.alivio);
		$scope.Sum_SldAntCon += parseFloat(value.SldAntCon);
		

		if(value.Gtv != "") { $scope.Sum_Gtv += parseFloat(value.Gtv);	 };

		$scope.Sum_SldCx += parseFloat(value.SldCx);
		$scope.Sum_DifDev += parseFloat(value.DifDev);
		$scope.Sum_DifCre += parseFloat(value.DifCre);
		
		if(value.FCsaldo_comp != "") { $scope.Sum_FCsaldo_comp += parseFloat(value.FCsaldo_comp);	 };
		if(value.FCsaldo_cofreadm != "") { $scope.Sum_FCsaldo_cofreadm += parseFloat(value.FCsaldo_cofreadm);	 };
		if(value.FCsaldo_bocadelobo != "") { $scope.Sum_FCsaldo_bocadelobo += parseFloat(value.FCsaldo_bocadelobo);	 };
		
		$scope.SumRR += (parseFloat(value.FCsaldo_comp)*1);
		
	  })
	});


  // Function to replicate setInterval using $timeout service.
<% if ConvData(request.Form("data")) <> ConvData(date()) then%>

    $scope.intervalFunction = function(){
    $timeout(function() {
      $scope.getData();
      $scope.intervalFunction();
    }, TimerPesq)
  };
  // Kick off the interval
  $scope.intervalFunction();

<%else%>

  $scope.intervalFunction = function(){
    $timeout(function() {
      $scope.getData();
      $scope.intervalFunction();
    }, TimerPesq)
  };
  // Kick off the interval
  $scope.intervalFunction();

<%end if%>
  
});
</script>
<%end if%>

<div class="row">
    <div class="col-sm-12">
		<%=CabecalhoUrl("Posição Diária Detalhada")%> 
    </div>
</div>

<form id="acesso" name="acesso" method="post" action="adm.asp?tc=<%=response.Write(request("tc"))%>&url=cleanup_beta&Orb=var&log=s">
<div class="row">

    <div class="col-sm-2">
	    <input class="form-control input-sm" type="text" name="data" id="data" value="<% if request("log") = "s" then%><%=request.form("data")%><%else%><%=response.Write(mid(now(),1,10))%><%end if%>"/>
    </div>
    <div class="col-sm-4">
	    <input class="form-control input-sm" type="text" ng-model="criterioDeBusca" placeholder="Informe a Loja!"/>
    </div>

    <div class="col-sm-2 btn-group-sm">
        <select class="form-control input-sm" id="orderby" name="orderby">
              <option value="LojaBB" <% if request("orderby") = "LojaBB" then%>selected<%end if%>>Loja</option>
              <option value="Ndata" <% if request("orderby") = "Ndata" then%>selected<%end if%>>Hora</option>
              <option value="stat" <% if request("orderby") = "stat" then%>selected<%end if%>>Status</option>
              <option value="saldo_in" <% if request("orderby") = "saldo_in" then%>selected<%end if%>>Remanescente</option>
              <option value="rec" <% if request("orderby") = "rec" then%>selected<%end if%>>Recebimento</option>
              <option value="pag" <% if request("orderby") = "pag" then%>selected<%end if%>>Pagamento</option>
              <option value="SldAntCon" <% if request("orderby") = "SldAntCon" then%>selected<%end if%>>Saldo Parcial</option>
              <option value="FCsaldo_comp" <% if request("orderby") = "FCsaldo_comp" then%>selected<%end if%>>CompuSafe</option>
              <option value="FCsaldo_cofreadm" <% if request("orderby") = "FCsaldo_cofreadm" then%>selected<%end if%>>Cofre ADM</option>
              <option value="FCsaldo_bocadelobo" <% if request("orderby") = "FCsaldo_bocadelobo" then%>selected<%end if%>>Boca Lobo</option>
            </select>                      
    </div>

    <div class="col-sm-1 btn-group-sm">
		<input class="btn btn-primary btn-lg btn-block" type="submit" name="bt" id="bt" value="Pesquisar" />
    </div>

</div>
</form>
<br>
<% if request("log") = "s" then%>
<div class="row">
<div ng-app="Orbistec" ng-controller="customersCtrl">
<div>
    <div class="col-sm-0"></div>
    <div class="col-sm-11">
    <table class="table table-striped table-bordered ">
	<thead>
        <tr> 
            <th>Loja</th>	
            <th NOWRAP>Nome</th>	
            <th NOWRAP align="right">Ult.OP</th>	
            <th NOWRAP align="right">Fech.</th>	
            <th NOWRAP align="right">Conf.</th>	
            <th NOWRAP align="right">Status</th>	
            <th NOWRAP align="right">Reman.</th>	
            <th align="right">Receb.</th>	
            <th align="right">Pagam.</th>	
            <th NOWRAP align="right">Alivios&nbsp;BB</th>	
            <th NOWRA Palign="right">Saldo&nbsp;Parcial</th>	
            <th NOWRAP align="right">GTV's</th>	
            <th NOWRAP align="right">Valor&nbsp;Caixa</th>	
            <th NOWRAP align="right">Dif.&nbsp;Caixa</th>
            <th align="right">Dif.&nbsp;Credora</th>	
            <th align="right">CompuSafe</th>	
            <th align="right">Cofre&nbsp;ADM</th>	
            <th align="right">BocaLobo</th>	
            <th align="right">Retaguarda</th>	
        </tr>
        </thead>
         <tbody>
          <tr ng-repeat="registro in RelLojas | filter:{lojabb:criterioDeBusca} | orderBy:criterioDeOrdenacao:direcaoDaOrdenacao">
            <td>{{registro.lojabb}}</td>	
            <td NOWRAP>{{registro.nome}}</td>	
            <td>{{registro.Ndata}}</td>	
            <td>{{registro.Fdata}}</td>	
            <td>{{registro.Cdata}}</td>	
            <td>
            <div ng-if="registro.StFc === ''">
                <div ng-if="registro.stat === 'Em Conferência'"><a href="rel_fech_loja.asp?cd={{registro.CdFc}}" target="new"><div style="color:#8000FF">Em Conferência</div></a></div>
                <div ng-if="registro.stat === 'Fechado'"><a href="rel_fech_loja.asp?cd={{registro.CdFc}}" target="new"><div style="color:#00F">Fechado </div></a></div>
                <div ng-if="registro.stat === 'Conferido'"><a href="rel_fech_loja.asp?cd={{registro.CdFc}}" target="new"><div style="color:#090">Conferido </div></a></div>
                <div ng-if="registro.stat === 'Aberto'"><div style="color:#F00">Aberto </div></div>
            </div>
                <div ng-if="registro.StFc === 'Em Conferência'"><a href="rel_fech_loja.asp?cd={{registro.CdFc}}" target="new"><div style="color:#8000FF">Em Conferência</div></a></div>
                <div ng-if="registro.StFc === 'Anexar'"><a href="rel_fech_loja.asp?cd={{registro.CdFc}}" target="new"><div style="color:#F60">Em Fechamento </div></a></div>
                <div ng-if="registro.StFc === 'Fechado'"><a href="rel_fech_loja.asp?cd={{registro.CdFc}}" target="new"><div style="color:#00F">Fechado </div></a></div>
                <div ng-if="registro.StFc === 'Conferido'"><a href="rel_fech_loja.asp?cd={{registro.CdFc}}" target="new"><div style="color:#090">Conferido </div></a></div>

            </td>	
            <td align="right" NOWRAP>{{registro.saldo_in | currency:'R$ '}}</td>	
            <td align="right" NOWRAP>{{registro.rec | currency:'R$ '}}</td>	
            <td align="right" NOWRAP>{{registro.pag | currency:'R$ '}}</td>	
            <td align="right" NOWRAP>{{registro.alivio | currency:'R$ '}}</td>	
            <td align="right" NOWRAP>{{registro.SldAntCon | currency:'R$ '}}</td>	
            <td align="right" NOWRAP>{{registro.Gtv | currency:'R$ '}}</td>	
            <td align="right" NOWRAP>{{registro.SldCx | currency:'R$ '}}</td>	
            <td align="right" NOWRAP>{{registro.DifDev | currency:'R$ '}}</td>
            <td align="right" NOWRAP>{{registro.DifCre | currency:'R$ '}}</td>	
            <td align="right" NOWRAP>{{registro.FCsaldo_comp | currency:'R$ '}}</td>	
            <td align="right" NOWRAP>{{registro.FCsaldo_cofreadm | currency:'R$ '}}</td>	
            <td align="right" NOWRAP>{{registro.FCsaldo_bocadelobo | currency:'R$ '}}</td>	
            <td align="right" NOWRAP>{{(registro.FCsaldo_comp*1)+(registro.FCsaldo_cofreadm*1)+(registro.FCsaldo_bocadelobo*1) | currency:'R$ '}}</td>	
        </tr>
       	</tbody>
        <tr>
            <td colspan="6" align="right">Total Lojas:</td>
            <td align="right">{{Sum_saldo_in | currency:'R$&nbsp;'}}</td>
            <td align="right">{{Sum_rec | currency:'R$&nbsp;'}}</td>
            <td align="right">{{Sum_pag | currency:'R$&nbsp;'}}</td>
            <td align="right">{{Sum_alivio | currency:'R$&nbsp;'}}</td>
            <td align="right">{{Sum_SldAntCon | currency:'R$&nbsp;'}}</td>
            <td align="right">{{Sum_Gtv | currency:'R$&nbsp;'}}</td>
            <td align="right">{{Sum_SldCx | currency:'R$&nbsp;'}}</td>
            <td align="right">{{Sum_DifDev | currency:'R$&nbsp;'}}</td>
            <td align="right">{{Sum_DifCre | currency:'R$&nbsp;'}}</td>
            <td align="right">{{Sum_FCsaldo_comp | currency:'R$&nbsp;'}}</td>
            <td align="right">{{Sum_FCsaldo_cofreadm | currency:'R$&nbsp;'}}</td>
            <td align="right">{{Sum_FCsaldo_bocadelobo | currency:'R$&nbsp;'}}</td>
            <td align="right">{{(Sum_FCsaldo_comp*1)+(Sum_FCsaldo_cofreadm*1)+(Sum_FCsaldo_bocadelobo*1) | currency:'R$ '}}</td>
        </tr>
    </table>
    </div>
    
</div>
</div>
</div>
<%end if%>