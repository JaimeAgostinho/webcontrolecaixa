<%
session("erro") = 0
Sub CheckError
 Select Case Err.Number
  Case 3
   session("erro") = " Return sem GoSub"
  Case 5
   session("erro") = "Chamada de Procedimento Inv�lida"
  Case 6
   session("erro") = "Sobrecarga"
  Case 7
   session("erro") = "Sem Mem�ria"
  Case 9
   session("erro") = "SubScript fora de �rea"
  Case 10
   session("erro") = "Este Array est� fixo ou temporariamente travado"
  Case 11
   session("erro") = "Divis�o Por Zero"
  Case 13
   session("erro") = "Tipos Incompat�veis"
  Case 14
   session("erro") = "Fora de Espa�o de String"
  Case 16
   session("erro") = "Express�o muito Complexa"
  Case 17
   session("erro") = "N�o pode recuperar a opera��o"
  Case 18
   session("erro") = "Interrup��o do usu�rio ocorrida"
  Case 20
   session("erro") = "Resume Without Error"
  Case 28
   session("erro") = "Fora de Espa�o de Pilha"
  Case 35
   session("erro") = "Sub ou Function n�o Definida"
  Case 47
   session("erro") = "Muitas DLL na aplica��o cliente"
  Case 48
   session("erro") = "Erro carregando DLL"
  Case 49
   session("erro") = "DLL com problemas de chamada"
  Case 51
   session("erro") = "Erro Interno"
  Case 52
   session("erro") = "Nome ou n�mero do arquivo errado"
  Case 53
   session("erro") = "Arquivo n�o Encontrado"
  Case 54
   session("erro") = "Modo de arquivo errado"
  Case 55
   session("erro") = "Arquivo j� est� Aberto"
  Case 57
   session("erro") = "Device I/O Error"
  Case 58
   session("erro") = "Arquivo j� existente"
  Case 59
   session("erro") = "Tamanho do registro errado"
  Case 61
   session("erro") = "Disco Cheio"
  Case 62
   session("erro") = "Entrada passa do final do arquivo"
  Case 63
   session("erro") = "N�mero de registros errados"
  Case 67
   session("erro") = "Muitos arquivos"
  Case 68
   session("erro") = "Ferramenta n�o dispon�vel"
  Case 70
   session("erro") = "Permiss�o Negada"
  Case 71
   session("erro") = "Disco n�o Preparado"
  Case 74
   session("erro") = "N�o posso renomear com discos diferentes"
  Case 75
   session("erro") = "Caminho/Arquivos Erro de acesso"
  Case 76
   session("erro") = "Caminho n�o encontrado"
  Case 91
   session("erro") = "Vari�vel de objeto n�o definida"
  Case 92
   session("erro") = "Loop For n�o foi inicializado"
  Case 94
   session("erro") = "Uso inv�lido de Null"
  Case 322
   session("erro") = "N�o posso criar Arquivos tempor�rios nescess�rios"
  Case 325
   session("erro") = "Formato inv�lido no arquivo"
  Case 380
   session("erro") = "Valor da propriedade inv�lida"
  Case 400
   session("erro") = "ERRO HTTP 1.1 --- pedido ruim"
  Case 401.1
   session("erro") = "ERRO HTTP 1.1 --- n�o autorizado: falha no logon"
  Case 401.2
   session("erro") = "ERRO HTTP 1.1 --- n�o autorizado: falha no logon devido a configura��o do servidor"
  Case 401.3
   session("erro") = "ERRO HTTP 1.1 --- n�o autorizado: n�o autorizado devido a ACL no recurso"
  Case 401.4
   session("erro") = "ERRO HTTP 1.1 --- n�o autorizado: falha na autoriza��o pelo filtro"
  Case 401.5
   session("erro") = "ERRO HTTP 1.1 --- n�o autorizado: falha na autoriza��o por ISAPI/CGI App"
  Case 403.1
   session("erro") = "ERRO HTTP 1.1 --- proibido: acesso a execu��o proibido"
  Case 403.2
   session("erro") = "ERRO HTTP 1.1 --- proibido: acesso de leitura proibido"
  Case 403.3
   session("erro") = "ERRO HTTP 1.1 --- proibido: acesso de escrever proibido"
  Case 403.4
   session("erro") = "ERRO HTTP 1.1 --- proibido: requer SSL"
  Case 403.5
   session("erro") = "ERRO HTTP 1.1 --- proibido: requer SSL 128"
  Case 403.6
   session("erro") = "ERRO HTTP 1.1 --- proibido: endere�o de IP rejeitado"
  Case 403.7
   session("erro") = "ERRO HTTP 1.1 --- proibido: requer certifica��o do cliente"
  Case 403.8
   session("erro") = "ERRO HTTP 1.1 --- proibido: acesso ao site negado"
  Case 403.9
   session("erro") = "ERRO HTTP 1.1 --- acesso proibido: Muitos usu�rios est�o conectados"
  Case 403.10
   session("erro") = "ERRO HTTP 1.1 --- acesso proibido: configura��o inv�lida"
  Case 403.11
   session("erro") = "ERRO HTTP 1.1 --- acesso proibido: senha alterada"
  Case 403.12
   session("erro") = "ERRO HTTP 1.1 --- acesso proibido: negado acesso ao mapa"
  Case 404
   session("erro") = "ERRO HTTP 1.1 --- n�o encontrado"
  Case 405
   session("erro") = "ERRO HTTP 1.1 --- m�todo n�o permitido"
  Case 406
   session("erro") = "ERRO HTTP 1.1 --- n�o aceit�vel"
  Case 407
   session("erro") = "ERRO HTTP 1.1 --- requer autentica��o do Proxy"
  Case 412
   session("erro") = "ERRO HTTP 1.1 --- falha em pr� condi��es"
  Case 414
   session("erro") = "ERRO HTTP 1.1 --- pedido - URI muito grande"
  Case 423
   session("erro") = "Propriedade ou metodo n�o encontrado"
  Case 424
   session("erro") = "Objeto Requerido"
  Case 429
   session("erro") = "OLE Automation n�o pode ser criado no servidor"
  Case 430
   session("erro") = "Classe n�o suportada pelo OLE Automation"
  Case 432
   session("erro") = "Nome do arquivo ou de classe n� encontrado durante a opera��o OLE Automation"
  Case 438
   session("erro") = "Objeto n�o suporta esta propriedade ou m�todo"
  Case 440
   session("erro") = "Erro na OLE Automation"
  Case 442
   session("erro") = "Connection to type library or object library for remote process has been lost. Press OK for dialog to remove reference"
  Case 443
   session("erro") = "Objeto OLE Automation n�o cont�m um valor padr�o"
  Case 445
   session("erro") = "Objeto n�o suporta esta a��o"
  Case 446
   session("erro") = "Objeto n�o suporta o nome do argumento"
  Case 447
   session("erro") = "Objeto n�o suporta a defini��o do local atual"
  Case 448
   session("erro") = "Nome de argumentos n�o encontrados"
  Case 449
   session("erro") = "Este argumento n�o � opcional"
  Case 450
   session("erro") = "N�mero de argumentos errado ou defini��o de propriedade inv�lida"
  Case 451
   session("erro") = "Objeto n�o � uma cole��o"
  Case 452
   session("erro") = "N�mero ordinal inv�lido"
  Case 453
   session("erro") = "Fun��o DLL especificada n�o foi encontrada"
  Case 454
   session("erro") = "c�digo de origem n�o encontrado"
  Case 455
   session("erro") = "Erro de trava no c�digo"
  Case 457
   session("erro") = "Esta chave j� est� associada a um elemento desta cole��o"
  Case 458
   session("erro") = "Tipos de vari�veis usadas na OLE Automation n�o s�o suportadas pelo Visual Basic"
  Case 462
   session("erro") = "A m�quina do servidor remoto n�o existe ou n�o est� dispon�vel"
  Case 481
   session("erro") = "Figura Inv�lida"
  Case 500
   session("erro") = "Vari�vel n�o definida"
  Case 501
   session("erro") = "Vari�vel n�o pode ser atribu�da"
  Case 502
   session("erro") = "Objeto n�o � seguro para script"
  Case 503
   session("erro") = "Objeto n�o � seguro para inicializa��o"
  Case 504
   session("erro") = "Objeto n�o � seguro para cria��o"
  Case 505
   session("erro") = "Refer�ncia inv�lida ou n�o qualificada"
  Case 506
   session("erro") = "Classe n�o definida"
  Case 1001
   session("erro") = "Sem mem�ria"
  Case 1002
   session("erro") = "Erro de Sintaxe"
  Case 1003
   session("erro") = "Esperado ':'"
  Case 1004
   session("erro") = "Esperado ';'"
  Case 1005
   session("erro") = "Esperado '('"
  Case 1006
   session("erro") = "Esperado ')'"
  Case 1007
   session("erro") = "Esperado ']'"
  Case 1008
   session("erro") = "Esperado '{'"
  Case 1009
   session("erro") = "Esperado '}'"
  Case 1010
   session("erro") = "Esperado Identificador"
  Case 1011
   session("erro") = "Esperado '='"
  Case 1012
   session("erro") = "Esperado 'If'"
  Case 1013
   session("erro") = "Esperado 'To'"
  Case 1014
   session("erro") = "Esperado 'End'"
  Case 1015
   session("erro") = "Esperado 'Function'"
  Case 1016
   session("erro") = "Esperado 'Sub'"
  Case 1017
   session("erro") = "Esperado 'Then'"
  Case 1018
   session("erro") = "Esperado 'Wend'"
  Case 1019
   session("erro") = "Esperado 'Loop'"
  Case 1020
   session("erro") = "Esperado 'Next'"
  Case 1021
   session("erro") = "Esperado 'Case'"
  Case 1022
   session("erro") = "Esperado 'Select'"
  Case 1023
   session("erro") = "Esperado express�o"
  Case 1024
   session("erro") = "Esperado declara��o"
  Case 1025
   session("erro") = "Esperado final da declara��o"
  Case 1026
   session("erro") = "Esperado inteiro constante"
  Case 1027
   session("erro") = "Esperado 'While' , 'Until'"
  Case 1028
   session("erro") = "Esperado 'While' , 'Until' ou final de declara��o"
  Case 1029
   session("erro") = "Esperado 'With'"
  Case 1030
   session("erro") = "Identificador Muito Longo"
  Case 1031
   session("erro") = "N�mero Inv�lido"
  Case 1032
   session("erro") = "Caracter Inv�lido"
  Case 1033
   session("erro") = "Constante de String n�o Terminada"
  Case 1034
   session("erro") = "Coment�rio n�o Terminado"
  Case 1035
   session("erro") = "Nested Comment"
  Case 1036
   session("erro") = "'Me' n�o pode ser usado como sa�da de rotina"
  Case 1037
   session("erro") = "Uso Inv�lido da Palavra Chave 'Me'"
  Case 1038
   session("erro") = "'Loop' sem 'Do'"
  Case 1039
   session("erro") = "Declara��o 'Exit' Inv�lida"
  Case 1040
   session("erro") = "Vari�vel de Controle de Loop 'for' Inv�lida"
  Case 1041
   session("erro") = "Vari�vel Redefinida"
  Case 1042
   session("erro") = "Tem que ser a primeira declara��o da linha"
  Case 1043
   session("erro") = "N�o pode atribuir non-By Val para um argumento"
  Case 1044
   session("erro") = "N�o pode usar par�ntesis para chamar uma sub"
  Case 1045
   session("erro") = "Esperada Constante Literal"
  Case 1046
   session("erro") = "Esperado 'In'"
  Case 1047
   session("erro") = "Esperado 'Class'"
  Case 1048
   session("erro") = "Tem que ser definido dentro de uma Classe"
  Case 1049
   session("erro") = "Esperado Let ou Set ou Get na declara��o de propriedade"
  Case 1050
   session("erro") = "Esperado 'Property'"
  Case 1051
   session("erro") = "N�mero de argumentos tem que ser consistente em especifica��es de propriedades"
  Case 1052
   session("erro") = "N�o pode haver m�todo/ propriedade padr�o m�ltiplo em uma Classe"
  Case 1053
   session("erro") = "Class initialize ou terminate n�o tem argumentos"
  Case 1054
   session("erro") = "Propriedade Set ou Let tem que ter pelo menos um argumento"
  Case 1055
   session("erro") = "'Next' inesperado"
  Case 1056
   session("erro") = "'Default' pode ser especificado somente em 'Property' ou 'Function' ou 'Sub'"
  Case 1057
   session("erro") = "Especifica��o 'Default' precisa especificar tamb�m 'Public'"
  Case 1058
   session("erro") = "Especifica��o 'Default' s� pode estar em Property Get"

  Case 3000
   session("erro") = "O provedor n�o concluiu a a��o pedida"
  Case 3001
   session("erro") = "A aplica��o est� usando argumentos do tipo errado, est�o fora do �mbito aceit�vel ou em conflito com alguma outra aplica��o"
  Case 3002
   session("erro") = "Ocorreu um erro durante a abertura do arquivo pedido"
  Case 3003
   session("erro") = "Erro na leitura do arquivo especificado"
  Case 3004
   session("erro") = "Erro ao escrever no arquivo"
  Case 3021
   session("erro") = "BOF ou EOF � True ou o registro atual foi deletado. A opera��o pedido pela aplica��o requer um registro atual"
  Case 3219
   session("erro") = "A opera��o pedida pela aplica��o n�o � permitida neste contexto"
  Case 3246
   session("erro") = "A aplica��o n�o pode fechar explicitamente um objeto connection no meio de uma transa��o"
  Case 3251
   session("erro") = "O provedor n�o oferece suporte a opera��o pedida pela aplica��o"
  Case 3265
   session("erro") = "ADO n�o pode achar o objeto na cole��o"
  Case 3367
   session("erro") = "N�o � anexar, objeto j� est� na cole��o"
  Case 3420
   session("erro") = "O objeto referenciado pela aplica��o n�o aponta mais para um objeto v�lido"
  Case 3421
   session("erro") = "A aplica��o est� usando um valor do tipo errado para a aplica��o atual"
  Case 3704
   session("erro") = "A opera��o pedida pela aplica��o n�o � permitida se o objeto estiver fechado"
  Case 3705
   session("erro") = "A opera��o pedida pela aplica��o n�o � permitida se o objeto estiver aberto"
  Case 3706
   session("erro") = "ADO n�o pode achar o provedor especificado"
  Case 3707
   session("erro") = "A aplica��o n�o pode alterar a propriedade ActiveConnect de um objeto Recordset com um objeto Command como fonte"
  Case 3708
   session("erro") = "A aplica��o definiu de modo impr�prio um objeto Parameter"
  Case 3709
   session("erro") = "A aplica��o pediu uma opera��o em um objeto com uma refer�ncia a um objeto Connection inv�lido ou fechado"
  Case 3710
   session("erro") = "A opera��o n�o � reentrante"
  Case 3711
   session("erro") = "A opera��o ainda est� executando"
  Case 3712
   session("erro") = "Opera��o cancelada"
  Case 3713
   session("erro") = "A opera��o ainda est� conectando"
  Case 3714
   session("erro") = "A transa��o � inv�lida"
  Case 3715
   session("erro") = "A opera��o n�o est� sendo executada"
  Case 3716
   session("erro") = "A opera��o n�o � segura sob estas circunst�ncias"
  Case 3717
   session("erro") = "A opera��o fez com que aparecesse uma caixa de di�logo"
  Case 3718
   session("erro") = "A opera��o fez com que aparecesse um cabe�alho de caixa de di�logo"
  Case 3719
   session("erro") = "A a��o falhou devido a uma viola��o na integridade dos dados"
  Case 3720
   session("erro") = "O provedor n�o pode ser modificado"
  Case 3721
   session("erro") = "Dados longos demais para o tipo de dados apresentados"
  Case 3722
   session("erro") = "A��o causou uma viola��o do esquema"
  Case 3723
   session("erro") = "A express�o continha sinais n�o coincidentes"
  Case 3724
   session("erro") = "O valor n�o pode ser convertido"
  Case 3725
   session("erro") = "O recurso n�o pode ser criado"
  Case 3726
   session("erro") = "A coluna especificada n�o existe nesta fileira"
  Case 3727
   session("erro") = "O URL n�o existe"
  Case 3728
   session("erro") = "Voc� n�o tem permiss�o para ver a �rvore do diret�rio"
  Case 3729
   session("erro") = "O URL apresentado � inv�lido"
  Case 3730
   session("erro") = "Recurso travado"
  Case 3731
   session("erro") = "Recurso j� existente"
  Case 3732
   session("erro") = "A a��o n�o pode ser conclu�da"
  Case 3733
   session("erro") = "O volume de arquivo n�o foi encontrado"
  Case 3734
   session("erro") = "Falha na opera��o porque o servidor n�o pode obter espa�o suficiente para completar a opera��o"
  Case 3735
   session("erro") = "Recurso fora de �mbito"
  Case 3736
   session("erro") = "Comando n�o est� dispon�vel"
  Case 3737
   session("erro") = "O URL na fileira identificada n�o existe"
  Case 3738
   session("erro") = "O recurso n�o pode ser deletado porque est� fora do escopo permitido"
  Case 3739
   session("erro") = "Esta propriedade � inv�lida para a coluna selecionada"
  Case 3740
   session("erro") = "Voc� apresentou uma op��o inv�lida para esta propriedade"
  Case 3741
   session("erro") = "Voc� apresentou um valor inv�lido para esta propriedade"
  Case 3742
   session("erro") = "A defini��o desta propriedade causou um conflito com outras propriedades"
  Case 3743
   session("erro") = "Nem todas as propriedades podem ser definidas"
  Case 3744
   session("erro") = "A propriedade n�o foi definida"
  Case 3745
   session("erro") = "A propriedade n�o pode ser definida"
  Case 3746
   session("erro") = "A propriedade n�o tem suporte"
  Case 3747
   session("erro") = "A a��o n�o pode ser conclu�da porque o cat�logo n�o est� definido"
  Case 3748
   session("erro") = "A conex�o n�o pode ser alterada"
  Case 3749
   session("erro") = "O m�todo Update da cole��o Fields falhou"
  Case 3750
   session("erro") = "N�o � poss�vel definir permiss�o Deny porque o provedor n�o oferece suporte para tanto"
  Case 3751
   session("erro") = "o provedor n�o oferece suporte ao tipo de pedido"
 End Select
End Sub

On Error Resume Next

IF Err > 0 Then Call CheckError
%>