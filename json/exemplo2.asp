<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Exemplo 2 - Json gerar Objeto ASP</title>
        <script language="javascript" runat="server" src="json2.asp"></script>
    </head>

    <body>		
		<h1>Exemplo 2 - Json gerar Objeto ASP</h1>
		<%
	
			'Declara variavel
			Dim notebook
			Dim variavel_json 
			
			'Variavel com o Json
			variavel_json = "{""marca"":""Acer"",""modelo"":""5130"",""monitor"":""15""}"			
		
			'Seta Objeto e executa o metodo que converte Json para Objeto ASP		
			Set notebook = JSON.parse(variavel_json)		
		
			response.Write "Marca: "&notebook.marca
			response.Write "<br/>Modelo: "&notebook.modelo
			response.Write "<br/>Monitor: "&notebook.monitor
			
			Set notebook = Nothing
		%>
    </body>
</html>	