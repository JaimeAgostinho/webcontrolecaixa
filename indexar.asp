<!--#include file="global.asp"-->
<!DOCTYPE html>
<html lang="pt-br">
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
	<title>Orbistec - Indexar tabelas</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- First include jquery js -->
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

	<!-- Then include bootstrap js -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

</head>
	<body>
 
	
<div class="row">
	<div class="col-xs-1"></div>
	<div class="col-xs-11">
		<%=CabecalhoUrl("Indexar Tabela CBF801 - Dia")%>
	</div>
</div>



<div class="container">
<div class="row">
 <div class="col-xs-8">
    <form id="modalExemplo" method="post" action=""> 
	<!--Panel-->
    <div class="panel panel-info">
      <div class="panel-heading" align="center">Index Tabela</div>
      <div class="panel-body form-group">

	<%
	if request("data") = "" then
		data = date()-5
		else
		data = request("data")		
	end if 


	idVez = 0
	Set objconnCobranca = Server.CreateObject("ADODB.Connection")
	objconnCobranca.open = StringConexaoBD()
	Sql = "SELECT cf.codigo, cf.mci, cf.dt_movim, cf.lojabb, cf.gestor, cf.statusbb, count(codigo) as quant, sum(valor) as valor, (select quant from cbf801_dia where gestor=cf.gestor and mci=cf.mci and lojabb=cf.lojabb and data=cf.dt_movim and statusbb=cf.statusbb) as conf FROM cbf801full cf where dt_movim = '"&ConvData(data)&"' group by mci, gestor, dt_movim, lojabb, statusbb having quant <> conf or conf is NULL"
	'response.write(Sql)
	set DadosCobranca = objconnCobranca.execute(Sql)
	if DadosCobranca.EOF then
	else
	DadosCobranca.MoveFirst
	While Not DadosCobranca.EOF

	var = "REPLACE INTO cbf801_dia (gestor, mci, lojabb, statusbb, data, valor, quant) VALUE ('"&DadosCobranca("gestor")&"', '"&DadosCobranca("mci")&"', '"&DadosCobranca("lojabb")&"', '"&DadosCobranca("statusbb")&"', '"&ConvData(DadosCobranca("dt_movim"))&"', '"&BdValor(DadosCobranca("valor"))&"', '"&DadosCobranca("quant")&"')"
	MysqlInsert(var)

	%>
			#
	<%
	idVez = idVez + 1	  
	DadosCobranca.MoveNext
	Wend
	end if
	if request("data") = date() then
		tipo = "parar"
	end if 

	dif = DateDiff("d",request("data"), date())
	%>

<% if dif > 0 then %>	
	<SCRIPT LANGUAGE="JavaScript">
	redirTime = "900";// 25000 equivale a 25 segundos
		redirURL = "indexar.asp?cont=<%=dif%>&data=<%=CStr(dateadd("d", 1, data))%>&tipo=<%=request("tipo")%>&vez=<%=vez%>";
	function redirTimer() { self.setTimeout("self.location.href = redirURL;",redirTime); }
	</script>

		<BODY onLoad="redirTimer()">
<%end if%>


      </div>
    </div>
	<!--/.Panel-->
    </form>
 </div>
</div>
</div>






</body>
</html>