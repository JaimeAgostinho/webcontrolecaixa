
<div class="row">
    <div class="col-sm-12">
		<%=CabecalhoUrl("Fechamento de Loja - Clean Up")%>
    </div>
</div>
<% if request("log") <> "a" then%>
<div class="row">
    <div class="col-sm-12">
        <form id="acesso" name="acesso" method="post" action="adm.asp?url=relatorio_fechamento&Orb=var&log=s">
        <div class="row">
        
            <div class="col-sm-2">
                <input class="form-control input-sm" type="text" name="data" maxlength="10" onKeyDown="MascaraForm(this,Data);" onKeyPress="MascaraForm(this,Data);" onKeyUp="MascaraForm(this,Data);" id="data" placeholder="Data:" value="<% if request("log") = "s" then%><%=request.form("data")%><%end if%>"/>
            </div>
            <div class="col-sm-2">
                <input class="form-control input-sm" type="text" name="loja"  id="loja" placeholder="Loja:" value="<% if request("log") = "s" then%><%=request.form("loja")%><%end if%>"/>
            </div>
            <div class="col-sm-2">
                <input class="form-control input-sm" type="text" name="chavej" id="chavej" placeholder="ChaveJ:" value="<% if request("log") = "s" then%><%=request.form("chavej")%><%end if%>"/>
            </div>
            <div class="col-sm-2">
                    <select class="form-control input-sm" id="status" name="status">
                          <option value="todos" <% if request("status") = "todos" then%>selected<%end if%>>Todos</option>
                          <option value="Lancado" <% if request("status") = "Lancado" then%>selected<%end if%>>Lançado Pelo PDV</option>
                          <option value="Valor a prejuízo" <% if request("status") = "Valor a prejuízo" then%>selected<%end if%>>Valor a prejuízo</option>
                          <option value="Valor em processo" <% if request("status") = "Valor em processo" then%>selected<%end if%>>Valor em processo</option>
                          <option value="Desconto em folha" <% if request("status") = "Desconto em folha" then%>selected<%end if%>>Desconto em folha</option>
                          <option value="Sinistro" <% if request("status") = "Sinistro" then%>selected<%end if%>>Sinistro</option>
                        </select>                      

            </div>
            <div class="col-sm-1 btn-group-sm">
                <input class="btn btn-primary btn-lg btn-block" type="submit" name="bt" id="bt" value="Pesquisar" />
            </div>
        
        </div>
        </form>
   </div>
</div>
<br>
<% if request("log") = "s" then%>

<%
where = ""
if request.form("data") <> "" then
	where = where&" and datacx = '"&ConvData(request.Form("data"))&"'"
end if 
if request.form("loja") <> "" then
	where = where&" and lojabb = '"&request.Form("loja")&"'"
end if 
if request.form("chavej") <> "" then
	where = where&" and local = '"&request.Form("chavej")&"'"
end if 
if request.form("status") <> "" then
	if request.form("status") <> "todos" then
		where = where&" and status = '"&request.Form("status")&"'"
	end if 
end if 

'Sql = "SELECT d.codigo, d.local, d.lojabb, d.data, d.datacx, d.tipo, replace(d.valor, ',','.') as valor, d.motivo, if(d.status = 'Lancado', 'Lançado', d.status ) as status, (select nome from usuario_operador where chavej=d.local limit 0,1) as nome FROM dif_caixa d where tipo='Devedor' "&where
Sql = "SELECT codigo, std_cx, lojabb, gerente, data, datacx, saldo_in, rec, pag, alivio, saldo_parcial, tot_gtv, obs_gtv, tot_difcxmenor, obs_difcxmenor, saldo_loja, saldo_pdv, saldo_comp, saldo_cofreadm, saldo_bocadelobo, obs_conf, tot_difcxmaior FROM financeiro_lj f where datacx = date(now()) "&where
%>
<script>
angular.module("Orbistec").controller("Relatorio", function ($scope)
{
	$scope.DifCaixa = <%=FuncJson(Sql)%>;
	
  $scope.Sum_saldo_in = 0;  
  $scope.Sum_rec = 0;  
  $scope.Sum_pag = 0;  
  $scope.Sum_alivio = 0;  
  $scope.Sum_SldAntCon = 0;  
  $scope.Sum_Gtv = 0;  
  $scope.Sum_SldCx = 0;  
  $scope.Sum_DifDev = 0;  
  $scope.Sum_DifCre = 0;  
  $scope.Sum_Retag = 0;  
	angular.forEach($scope.DifCaixa, function(value, key) {
	
	});
});
</script>
<div class="row">
<div ng-controller="Relatorio">
<div class="col-sm-11">
    <table class="table table-striped table-bordered ">
        <tr> 
            <th>loja</th>	
            <th>Status</th>	
            <th>Hr.Fechamento</th>	
            <th>Sld.Reman</th>	
            <th>Receb.</th>	
            <th>Pagam.</th>	
            <th>Status</th>	
            <th>Menu</th>	
        </tr>
        <tr ng-repeat="registro in DifCaixa | filter:criterioDeBusca">
            <td>{{registro.lojabb}}</td>	
            <td>{{registro.local}}</td>	
            <td>{{registro.nome}}</td>	
            <td>{{registro.datacx | date:'dd/MM/yyyy'}}</td>	
            <td>{{registro.motivo}}</td>	
            <td align="right">{{registro.valor | currency:'R$ '}}</td>	
            <td>{{registro.status}}</td>	
            <td> 
				AAA
            </td>	
        </tr>
        <tr>
            <td align="right" colspan="5">Total:</td>
            <td align="right">{{Sum | currency:'R$ '}}</td>
            <td align="right" colspan="2"></td>
        </tr>

    </table>
</div>
</div>
</div>
<% end if %>
<% end if %>
