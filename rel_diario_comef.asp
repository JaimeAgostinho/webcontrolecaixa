

<form id="acesso" name="acesso" method="post" action="adm.asp?tc=<%=response.Write(request("tc"))%>&url=rel_diario_comef&Orb=var&log=s">
<div class="row">
	<div class="col-sm-2">
		<input class="form-control input-sm" type="text" name="lojabb" id="lojabb" placeholder="Número da Loja:" value="<%=request("lojabb")%>"/>
	</div>
	<div class="col-sm-2">
		<input class="form-control input-sm" type="text" name="tipo_loja" id="tipo_loja" placeholder="Tipo de Loja:" value="<%=request("tipo_loja")%>"/>
	</div>
	<div class="col-sm-2">

<select class="form-control input-sm" id="regional" name="regional">
<%
Set objconnA = Server.CreateObject("ADODB.Connection")
objconnA.open = StringConexaoBD()
instrucao_sqlA = "SELECT regional FROM cad_lojabb group by regional order by regional"
set dadosA = objconnA.execute(instrucao_sqlA)
if dadosA.EOF then
else
dadosA.MoveFirst
While Not dadosA.EOF
%>
      <option value="<%=dadosA("regional")%>" <% if request("regional") = ""&dadosA("regional")&"" then%>selected<%end if%>><%=dadosA("regional")%></option>
<%
dadosA.MoveNext
Wend
end if
%>  
</select>                      


	</div>
    <div class="col-sm-2">
    	<label for="texto">Data de pesquisa:</label>
    </div>
    <div class="col-sm-2">
        <select class="form-control input-sm" id="mesbase" name="mesbase">


<%
Set objconnA = Server.CreateObject("ADODB.Connection")
objconnA.open = StringConexaoBD()
instrucao_sqlA = "SELECT mid(data,1,7) as dt FROM cbf801_dia where mci = '211217963' group by dt order by data desc"
set dadosA = objconnA.execute(instrucao_sqlA)
if dadosA.EOF then
else
dadosA.MoveFirst
While Not dadosA.EOF
%>
              <option value="<%=dadosA("dt")%>" <% if request("mesbase") = ""&dadosA("dt")&"" then%>selected<%end if%>><%=response.write(mid(dadosA("dt"),6,2))%>/<%=mid(dadosA("dt"),1,4)%></option>
<%
dadosA.MoveNext
Wend
end if
%>  
</select>                      

    </div>
    <div class="col-sm-1 btn-group-sm">
		<input class="btn btn-primary btn-lg btn-block" type="submit" name="bt" id="bt" value="Pesquisar" />
    </div>

</div>
</form>
<br>
<% if request("log") = "s" then%>

<table class="table table-striped table-bordered ">
  <thead>
    <tr bgcolor="#CCCCCC">
      <th align="left">LojaBB</th>
      <th class="align-text-bottom" NOWRAP>Nome:</th>
      <th class="align-text-bottom" NOWRAP>Tipo de Loja:</th>
      <th class="align-text-bottom" NOWRAP>Regional:</th>
<%
dia = 0
sql = ""
Set objconnA = Server.CreateObject("ADODB.Connection")
objconnA.open = StringConexaoBD()
instrucao_sqlA = "SELECT data FROM cbf801_dia where mci = '211217963' and data >= '"&request("mesbase")&"-01' and data <= '"&request("mesbase")&"-31'  group by data order by data"
set dadosPesqData = objconnA.execute(instrucao_sqlA)
if dadosPesqData.EOF then
else
dadosPesqData.MoveFirst
While Not dadosPesqData.EOF
sql = sql &""
dia = dia + 1
sql = sql &", (select quant from cbf801_dia where mci='211217963' and lojabb=cl.lojabb and data='"&ConvData(dadosPesqData("data"))&"' and statusbb='1') as D"&dia
%>
      <th><div align="center"> <%=dia%>º Dia<br>(<%=mid(dadosPesqData("data"),1,5)%>)<br><%=response.write(DiaDaSemanaSimples(dadosPesqData("data")))%></div></th>
<%
if request.form("lojabb") <> "" then
	lojabbFilter = "and cl.lojabb = '"&request("lojabb")&"'"
	regional = ", cl.regional "
else
	lojabbFilter = ""
	regional = ""
end if
if request.form("tipo_loja") <> "" then
	tipoLojaFilter = " and cl.tipo_loja like '%"&request("tipo_loja")&"%'"
	regional = ", cl.regional "
else
	tipoLojaFilter = ""
	regional = ""
end if
if request.form("regional") <> "" then
	regionalFilter = " and cl.regional like '%"&request("regional")&"%'"
end if
sql = sql &", (select sum(quant) from cbf801_dia where mci='211217963' and lojabb=cl.lojabb and data >= '"&request("mesbase")&"-01' and data <= '"&request("mesbase")&"-31' and statusbb='1') as Acumulado"
dadosPesqData.MoveNext
Wend
end if
%>  
      <th align="left">Acumulado:</th>
    </tr>
  </thead>
  <tbody>

<%
For i = 1 To dia
	session("acum_D_"&i&"") = 0
Next
	session("Acumulado") = 0
%>

<%
Set objconnRel = Server.CreateObject("ADODB.Connection")
objconnRel.open = StringConexaoBD()
instrucao_sqlRel = "SELECT cl.lojabb, cl.nome, replace(cl.tipo_loja,' ','_') as tipo_loja, cl.regional"&sql&" FROM cad_lojabb cl where cl.mci='211217963' "&lojabbFilter&tipoLojaFilter&regionalFilter&" order by lojabb"
set dadosRel = objconnRel.execute(instrucao_sqlRel)
if dadosRel.EOF then
else
dadosRel.MoveFirst
While Not dadosRel.EOF
%>
    <tr>
    <td NOWRAP><%=dadosRel("lojabb")%></td>
    <td NOWRAP><%=dadosRel("nome")%></td>
    <td NOWRAP><%=dadosRel("tipo_loja")%></td>
    <td NOWRAP><%=dadosRel("regional")%></td>
    	<%
    	For i = 1 To dia
	    	if session(""&dadosRel("tipo_loja")&"") = "" then
				session(""&dadosRel("tipo_loja")&"") = 0	    		
	    	end if 
	    session(""&dadosRel("tipo_loja")&"_D_"&i&"") = 	((session(""&dadosRel("tipo_loja")&"_D_"&i&""))+(ccur(dadosRel("D"&i&""))))
    	session("acum_D_"&i&"") = ((ccur(dadosRel("D"&i&"")))+(ccur(session("acum_D_"&i&""))))
    	%>
		    <td NOWRAP><div align="center"><%=SumFormCnubDg(dadosRel("D"&i&""), 0)%></div></td>
    	<%
    	Next
    	%>
		    <td NOWRAP><div align="center"><%=SumFormCnubDg(dadosRel("Acumulado"),0)%></div></td>

    </tr>
<%
session("Acumulado") = ((ccur(dadosRel("Acumulado")))+(ccur(session("Acumulado"))))
session("Acm"&dadosRel("tipo_loja")&"") = ((ccur(dadosRel("Acumulado")))+(ccur(session("Acm"&dadosRel("tipo_loja")&""))))

dadosRel.MoveNext
Wend
end if
%>  
    <tr>
      <td NOWRAP colspan="3" align="right">Acumulado:</td>
	<%
	For x = 1 To dia

	%>
      <td NOWRAP><div align="center"><%=SumFormCnubDg(session("acum_D_"&x&""),0)%></div></td>
	<%
	Next
	%>
      <td NOWRAP><div align="center"><%=SumFormCnubDg(session("Acumulado"),0)%></div></td>

    </tr>

    <tr>
      <td NOWRAP></td>
	</tr>

    <tr>
      <td NOWRAP colspan="3" align="right">Total Acumulativo Governo</td>
		<%
		For A = 1 To dia
		%>
	      <td NOWRAP><div align="center"><%=SumFormCnubDg(session("Governo_D_"&A&""),0)%></div></td>
		<%
		session("Governo_D_"&A&"") = 0
		Next
		%>
	      <td NOWRAP><div align="center"><%=SumFormCnubDg(session("AcmGoverno"),0)%></div></td>
		<%
		session("AcmGoverno") = 0
		%>
    </tr>

    <tr>
      <td NOWRAP colspan="3" align="right">Total Acumulativo Ponto Estratégico</td>
	<%
	For B = 1 To dia
	%>
	      <td NOWRAP><div align="center"><%=SumFormCnubDg(session("Ponto_Estratégico_D_"&B&""),0)%></div></td>
	<%
	session("Ponto_Estratégico_D_"&B&"") = 0
	Next
	%>
	      <td NOWRAP><div align="center"><%=SumFormCnubDg(session("AcmPonto_Estratégico"),0)%></div></td>
    </tr>

		<%
		session("AcmPonto_Estratégico") = 0
		%>
    <tr>
      <td NOWRAP colspan="3" align="right">Total Acumulativo Poupatempos</td>
	<%
	For C = 1 To dia
	%>
	      <td NOWRAP><div align="center"><%=SumFormCnubDg(session("Poupatempo_D_"&C&""),0)%></div></td>
	<%
	session("Poupatempo_D_"&C&"") = 0
	Next
	%>
	      <td NOWRAP><div align="center"><%=SumFormCnubDg(session("AcmPoupatempo"),0)%></div></td>
		<%
		session("AcmPoupatempo") = 0
		%>

    </tr>


  </tbody>
</TABLE>


<%end if%>