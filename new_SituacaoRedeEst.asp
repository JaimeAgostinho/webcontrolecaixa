﻿<!--#include file="global.asp"-->
<%
function PesqValorRel(sql)
vlr = PesqResp(sql)
if vlr <> "" then
	PesqValorRel = vlr
else
	PesqValorRel = 0
end if
end function

function Currenc(valor)
if valor <> "" then
	Currenc = formatcurrency(valor,2)
else
	Currenc = formatcurrency("0",2)
end if
end function


%>
<% if request("gr") = "s" then%>

<%

	if request("valor") <> "" then
		sqlLoja = "REPLACE INTO rel_gerencial (nome, data, valor)VALUES ('"&request("nome")&"', date(now()), '"&BdValor(request("valor"))&"')"
		MysqlInsert(sqlLoja)
	end if

response.Write("ok")	
%>
<%else%>
<html lang="pt-br">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Sys PDV">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Jaime Agostinho">
    <title>RELATORIO DIARIO - PONTOS PROPRIOS E SUBSTABELECIDOS</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <style type="text/css">
   body, table{
	margin-top: 10px;
	margin-left: 10px;
	margin-right: 10px;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 10px;
	text-align: right;
	}
   .glyphicon { margin-right:6px; }
   .card-block a{text-decoration: none;}
   .collapse .card-block.disabled a{color:#c5c5c5;}
   .collapse .card-block.disabled, .collapse .card-block.disabled span{color:#c5c5c5;}
   
div.vertical
{
 writing-mode: tb-rl;
 filter: flipv fliph;
 text-align:center;
}

	.verticalText
	{
		text-align: center;
		vertical-align: middle;
		width: 10px;
		margin: 0px;
		padding: 0px;
		padding-left: 1px;
		padding-right: 1px;
		padding-top: 0px;
		white-space: nowrap;
		-webkit-transform: rotate(-90deg); 
		-moz-transform: rotate(-90deg);                 
	};
    </style>
</head>
<body>
<% 
if request("log") = "s" then
	if request("dt") <> "" then
		dtPesq = ConvData(request("dt"))
		dtPesqSemForm = request("dt")
	else
		dtPesq = ConvData(date())
		dtPesqSemForm = date()
	end if
%>
<div class="row">
  <div class="container-fluid">
      <div class="row">
	      <div class="col-lg-9" align="left">

	      <img src="imagem/comef.jpg" width="131" height="27"> 


	      </div>
          <div class="col-lg-3" align="right"></br>DATA DO MOVIMENTO: <%=response.Write(dtPesqSemForm)%></div>
      </div>
    
      <div class="row">
        <div class="col-lg-12 table-bordered">
    	    <div class="row text-xs-center"><b>RELATORIO DIARIO - PONTOS PROPRIOS E SUBSTABELECIDOS</b></div>
		</div>
    </div>

      <div class="row">
        <div class="col-lg-12 table-bordered">
    	    <div class="row text-xs-center"><b>LIMITE OPERACIONAL DIÁRIO(LOD) - PRÓPRIOS</b></div>
		</div>
    </div>

      <div class="row">
          <div class="col-lg-6 text-nowrap" align="left">LIMITE AUTORIZADO: <%=formatcurrency(PesqValorRel("SELECT if(valor > 0, valor, 0) as Resp FROM rel_gerencial where data = '"&dtPesq&"' and nome='LimitAutoProp'"))%></div>
      </div>
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left">DATA:</td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 9,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 8,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 7,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 6,1" )
			PernPriB = Dia
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 5,1" )
			PernPriA = Dia
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 4,1" )
			PernPri = Dia
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 3,1" )
			PernSeg = Dia
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 2,1" )
			PernTer = Dia
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 1,1" )
			PernQua = Dia
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 0,1" )
			PernPrinc = Dia
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td>MÉDIA</td>
          </tr>
          <tr>
            <td align="left" nowrap>LOD DISTRIBUIDO:</td>
            <td nowrap>
				<%
				totLodDistProp = 0
					LodDistProp9 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 9,1" )
					LimitAutoProp9 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoProp' order by data desc limit 9,1" )
					response.Write(Formatcurrency(LodDistProp9, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp9)
  			     %>
            </td nowrap>
            <td><%
					LodDistProp8 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 8,1" )
					LimitAutoProp8 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoProp' order by data desc limit 8,1" )
					response.Write(Formatcurrency(LodDistProp8, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp8)
  			     %></td>
            <td nowrap><%
					LodDistProp7 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 7,1" )
					LimitAutoProp7 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoProp' order by data desc limit 7,1" )
					response.Write(Formatcurrency(LodDistProp7, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp7)
  			     %></td>
            <td nowrap><%
					LodDistProp6 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 6,1" )
					LimitAutoProp6 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoProp' order by data desc limit 6,1" )
					response.Write(Formatcurrency(LodDistProp6, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp6)
  			     %></td>
            <td nowrap><%
					LodDistProp5 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 5,1" )
					LimitAutoProp5 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoProp' order by data desc limit 5,1" )
					response.Write(Formatcurrency(LodDistProp5, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp5)
  			     %></td>
            <td nowrap><%
					LodDistProp4 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 4,1" )
					LimitAutoProp4 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoProp' order by data desc limit 4,1" )
					response.Write(Formatcurrency(LodDistProp4, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp4)
  			     %></td>
            <td nowrap><%
					LodDistProp3 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 3,1" )
					LimitAutoProp3 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoProp' order by data desc limit 3,1" )
					response.Write(Formatcurrency(LodDistProp3, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp3)
  			     %></td>
            <td nowrap><%
					LodDistProp2 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 2,1" )
					LimitAutoProp2 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoProp' order by data desc limit 2,1" )
					response.Write(Formatcurrency(LodDistProp2, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp2)
  			     %></td>
            <td nowrap><%
					LodDistProp1 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 1,1" )
					LimitAutoProp1 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoProp' order by data desc limit 1,1" )
					response.Write(Formatcurrency(LodDistProp1, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp1)
  			     %></td>
            <td nowrap><%
					LodDistProp0 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistProp' order by data desc limit 0,1" )
					LimitAutoProp0 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoProp' order by data desc limit 0,1" )
					response.Write(Formatcurrency(LodDistProp0, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp0)
  			     %></td>
            <td nowrap>
			<%
			totLodDistProp = ((totLodDistProp)/10)
			response.Write(formatcurrency(totLodDistProp,2))
			%>
            </td>
          </tr>
          <tr>
            <td align="left" nowrap>SALDO ATUAL:</td>
            <td nowrap><%
					totLodAtuaProp = 0
					LodAtuaProp9 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaProp' order by data desc limit 9,1" )
					response.Write(Formatcurrency(LodAtuaProp9, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp9)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp8 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaProp' order by data desc limit 8,1" )
					response.Write(Formatcurrency(LodAtuaProp8, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp8)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp7 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaProp' order by data desc limit 7,1" )
					response.Write(Formatcurrency(LodAtuaProp7, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp7)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp6 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaProp' order by data desc limit 6,1" )
					response.Write(Formatcurrency(LodAtuaProp6, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp6)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp5 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaProp' order by data desc limit 5,1" )
					response.Write(Formatcurrency(LodAtuaProp5, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp5)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp4 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaProp' order by data desc limit 4,1" )
					response.Write(Formatcurrency(LodAtuaProp4, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp4)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp3 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaProp' order by data desc limit 3,1" )
					response.Write(Formatcurrency(LodAtuaProp3, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp3)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp2 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaProp' order by data desc limit 2,1" )
					response.Write(Formatcurrency(LodAtuaProp2, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp2)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp1 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaProp' order by data desc limit 1,1" )
					response.Write(Formatcurrency(LodAtuaProp1, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp1)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp0 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaProp' order by data desc limit 0,1" )
					response.Write(Formatcurrency(LodAtuaProp0, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp0)					
  			     %></td>
            <td nowrap>			
			<%
			totLodAtuaProp = ((totLodAtuaProp)/10)
			response.Write(formatcurrency(totLodAtuaProp,2))
			%></td>
          </tr>
          <tr>
            <td align="left" nowrap>% DISTRIBUIDO:</td>
            <td nowrap>
			<%
			totPercDist = 0
			if LodDistProp9 <> "0" and LimitAutoProp9 <> "0" then
			PercDist = (((formatnumber(LodDistProp9))/(formatnumber(LimitAutoProp9)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%>
            </td>
            <td nowrap><%
			if LodDistProp8 <> "0" and LimitAutoProp8 <> "0" then
			PercDist = (((formatnumber(LodDistProp8))/(formatnumber(LimitAutoProp8)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp7 <> "0" and LimitAutoProp7 <> "0" then
			PercDist = (((formatnumber(LodDistProp7))/(formatnumber(LimitAutoProp7)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp6 <> "0" and LimitAutoProp6 <> "0" then
			PercDist = (((formatnumber(LodDistProp6))/(formatnumber(LimitAutoProp6)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp5 <> "0" and LimitAutoProp5 <> "0" then
			PercDist = (((formatnumber(LodDistProp5))/(formatnumber(LimitAutoProp5)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp4 <> "0" and LimitAutoProp4 <> "0" then
			PercDist = (((formatnumber(LodDistProp4))/(formatnumber(LimitAutoProp4)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp3 <> "0" and LimitAutoProp3 <> "0" then
			PercDist = (((formatnumber(LodDistProp3))/(formatnumber(LimitAutoProp3)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp2 <> "0" and LimitAutoProp2 <> "0" then
			PercDist = (((formatnumber(LodDistProp2))/(formatnumber(LimitAutoProp2)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp1 <> "0" and LimitAutoProp1 <> "0" then
			PercDist = (((formatnumber(LodDistProp1))/(formatnumber(LimitAutoProp1)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp0 <> "0" and LimitAutoProp0 <> "0" then
			PercDist = (((formatnumber(LodDistProp0))/(formatnumber(LimitAutoProp0)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td nowrap>
            <%
			totPercDist = ((totPercDist)/10)
			response.Write(formatnumber(totPercDist,2)&"%")
			%></td>
          </tr>
          <tr>
            <td align="left" nowrap>% CONSUMO LOD:</td>
            <td nowrap><%
			totPercDistB = 0
			if LodDistProp9 <> "0" and LodAtuaProp9 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp9))/(formatnumber(LodDistProp9)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp8 <> "0" and LodAtuaProp8 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp8))/(formatnumber(LodDistProp8)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp7 <> "0" and LodAtuaProp7 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp7))/(formatnumber(LodDistProp7)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp6 <> "0" and LodAtuaProp6 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp6))/(formatnumber(LodDistProp6)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp5 <> "0" and LodAtuaProp5 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp5))/(formatnumber(LodDistProp5)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp4 <> "0" and LodAtuaProp4 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp4))/(formatnumber(LodDistProp4)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp3 <> "0" and LodAtuaProp3 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp3))/(formatnumber(LodDistProp3)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp2 <> "0" and LodAtuaProp2 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp2))/(formatnumber(LodDistProp2)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp1 <> "0" and LodAtuaProp1 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp1))/(formatnumber(LodDistProp1)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td nowrap><%
			if LodDistProp0 <> "0" and LodAtuaProp0 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp0))/(formatnumber(LodDistProp0)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td nowrap><%
			totPercDistB = ((totPercDistB)/10)
			response.Write(formatnumber(totPercDistB,2)&"%")
			%></td>
          </tr>
        </table>
      <div class="row"><br></div>
      <div class="row">
        <div class="col-lg-12 table-bordered">
    	    <div class="row text-xs-center"><b>LIMITE OPERACIONAL DIÁRIO(LOD) - SUBSTABELECIDOS</b></div>
		</div>
    </div>
      <div class="row">
          <div class="col-lg-6 text-nowrap" align="left">LIMITE AUTORIZADO: <%=formatcurrency(PesqValorRel("SELECT if(valor > 0, valor, 0) as Resp FROM rel_gerencial where data = '"&dtPesq&"' and nome='LimitAutoSub'"))%></div>
      </div>
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left">DATA:</td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 9,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 8,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 7,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 6,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 5,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 4,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 3,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 2,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 1,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 0,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td>MÉDIA</td>
          </tr>
          <tr>
            <td align="left"  nowrap>LOD DISTRIBUIDO:</td>
            <td>
				<%
				totLodDistProp = 0
					LodDistProp9 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistSub' order by data desc limit 9,1" )
					LimitAutoProp9 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 9,1" )
					response.Write(Formatcurrency(LodDistProp9, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp9)
  			     %>
            </td>
            <td><%
					LodDistProp8 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistSub' order by data desc limit 8,1" )
					LimitAutoProp8 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 8,1" )
					response.Write(Formatcurrency(LodDistProp8, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp8)
  			     %></td>
            <td><%
					LodDistProp7 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistSub' order by data desc limit 7,1" )
					LimitAutoProp7 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 7,1" )
					response.Write(Formatcurrency(LodDistProp7, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp7)
  			     %></td>
            <td><%
					LodDistProp6 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistSub' order by data desc limit 6,1" )
					LimitAutoProp6 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 6,1" )
					response.Write(Formatcurrency(LodDistProp6, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp6)
  			     %></td>
            <td><%
					LodDistProp5 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistSub' order by data desc limit 5,1" )
					LimitAutoProp5 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 5,1" )
					response.Write(Formatcurrency(LodDistProp5, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp5)
  			     %></td>
            <td><%
					LodDistProp4 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistSub' order by data desc limit 4,1" )
					LimitAutoProp4 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 4,1" )
					response.Write(Formatcurrency(LodDistProp4, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp4)
  			     %></td>
            <td><%
					LodDistProp3 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistSub' order by data desc limit 3,1" )
					LimitAutoProp3 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 3,1" )
					response.Write(Formatcurrency(LodDistProp3, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp3)
  			     %></td>
            <td><%
					LodDistProp2 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistSub' order by data desc limit 2,1" )
					LimitAutoProp2 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 2,1" )
					response.Write(Formatcurrency(LodDistProp2, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp2)
  			     %></td>
            <td><%
					LodDistProp1 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistSub' order by data desc limit 1,1" )
					LimitAutoProp1 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 1,1" )
					response.Write(Formatcurrency(LodDistProp1, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp1)
  			     %></td>
            <td><%
					LodDistProp0 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistSub' order by data desc limit 0,1" )
					LimitAutoProp0 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoSub' order by data desc limit 0,1" )
					response.Write(Formatcurrency(LodDistProp0, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp0)
  			     %></td>
            <td>
			<%
			totLodDistProp = ((totLodDistProp)/10)
			response.Write(formatcurrency(totLodDistProp,2))
			%>
            </td>
          </tr>
          <tr>
            <td align="left" nowrap>SALDO ATUAL:</td>
            <td><%
					totLodAtuaProp = 0
					LodAtuaProp9 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaSub' order by data desc limit 9,1" )
					response.Write(Formatcurrency(LodAtuaProp9, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp9)					
  			     %></td>
            <td><%
					LodAtuaProp8 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaSub' order by data desc limit 8,1" )
					response.Write(Formatcurrency(LodAtuaProp8, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp8)					
  			     %></td>
            <td><%
					LodAtuaProp7 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaSub' order by data desc limit 7,1" )
					response.Write(Formatcurrency(LodAtuaProp7, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp7)					
  			     %></td>
            <td><%
					LodAtuaProp6 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaSub' order by data desc limit 6,1" )
					response.Write(Formatcurrency(LodAtuaProp6, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp6)					
  			     %></td>
            <td><%
					LodAtuaProp5 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaSub' order by data desc limit 5,1" )
					response.Write(Formatcurrency(LodAtuaProp5, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp5)					
  			     %></td>
            <td><%
					LodAtuaProp4 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaSub' order by data desc limit 4,1" )
					response.Write(Formatcurrency(LodAtuaProp4, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp4)					
  			     %></td>
            <td><%
					LodAtuaProp3 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaSub' order by data desc limit 3,1" )
					response.Write(Formatcurrency(LodAtuaProp3, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp3)					
  			     %></td>
            <td><%
					LodAtuaProp2 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaSub' order by data desc limit 2,1" )
					response.Write(Formatcurrency(LodAtuaProp2, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp2)					
  			     %></td>
            <td><%
					LodAtuaProp1 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaSub' order by data desc limit 1,1" )
					response.Write(Formatcurrency(LodAtuaProp1, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp1)					
  			     %></td>
            <td><%
					LodAtuaProp0 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaSub' order by data desc limit 0,1" )
					response.Write(Formatcurrency(LodAtuaProp0, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp0)					
  			     %></td>
            <td>			
			<%
			totLodAtuaProp = ((totLodAtuaProp)/10)
			response.Write(formatcurrency(totLodAtuaProp,2))
			%></td>
          </tr>
          <tr>
            <td align="left" nowrap>% DISTRIBUIDO:</td>
            <td>
			<%
			totPercDist = 0
			if LodDistProp9 <> "0" and LimitAutoProp9 <> "0" then
			PercDist = (((formatnumber(LodDistProp9))/(formatnumber(LimitAutoProp9)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%>
            </td>
            <td><%
			if LodDistProp8 <> "0" and LimitAutoProp8 <> "0" then
			PercDist = (((formatnumber(LodDistProp8))/(formatnumber(LimitAutoProp8)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp7 <> "0" and LimitAutoProp7 <> "0" then
			PercDist = (((formatnumber(LodDistProp7))/(formatnumber(LimitAutoProp7)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp6 <> "0" and LimitAutoProp6 <> "0" then
			PercDist = (((formatnumber(LodDistProp6))/(formatnumber(LimitAutoProp6)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp5 <> "0" and LimitAutoProp5 <> "0" then
			PercDist = (((formatnumber(LodDistProp5))/(formatnumber(LimitAutoProp5)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp4 <> "0" and LimitAutoProp4 <> "0" then
			PercDist = (((formatnumber(LodDistProp4))/(formatnumber(LimitAutoProp4)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp3 <> "0" and LimitAutoProp3 <> "0" then
			PercDist = (((formatnumber(LodDistProp3))/(formatnumber(LimitAutoProp3)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp2 <> "0" and LimitAutoProp2 <> "0" then
			PercDist = (((formatnumber(LodDistProp2))/(formatnumber(LimitAutoProp2)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp1 <> "0" and LimitAutoProp1 <> "0" then
			PercDist = (((formatnumber(LodDistProp1))/(formatnumber(LimitAutoProp1)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp0 <> "0" and LimitAutoProp0 <> "0" then
			PercDist = (((formatnumber(LodDistProp0))/(formatnumber(LimitAutoProp0)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td>
            <%
			totPercDist = ((totPercDist)/10)
			response.Write(formatnumber(totPercDist,2)&"%")
			%></td>
          </tr>
          <tr>
            <td align="left" nowrap>% CONSUMO LOD:</td>
            <td><%
			totPercDistB = 0
			if LodDistProp9 <> "0" and LodAtuaProp9 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp9))/(formatnumber(LodDistProp9)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp8 <> "0" and LodAtuaProp8 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp8))/(formatnumber(LodDistProp8)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp7 <> "0" and LodAtuaProp7 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp7))/(formatnumber(LodDistProp7)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp6 <> "0" and LodAtuaProp6 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp6))/(formatnumber(LodDistProp6)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp5 <> "0" and LodAtuaProp5 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp5))/(formatnumber(LodDistProp5)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp4 <> "0" and LodAtuaProp4 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp4))/(formatnumber(LodDistProp4)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp3 <> "0" and LodAtuaProp3 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp3))/(formatnumber(LodDistProp3)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp2 <> "0" and LodAtuaProp2 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp2))/(formatnumber(LodDistProp2)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp1 <> "0" and LodAtuaProp1 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp1))/(formatnumber(LodDistProp1)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp0 <> "0" and LodAtuaProp0 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp0))/(formatnumber(LodDistProp0)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			totPercDistB = ((totPercDistB)/10)
			response.Write(formatnumber(totPercDistB,2)&"%")
			%></td>
          </tr>
        </table>
      <div class="row"><br></div>
      <div class="row">
        <div class="col-lg-12 table-bordered">
    	    <div class="row text-xs-center"><b>LIMITE OPERACIONAL DIÁRIO(LOD) - CIELO</b></div>
		</div>
    </div>
      <div class="row">
          <div class="col-lg-6 text-nowrap" align="left">LIMITE AUTORIZADO: <%=formatcurrency(PesqValorRel("SELECT if(valor > 0, valor, 0) as Resp FROM rel_gerencial where data = '"&dtPesq&"' and nome='LimitAutoCielo'"))%></div>
      </div>
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left">DATA:</td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 9,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 8,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 7,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 6,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 5,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 4,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 3,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 2,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 1,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 0,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td>MÉDIA</td>
          </tr>
          <tr>
            <td align="left" nowrap>LOD DISTRIBUIDO:</td>
            <td>
				<%
				totLodDistProp = 0
					LodDistProp9 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistCielo' order by data desc limit 9,1" )
					LimitAutoProp9 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 9,1" )
					response.Write(Formatcurrency(LodDistProp9, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp9)
  			     %>
            </td>
            <td><%
					LodDistProp8 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistCielo' order by data desc limit 8,1" )
					LimitAutoProp8 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 8,1" )
					response.Write(Formatcurrency(LodDistProp8, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp8)
  			     %></td>
            <td><%
					LodDistProp7 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistCielo' order by data desc limit 7,1" )
					LimitAutoProp7 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 7,1" )
					response.Write(Formatcurrency(LodDistProp7, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp7)
  			     %></td>
            <td><%
					LodDistProp6 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistCielo' order by data desc limit 6,1" )
					LimitAutoProp6 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 6,1" )
					response.Write(Formatcurrency(LodDistProp6, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp6)
  			     %></td>
            <td><%
					LodDistProp5 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistCielo' order by data desc limit 5,1" )
					LimitAutoProp5 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 5,1" )
					response.Write(Formatcurrency(LodDistProp5, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp5)
  			     %></td>
            <td><%
					LodDistProp4 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistCielo' order by data desc limit 4,1" )
					LimitAutoProp4 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 4,1" )
					response.Write(Formatcurrency(LodDistProp4, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp4)
  			     %></td>
            <td><%
					LodDistProp3 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistCielo' order by data desc limit 3,1" )
					LimitAutoProp3 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 3,1" )
					response.Write(Formatcurrency(LodDistProp3, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp3)
  			     %></td>
            <td><%
					LodDistProp2 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistCielo' order by data desc limit 2,1" )
					LimitAutoProp2 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 2,1" )
					response.Write(Formatcurrency(LodDistProp2, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp2)
  			     %></td>
            <td><%
					LodDistProp1 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistCielo' order by data desc limit 1,1" )
					LimitAutoProp1 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 1,1" )
					response.Write(Formatcurrency(LodDistProp1, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp1)
  			     %></td>
            <td><%
					LodDistProp0 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodDistCielo' order by data desc limit 0,1" )
					LimitAutoProp0 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LimitAutoCielo' order by data desc limit 0,1" )
					response.Write(Formatcurrency(LodDistProp0, 2))
					totLodDistProp = totLodDistProp + formatnumber(LodDistProp0)
  			     %></td>
            <td>
			<%
			totLodDistProp = ((totLodDistProp)/10)
			response.Write(formatcurrency(totLodDistProp,2))
			%>
            </td>
          </tr>
          <tr>
            <td align="left" nowrap>SALDO ATUAL:</td>
            <td nowrap><%
					totLodAtuaProp = 0
					LodAtuaProp9 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaCielo' order by data desc limit 9,1" )
					response.Write(Formatcurrency(LodAtuaProp9, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp9)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp8 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaCielo' order by data desc limit 8,1" )
					response.Write(Formatcurrency(LodAtuaProp8, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp8)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp7 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaCielo' order by data desc limit 7,1" )
					response.Write(Formatcurrency(LodAtuaProp7, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp7)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp6 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaCielo' order by data desc limit 6,1" )
					response.Write(Formatcurrency(LodAtuaProp6, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp6)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp5 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaCielo' order by data desc limit 5,1" )
					response.Write(Formatcurrency(LodAtuaProp5, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp5)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp4 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaCielo' order by data desc limit 4,1" )
					response.Write(Formatcurrency(LodAtuaProp4, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp4)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp3 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaCielo' order by data desc limit 3,1" )
					response.Write(Formatcurrency(LodAtuaProp3, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp3)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp2 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaCielo' order by data desc limit 2,1" )
					response.Write(Formatcurrency(LodAtuaProp2, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp2)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp1 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaCielo' order by data desc limit 1,1" )
					response.Write(Formatcurrency(LodAtuaProp1, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp1)					
  			     %></td>
            <td nowrap><%
					LodAtuaProp0 = PesqValorRel("SELECT valor as Resp FROM rel_gerencial where data <= '"&dtPesq&"' and nome='LodAtuaCielo' order by data desc limit 0,1" )
					response.Write(Formatcurrency(LodAtuaProp0, 2))
					totLodAtuaProp = totLodAtuaProp + formatnumber(LodAtuaProp0)					
  			     %></td>
            <td nowrap>			
			<%
			totLodAtuaProp = ((totLodAtuaProp)/10)
			response.Write(formatcurrency(totLodAtuaProp,2))
			%></td>
          </tr>
          <tr>
            <td align="left" nowrap>% DISTRIBUIDO:</td>
            <td>
			<%
			totPercDist = 0
			if LodDistProp9 <> "0" and LimitAutoProp9 <> "0" then
			PercDist = (((formatnumber(LodDistProp9))/(formatnumber(LimitAutoProp9)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%>
            </td>
            <td><%
			if LodDistProp8 <> "0" and LimitAutoProp8 <> "0" then
			PercDist = (((formatnumber(LodDistProp8))/(formatnumber(LimitAutoProp8)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp7 <> "0" and LimitAutoProp7 <> "0" then
			PercDist = (((formatnumber(LodDistProp7))/(formatnumber(LimitAutoProp7)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp6 <> "0" and LimitAutoProp6 <> "0" then
			PercDist = (((formatnumber(LodDistProp6))/(formatnumber(LimitAutoProp6)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp5 <> "0" and LimitAutoProp5 <> "0" then
			PercDist = (((formatnumber(LodDistProp5))/(formatnumber(LimitAutoProp5)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp4 <> "0" and LimitAutoProp4 <> "0" then
			PercDist = (((formatnumber(LodDistProp4))/(formatnumber(LimitAutoProp4)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp3 <> "0" and LimitAutoProp3 <> "0" then
			PercDist = (((formatnumber(LodDistProp3))/(formatnumber(LimitAutoProp3)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp2 <> "0" and LimitAutoProp2 <> "0" then
			PercDist = (((formatnumber(LodDistProp2))/(formatnumber(LimitAutoProp2)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp1 <> "0" and LimitAutoProp1 <> "0" then
			PercDist = (((formatnumber(LodDistProp1))/(formatnumber(LimitAutoProp1)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td><%
			if LodDistProp0 <> "0" and LimitAutoProp0 <> "0" then
			PercDist = (((formatnumber(LodDistProp0))/(formatnumber(LimitAutoProp0)))*100)
				response.Write(formatnumber(PercDist,2)&"%")
				totPercDist = totPercDist + formatnumber(PercDist)
			end if
			%></td>
            <td>
            <%
			totPercDist = ((totPercDist)/10)
			response.Write(formatnumber(totPercDist,2)&"%")
			%></td>
          </tr>
          <tr>
            <td align="left" nowrap>% CONSUMO LOD:</td>
            <td><%
			totPercDistB = 0
			if LodDistProp9 <> "0" and LodAtuaProp9 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp9))/(formatnumber(LodDistProp9)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp8 <> "0" and LodAtuaProp8 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp8))/(formatnumber(LodDistProp8)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp7 <> "0" and LodAtuaProp7 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp7))/(formatnumber(LodDistProp7)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp6 <> "0" and LodAtuaProp6 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp6))/(formatnumber(LodDistProp6)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp5 <> "0" and LodAtuaProp5 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp5))/(formatnumber(LodDistProp5)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp4 <> "0" and LodAtuaProp4 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp4))/(formatnumber(LodDistProp4)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp3 <> "0" and LodAtuaProp3 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp3))/(formatnumber(LodDistProp3)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp2 <> "0" and LodAtuaProp2 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp2))/(formatnumber(LodDistProp2)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp1 <> "0" and LodAtuaProp1 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp1))/(formatnumber(LodDistProp1)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if LodDistProp0 <> "0" and LodAtuaProp0 <> "0" then
			PercDistB = (((formatnumber(LodAtuaProp0))/(formatnumber(LodDistProp0)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			totPercDistB = ((totPercDistB)/10)
			response.Write(formatnumber(totPercDistB,2)&"%")
			%></td>
          </tr>
        </table>
      <div class="row"><br></div>
    <div class="row">
        <div class="col-lg-12 table-bordered">
    	    <div class="row text-xs-center"><b>TRANSAÇÕES E CANCELAMENTOS</b></div>
		</div>
    </div>

<%
varNomeSess = ""
dtbase = ""
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT dt_movim, statusbb, count(codigo) as QT FROM cbf801full c where dt_movim >= (SELECT dt_movim FROM cbf801full where dt_movim <= '"&dtPesq&"' group by dt_movim order by dt_movim desc limit 9,1) and statusbb in ('1','3') group by dt_movim, statusbb"
set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF
if varNomeSess = "" then
	dtbase = replace(dadosACi("dt_movim"),"/","")
	varNomeSess = dadosACi("dt_movim")
else
	if dtbase <> replace(dadosACi("dt_movim"),"/","") then
		varNomeSess = varNomeSess&","&dadosACi("dt_movim")
		dtbase = replace(dadosACi("dt_movim"),"/","")
	end if
end if
session(""&dtbase&"D") = dadosACi("dt_movim")
session(""&dtbase&"V"&dadosACi("statusbb")) = dadosACi("QT")

dadosACi.MoveNext
Wend
end if
DtBaseAr = Split(varNomeSess,",")
Vlr = 0
VlrX = 0
%>	
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left">DATA:</td>
            <td>
				<%
                response.Write(DtBaseAr(0)&"<br>")	
                response.Write("<br>"&response.Write(DiaDaSemana(DtBaseAr(0))) )	
                %>
			</td>
            <td><%
                response.Write(DtBaseAr(1)&"<br>")	
                response.Write("<br>"&response.Write(DiaDaSemana(DtBaseAr(1))) )	
                %></td>
            <td><%
                response.Write(DtBaseAr(2)&"<br>")	
                response.Write("<br>"&response.Write(DiaDaSemana(DtBaseAr(2))) )	
                %></td>
            <td><%
                response.Write(DtBaseAr(3)&"<br>")	
                response.Write("<br>"&response.Write(DiaDaSemana(DtBaseAr(3))) )	
                %></td>
				<td><%
                response.Write(DtBaseAr(4)&"<br>")	
                response.Write("<br>"&response.Write(DiaDaSemana(DtBaseAr(4))) )	
                %></td>
            <td><%
                response.Write(DtBaseAr(5)&"<br>")	
                response.Write("<br>"&response.Write(DiaDaSemana(DtBaseAr(5))) )	
                %></td>
            <td><%
                response.Write(DtBaseAr(6)&"<br>")	
                response.Write("<br>"&response.Write(DiaDaSemana(DtBaseAr(6))) )	
                %></td>
            <td><%
                response.Write(DtBaseAr(7)&"<br>")	
                response.Write("<br>"&response.Write(DiaDaSemana(DtBaseAr(7))) )	
                %></td>
            <td><%
                response.Write(DtBaseAr(8)&"<br>")	
                response.Write("<br>"&response.Write(DiaDaSemana(DtBaseAr(8))) )	
                %></td>
            <td><%
                response.Write(DtBaseAr(9)&"<br>")	
                response.Write("<br>"&response.Write(DiaDaSemana(DtBaseAr(9))) )	
                %></td>
            <td>MÉDIA</td>
          </tr>
          <tr>
            <td align="left" nowrap>TRANSAÇÕES EFETIVADAS:</td>
            <td><%
			VlrI0 = session(""&replace(DtBaseAr(0),"/","")&"V1")
			Vlr = Vlr + formatnumber(VlrI0)
			response.Write(formatnumber(VlrI0,0))
			%></td>
            <td><%
			VlrI1 = session(""&replace(DtBaseAr(1),"/","")&"V1")
			Vlr = Vlr + formatnumber(VlrI1)
			response.Write(formatnumber(VlrI1,0))
			%></td>
            <td><%
			VlrI2 = session(""&replace(DtBaseAr(2),"/","")&"V1")
			Vlr = Vlr + formatnumber(VlrI2)
			response.Write(formatnumber(VlrI2,0))
			%></td>
            <td><%
			VlrI3 = session(""&replace(DtBaseAr(3),"/","")&"V1")
			Vlr = Vlr + formatnumber(VlrI3)
			response.Write(formatnumber(VlrI3,0))
			%></td>
            <td><%
			VlrI4 = session(""&replace(DtBaseAr(4),"/","")&"V1")
			Vlr = Vlr + formatnumber(VlrI4)
			response.Write(formatnumber(VlrI4,0))
			%></td>
            <td><%
			VlrI5 = session(""&replace(DtBaseAr(5),"/","")&"V1")
			Vlr = Vlr + formatnumber(VlrI5)
			response.Write(formatnumber(VlrI5,0))
			%></td>
            <td><%
			VlrI6 = session(""&replace(DtBaseAr(6),"/","")&"V1")
			Vlr = Vlr + formatnumber(VlrI6)
			response.Write(formatnumber(VlrI6,0))
			%></td>
            <td><%
			VlrI7 = session(""&replace(DtBaseAr(7),"/","")&"V1")
			Vlr = Vlr + formatnumber(VlrI7)
			response.Write(formatnumber(VlrI7,0))
			%></td>
            <td><%
			VlrI8 = session(""&replace(DtBaseAr(8),"/","")&"V1")
			Vlr = Vlr + formatnumber(VlrI8)
			response.Write(formatnumber(VlrI8,0))
			%></td>
            <td><%
			VlrI9 = session(""&replace(DtBaseAr(9),"/","")&"V1")
			Vlr = Vlr + formatnumber(VlrI9)
			response.Write(formatnumber(VlrI9,0))
			%></td>
            <td><%
			Vlr = ((Vlr)/10)
			response.Write(formatnumber(Vlr,0))
			%></td>
          </tr>
          <tr>
            <td align="left" nowrap>TRANSAÇÕES CANCELADAS:</td>
            <td><%
			VlrIA0 = session(""&replace(DtBaseAr(0),"/","")&"V3")
			VlrX = VlrX + formatnumber(VlrIA0)
			response.Write(formatnumber(VlrIA0,0))
			%></td>
            <td><%
			VlrIA1 = session(""&replace(DtBaseAr(1),"/","")&"V3")
			VlrX = VlrX + formatnumber(VlrIA1)
			response.Write(formatnumber(VlrIA1,0))
			%></td>
            <td><%
			VlrIA2 = session(""&replace(DtBaseAr(2),"/","")&"V3")
			VlrX = VlrX + formatnumber(VlrIA2)
			response.Write(formatnumber(VlrIA2,0))
			%></td>
            <td><%
			VlrIA3 = session(""&replace(DtBaseAr(3),"/","")&"V3")
			VlrX = VlrX + formatnumber(VlrIA3)
			response.Write(formatnumber(VlrIA3,0))
			%></td>
            <td><%
			VlrIA4 = session(""&replace(DtBaseAr(4),"/","")&"V3")
			VlrX = VlrX + formatnumber(VlrIA4)
			response.Write(formatnumber(VlrIA4,0))
			%></td>
            <td><%
			VlrIA5 = session(""&replace(DtBaseAr(5),"/","")&"V3")
			VlrX = VlrX + formatnumber(VlrIA5)
			response.Write(formatnumber(VlrIA5,0))
			%></td>
            <td><%
			VlrIA6 = session(""&replace(DtBaseAr(6),"/","")&"V3")
			VlrX = VlrX + formatnumber(VlrIA6)
			response.Write(formatnumber(VlrIA6,0))
			%></td>
            <td><%
			VlrIA7 = session(""&replace(DtBaseAr(7),"/","")&"V3")
			VlrX = VlrX + formatnumber(VlrIA7)
			response.Write(formatnumber(VlrIA7,0))
			%></td>
            <td><%
			VlrIA8 = session(""&replace(DtBaseAr(8),"/","")&"V3")
			VlrX = VlrX + formatnumber(VlrIA8)
			response.Write(formatnumber(VlrIA8,0))
			%></td>
            <td><%
			VlrIA9 = session(""&replace(DtBaseAr(9),"/","")&"V3")
			VlrX = VlrX + formatnumber(VlrIA9)
			response.Write(formatnumber(VlrIA9,0))
			%></td>
            <td><%
			VlrX = ((VlrX)/10)
			response.Write(formatnumber(VlrX,0))
			%></td>
          </tr>
          <tr>
            <td align="left" nowrap>% TRANS. EFETIVADAS/CANCELADAS:</td>
            <td><%
			PercDistB = 0
			totPercDistB = 0
			if VlrI0 <> "" and VlrIA0 <> "" then
			PercDistB = (((formatnumber(VlrIA0))/(formatnumber(VlrI0)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if VlrI1 <> "" and VlrIA1 <> "" then
			PercDistB = (((formatnumber(VlrIA1))/(formatnumber(VlrI1)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if VlrI2 <> "" and VlrIA2 <> "" then
			PercDistB = (((formatnumber(VlrIA2))/(formatnumber(VlrI2)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if VlrI3 <> "" and VlrIA3 <> "" then
			PercDistB = (((formatnumber(VlrIA3))/(formatnumber(VlrI3)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if VlrI4 <> "" and VlrIA4 <> "" then
			PercDistB = (((formatnumber(VlrIA4))/(formatnumber(VlrI4)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if VlrI5 <> "" and VlrIA5 <> "" then
			PercDistB = (((formatnumber(VlrIA5))/(formatnumber(VlrI5)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if VlrI6 <> "" and VlrIA6 <> "" then
			PercDistB = (((formatnumber(VlrIA6))/(formatnumber(VlrI6)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if VlrI7 <> "" and VlrIA7 <> "" then
			PercDistB = (((formatnumber(VlrIA7))/(formatnumber(VlrI7)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if VlrI8 <> "" and VlrIA8 <> "" then
			PercDistB = (((formatnumber(VlrIA8))/(formatnumber(VlrI8)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			if VlrI9 <> "" and VlrIA9 <> "" then
			PercDistB = (((formatnumber(VlrIA9))/(formatnumber(VlrI9)))*100)
				response.Write(formatnumber(PercDistB,2)&"%")
				totPercDistB = totPercDistB + formatnumber(PercDistB)
			end if
			%></td>
            <td><%
			totPercDistB = ((totPercDistB)/10)
			response.Write(formatnumber(totPercDistB,2)&"%")
			%></td>
          </tr>
        </table>
      <div class="row"><br></div>
    <div class="row">
        <div class="col-lg-12 table-bordered">
    	    <div class="row text-xs-center"><b>CONSULTA SOLICITAÇÕES DE IMPRESSÃO DE 2º VIA</b></div>
		</div>
    </div>
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left">DATA:</td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_solic_segund r where data <= '"&dtPesq&"' order by data desc limit 9,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_solic_segund r where data <= '"&dtPesq&"' order by data desc limit 8,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_solic_segund r where data <= '"&dtPesq&"' order by data desc limit 7,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_solic_segund r where data <= '"&dtPesq&"' order by data desc limit 6,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_solic_segund r where data <= '"&dtPesq&"' order by data desc limit 5,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_solic_segund r where data <= '"&dtPesq&"' order by data desc limit 4,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_solic_segund r where data <= '"&dtPesq&"' order by data desc limit 3,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_solic_segund r where data <= '"&dtPesq&"' order by data desc limit 2,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_solic_segund r where data <= '"&dtPesq&"' order by data desc limit 1,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td><%
			Dia =PesqResp("SELECT data as Resp FROM rel_solic_segund r where data <= '"&dtPesq&"' order by data desc limit 0,1" )
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
				
			%></td>
            <td>MÉDIA</td>
          </tr>
          <tr>
            <td align="left">QT. SOLICITADA:</td>
            <td>
				<%
				totSolic = 0
				totAutor = 0
					Solic = PesqValorRel("SELECT solic as Resp FROM rel_solic_segund where data <= '"&dtPesq&"'  order by data desc limit 9,1" )
					Autor = PesqValorRel("SELECT autor as Resp FROM rel_solic_segund where data <= '"&dtPesq&"' order by data desc limit 9,1" )
					response.Write(formatnumber(Solic, 0))
					totSolic = totSolic + formatnumber(Solic)
					totAutor = totAutor + formatnumber(Autor)
					Autor9 = Autor
  			     %>
            </td>
            <td><%
					Solic = PesqValorRel("SELECT solic as Resp FROM rel_solic_segund where data <= '"&dtPesq&"'  order by data desc limit 8,1" )
					Autor = PesqValorRel("SELECT autor as Resp FROM rel_solic_segund where data <= '"&dtPesq&"' order by data desc limit 8,1" )
					response.Write(formatnumber(Solic, 0))
					totSolic = totSolic + formatnumber(Solic)
					totAutor = totAutor + formatnumber(Autor)
					Autor8 = Autor
  			     %></td>
            <td><%
					Solic = PesqValorRel("SELECT solic as Resp FROM rel_solic_segund where data <= '"&dtPesq&"'  order by data desc limit 7,1" )
					Autor = PesqValorRel("SELECT autor as Resp FROM rel_solic_segund where data <= '"&dtPesq&"' order by data desc limit 7,1" )
					response.Write(formatnumber(Solic, 0))
					totSolic = totSolic + formatnumber(Solic)
					totAutor = totAutor + formatnumber(Autor)
					Autor7 = Autor
  			     %></td>
            <td><%
					Solic = PesqValorRel("SELECT solic as Resp FROM rel_solic_segund where data <= '"&dtPesq&"'  order by data desc limit 6,1" )
					Autor = PesqValorRel("SELECT autor as Resp FROM rel_solic_segund where data <= '"&dtPesq&"' order by data desc limit 6,1" )
					response.Write(formatnumber(Solic, 0))
					totSolic = totSolic + formatnumber(Solic)
					totAutor = totAutor + formatnumber(Autor)
					Autor6 = Autor
  			     %></td>
            <td><%
					Solic = PesqValorRel("SELECT solic as Resp FROM rel_solic_segund where data <= '"&dtPesq&"'  order by data desc limit 5,1" )
					Autor = PesqValorRel("SELECT autor as Resp FROM rel_solic_segund where data <= '"&dtPesq&"' order by data desc limit 5,1" )
					response.Write(formatnumber(Solic, 0))
					totSolic = totSolic + formatnumber(Solic)
					totAutor = totAutor + formatnumber(Autor)
					Autor5 = Autor
  			     %></td>
            <td><%
					Solic = PesqValorRel("SELECT solic as Resp FROM rel_solic_segund where data <= '"&dtPesq&"'  order by data desc limit 4,1" )
					Autor = PesqValorRel("SELECT autor as Resp FROM rel_solic_segund where data <= '"&dtPesq&"' order by data desc limit 4,1" )
					response.Write(formatnumber(Solic, 0))
					totSolic = totSolic + formatnumber(Solic)
					totAutor = totAutor + formatnumber(Autor)
					Autor4 = Autor
  			     %></td>
            <td><%
					Solic = PesqValorRel("SELECT solic as Resp FROM rel_solic_segund where data <= '"&dtPesq&"'  order by data desc limit 3,1" )
					Autor = PesqValorRel("SELECT autor as Resp FROM rel_solic_segund where data <= '"&dtPesq&"' order by data desc limit 3,1" )
					response.Write(formatnumber(Solic, 0))
					totSolic = totSolic + formatnumber(Solic)
					totAutor = totAutor + formatnumber(Autor)
					Autor3 = Autor
  			     %></td>
            <td><%
					Solic = PesqValorRel("SELECT solic as Resp FROM rel_solic_segund where data <= '"&dtPesq&"'  order by data desc limit 2,1" )
					Autor = PesqValorRel("SELECT autor as Resp FROM rel_solic_segund where data <= '"&dtPesq&"' order by data desc limit 2,1" )
					response.Write(formatnumber(Solic, 0))
					totSolic = totSolic + formatnumber(Solic)
					totAutor = totAutor + formatnumber(Autor)
					Autor2 = Autor
  			     %></td>
            <td><%
					Solic = PesqValorRel("SELECT solic as Resp FROM rel_solic_segund where data <= '"&dtPesq&"'  order by data desc limit 1,1" )
					Autor = PesqValorRel("SELECT autor as Resp FROM rel_solic_segund where data <= '"&dtPesq&"' order by data desc limit 1,1" )
					response.Write(formatnumber(Solic, 0))
					totSolic = totSolic + formatnumber(Solic)
					totAutor = totAutor + formatnumber(Autor)
					Autor1 = Autor
  			     %></td>
            <td><%
					Solic = PesqValorRel("SELECT solic as Resp FROM rel_solic_segund where data <= '"&dtPesq&"'  order by data desc limit 0,1" )
					Autor = PesqValorRel("SELECT autor as Resp FROM rel_solic_segund where data <= '"&dtPesq&"' order by data desc limit 0,1" )
					response.Write(formatnumber(Solic, 0))
					totSolic = totSolic + formatnumber(Solic)
					totAutor = totAutor + formatnumber(Autor)
					Autor0 = Autor
  			     %></td>
            <td>
			<%
			totSolic = ((totSolic)/10)
			response.Write(formatnumber(totSolic,1))
			%>
            </td>
          </tr>
          <tr>
            <td align="left">QT. AUTORIZADA:</td>
            <td><%
					response.Write(formatnumber(Autor9, 0))
  			     %></td>
            <td><%
					response.Write(formatnumber(Autor8, 0))
  			     %></td>
            <td><%
					response.Write(formatnumber(Autor7, 0))
  			     %></td>
            <td><%
					response.Write(formatnumber(Autor6, 0))
  			     %></td>
            <td><%
					response.Write(formatnumber(Autor5, 0))
  			     %></td>
            <td><%
					response.Write(formatnumber(Autor4, 0))
  			     %></td>
            <td><%
					response.Write(formatnumber(Autor3, 0))
  			     %></td>
            <td><%
					response.Write(formatnumber(Autor2, 0))
  			     %></td>
            <td><%
					response.Write(formatnumber(Autor1, 0))
  			     %></td>
            <td><%
					response.Write(formatnumber(Autor0, 0))
  			     %></td>
            <td>			
			<%
			totAutor = ((totAutor)/10)
			response.Write(formatnumber(totAutor,1))
			%></td>
          </tr>
        </table>

      <div class="row"><br></div>

    <div class="row">
        <div class="col-lg-12 table-bordered">
    	    <div class="row text-xs-center"><b>FECHAMENTO DE CAIXA NÃO EFETUADO</b></div>
		</div>
    </div>
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left">DATA:</td>
            <td><%
			TotQuatDiaA = 0
			TotQuatDiaF = 0
			Dia =PesqResp("SELECT f.datacx as Resp, DAYNAME(datacx) as DiaSem FROM financeiro f where datacx <= '"&dtPesq&"' and local not like  'J%'   group by datacx having DiaSem <> 'Saturday' and DiaSem <> 'Sunday' order by datacx desc limit 9,1" )
			QuatDiaA9 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx is NULL or j.std_cx = 'Anexar' )" )
			QuatDiaF9 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx = 'Em Conferência' or j.std_cx = 'Fechado' )" )
			TotQuatDiaA = TotQuatDiaA + formatnumber(QuatDiaA9)
			TotQuatDiaF = TotQuatDiaF + formatnumber(QuatDiaF9)
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td><%
			Dia =PesqResp("SELECT f.datacx as Resp, DAYNAME(datacx) as DiaSem FROM financeiro f where datacx <= '"&dtPesq&"' and local not like  'J%'   group by datacx having DiaSem <> 'Saturday' and DiaSem <> 'Sunday' order by datacx desc limit 8,1" )
			QuatDiaA8 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx is NULL or j.std_cx = 'Anexar' )" )
			QuatDiaF8 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx = 'Em Conferência' or j.std_cx = 'Fechado' )" )
			TotQuatDiaA = TotQuatDiaA + formatnumber(QuatDiaA8)
			TotQuatDiaF = TotQuatDiaF + formatnumber(QuatDiaF8)
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td><%
			Dia =PesqResp("SELECT f.datacx as Resp, DAYNAME(datacx) as DiaSem FROM financeiro f where datacx <= '"&dtPesq&"' and local not like  'J%'   group by datacx having DiaSem <> 'Saturday' and DiaSem <> 'Sunday' order by datacx desc limit 7,1" )
			QuatDiaA7 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx is NULL or j.std_cx = 'Anexar' )" )
			QuatDiaF7 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx = 'Em Conferência' or j.std_cx = 'Fechado' )" )
			TotQuatDiaA = TotQuatDiaA + formatnumber(QuatDiaA7)
			TotQuatDiaF = TotQuatDiaF + formatnumber(QuatDiaF7)
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td><%
			Dia =PesqResp("SELECT f.datacx as Resp, DAYNAME(datacx) as DiaSem FROM financeiro f where datacx <= '"&dtPesq&"' and local not like  'J%'   group by datacx having DiaSem <> 'Saturday' and DiaSem <> 'Sunday' order by datacx desc limit 6,1" )
			QuatDiaA6 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx is NULL or j.std_cx = 'Anexar' )" )
			QuatDiaF6 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx = 'Em Conferência' or j.std_cx = 'Fechado' )" )
			TotQuatDiaA = TotQuatDiaA + formatnumber(QuatDiaA6)
			TotQuatDiaF = TotQuatDiaF + formatnumber(QuatDiaF6)
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td><%
			Dia =PesqResp("SELECT f.datacx as Resp, DAYNAME(datacx) as DiaSem FROM financeiro f where datacx <= '"&dtPesq&"' and local not like  'J%'   group by datacx having DiaSem <> 'Saturday' and DiaSem <> 'Sunday' order by datacx desc limit 5,1" )
			QuatDiaA5 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx is NULL or j.std_cx = 'Anexar' )" )
			QuatDiaF5 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx = 'Em Conferência' or j.std_cx = 'Fechado' )" )
			TotQuatDiaA = TotQuatDiaA + formatnumber(QuatDiaA5)
			TotQuatDiaF = TotQuatDiaF + formatnumber(QuatDiaF5)
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td><%
			Dia =PesqResp("SELECT f.datacx as Resp, DAYNAME(datacx) as DiaSem FROM financeiro f where datacx <= '"&dtPesq&"' and local not like  'J%'   group by datacx having DiaSem <> 'Saturday' and DiaSem <> 'Sunday' order by datacx desc limit 4,1" )
			QuatDiaA4 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx is NULL or j.std_cx = 'Anexar' )" )
			QuatDiaF4 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx = 'Em Conferência' or j.std_cx = 'Fechado' )" )
			TotQuatDiaA = TotQuatDiaA + formatnumber(QuatDiaA4)
			TotQuatDiaF = TotQuatDiaF + formatnumber(QuatDiaF4)
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td><%
			Dia =PesqResp("SELECT f.datacx as Resp, DAYNAME(datacx) as DiaSem FROM financeiro f where datacx <= '"&dtPesq&"' and local not like  'J%'   group by datacx having DiaSem <> 'Saturday' and DiaSem <> 'Sunday' order by datacx desc limit 3,1" )
			QuatDiaA3 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx is NULL or j.std_cx = 'Anexar' )" )
			QuatDiaF3 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx = 'Em Conferência' or j.std_cx = 'Fechado' )" )
			TotQuatDiaA = TotQuatDiaA + formatnumber(QuatDiaA3)
			TotQuatDiaF = TotQuatDiaF + formatnumber(QuatDiaF3)
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td><%
			Dia =PesqResp("SELECT f.datacx as Resp, DAYNAME(datacx) as DiaSem FROM financeiro f where datacx <= '"&dtPesq&"' and local not like  'J%'   group by datacx having DiaSem <> 'Saturday' and DiaSem <> 'Sunday' order by datacx desc limit 2,1" )
			QuatDiaA2 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx is NULL or j.std_cx = 'Anexar' )" )
			QuatDiaF2 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx = 'Em Conferência' or j.std_cx = 'Fechado' )" )
			TotQuatDiaA = TotQuatDiaA + formatnumber(QuatDiaA2)
			TotQuatDiaF = TotQuatDiaF + formatnumber(QuatDiaF2)
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td><%
			Dia =PesqResp("SELECT f.datacx as Resp, DAYNAME(datacx) as DiaSem FROM financeiro f where datacx <= '"&dtPesq&"' and local not like  'J%'   group by datacx having DiaSem <> 'Saturday' and DiaSem <> 'Sunday' order by datacx desc limit 1,1" )
			QuatDiaA1 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx is NULL or j.std_cx = 'Anexar' )" )
			QuatDiaF1 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx = 'Em Conferência' or j.std_cx = 'Fechado' )" )
			TotQuatDiaA = TotQuatDiaA + formatnumber(QuatDiaA1)
			TotQuatDiaF = TotQuatDiaF + formatnumber(QuatDiaF1)
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td><%
			Dia =PesqResp("SELECT f.datacx as Resp, DAYNAME(datacx) as DiaSem FROM financeiro f where datacx <= '"&dtPesq&"' and local not like  'J%'   group by datacx having DiaSem <> 'Saturday' and DiaSem <> 'Sunday' order by datacx desc limit 0,1" )
			QuatDiaA0 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx is NULL or j.std_cx = 'Anexar' )" )
			QuatDiaF0 =PesqResp("SELECT  count(f.codigo) as Resp FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx = 'Em Conferência' or j.std_cx = 'Fechado' )" )
			TotQuatDiaA = TotQuatDiaA + formatnumber(QuatDiaA0)
			TotQuatDiaF = TotQuatDiaF + formatnumber(QuatDiaF0)
			response.Write(Dia&"<br>")	
			response.Write("<br>"&response.Write(DiaDaSemana(Dia)) )	
			%></td>
            <td>MÉDIA</td>
          </tr>
          <tr>
            <td align="left">QT. CAIXAS NÃO FECHADOS:</td>
            <td><%=response.Write(formatnumber(QuatDiaA9, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaA8, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaA7, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaA6, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaA5, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaA4, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaA3, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaA2, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaA1, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaA0, 0))%></td>
            <td><%
			TotQuatDiaA = ((TotQuatDiaA)/10)
			response.Write(formatnumber(TotQuatDiaA,1))
			%></td>
          </tr>
          <tr>
            <td align="left">QT. CAIXAS NÃO CONFERIDOS:</td>
            <td><%=response.Write(formatnumber(QuatDiaF9, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaF8, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaF7, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaF6, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaF5, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaF4, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaF3, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaF2, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaF1, 0))%></td>
            <td><%=response.Write(formatnumber(QuatDiaF0, 0))%></td>
            <td><%
			TotQuatDiaF = ((TotQuatDiaF)/10)
			response.Write(formatnumber(TotQuatDiaF,1))
			%></td>
          </tr>
        </table>

<%
Lojas = ""
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT  f.* FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and ( j.std_cx is NULL or j.std_cx = 'Anexar' )"
set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF

if Lojas = "" then
	Lojas = dadosACi("local")&"-"&NomeLojaBB(dadosACi("local"), "nome")
else
	Lojas = Lojas&"; "&dadosACi("local")&"-"&NomeLojaBB(dadosACi("local"), "nome")
end if

dadosACi.MoveNext
Wend
end if
%>	

<%
LojasiNC = ""
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT  f.* FROM financeiro f  left JOIN financeiro_lj j ON f.lojabb=j.lojabb and f.datacx=j.datacx where  f.datacx = '"&ConvData(Dia)&"' and f.local not like  'J%' and  j.std_cx = 'Inconclusivo' "
set dadosInc = objconnACi.execute(instrucao_sqlACi)
if dadosInc.EOF then
else
dadosInc.MoveFirst
While Not dadosInc.EOF

if LojasiNC = "" then
	LojasiNC = dadosInc("local")&"-"&NomeLojaBB(dadosInc("local"), "nome")
else
	LojasiNC = LojasiNC&"; "&dadosInc("local")&"-"&NomeLojaBB(dadosInc("local"), "nome")
end if

dadosInc.MoveNext
Wend
end if
%>	

<%
LojasSemoP = ""
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT cl.lojabb as local, (select codigo from financeiro where lojabb=cl.lojabb and datacx = '"&ConvData(Dia)&"' limit 0,1) as codigo FROM cad_lojabb cl where convenio = '49093' and mci = '211217963' having codigo is NULL"
set dadosSemOP = objconnACi.execute(instrucao_sqlACi)
if dadosSemOP.EOF then
else
dadosSemOP.MoveFirst
While Not dadosSemOP.EOF

if LojasSemoP = "" then
	LojasSemoP = dadosSemOP("local")&"-"&NomeLojaBB(dadosSemOP("local"), "nome")
else
	LojasSemoP = LojasSemoP&"; "&dadosSemOP("local")&"-"&NomeLojaBB(dadosSemOP("local"), "nome")
end if

dadosSemOP.MoveNext
Wend
end if
%>	


<%
if Lojas <> "" then
%>
    <div class="row">
        <div class="col-lg-12">
    	    <div class="row text-xs-left">&nbsp;&nbsp;LOJAS QUE NÃO FECHARAM: <%=Lojas%>.</div> 
		</div>
    </div>
<%
end if
%>
<%
if LojasiNC <> "" then
%>

    <div class="row">
        <div class="col-lg-12">
    	    <div class="row text-xs-left">&nbsp;&nbsp;CONFERÊNCIA DE CAIXA INCONCLUSIVO: <%=LojasiNC%>.</div> 
		</div>
    </div>
<%
end if
%>


<%
if LojasSemoP <> "" then
%>
    <div class="row">
        <div class="col-lg-12">
    	    <div class="row text-xs-left">&nbsp;&nbsp;LOJAS SEM OPERAÇÕES: <%=LojasSemoP%>.</div> 
		</div>
    </div>

<%
end if
%>


<div style="page-break-before: right;"> </div>
    <div class="row">
        <div class="col-lg-12 table-bordered">
    	    <div class="row text-xs-center"><b>PERNOITE</b></div>
		</div>
    </div>
<div class="row">
<table width="98%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td><table border="0" cellspacing="0" cellpadding="0" bordercolor="#999999">
  <tr>
    <td><div class="verticalText" ><br><br><%=PernPrinc%></div></td>
  </tr>
</table>
</td>
    <td valign="top"><table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#999999">
  <tr>
    <td rowspan="2" align="center">LOJA</td>
    <td colspan="5" align="center">SÉRIE HISTÓRICA</td>
    <td colspan="3" align="center">ARMAZENAMENTO DE NUMERARIO</td>
    <td rowspan="2" align="center" nowrap>HORARIO DE <br>
      RECOLHIMENTO</td>
    <td rowspan="2" align="center" nowrap>QUANT.<br>
TRANSAÇÕES</td>
    <td rowspan="2" align="center" nowrap>VALOR<br>
      TRANSAÇÕES</td>
    <td rowspan="2" align="center" nowrap>ULTIMA<br>
      TRANSAÇÃO</td>
  </tr>
  <tr>
    <td><%=PernPri%></td>
    <td><%=PernSeg%></td>
    <td><%=PernTer%></td>
    <td><%=PernQua%></td>
    <td><%=PernPrinc%></td>
    <td align="right">COMPUSAFE</td>
    <td align="right">ADMINISTRATIVO</td>
    <td align="right">BOCA DE LOBO</td>
    </tr>
<%
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT f.*, (select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernPri)&"' ) as Pri, (select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernSeg)&"' ) as Seg,(select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernTer)&"' ) as Ter,(select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernQua)&"' ) as Qua, (f.saldo_comp + f.saldo_cofreadm + f.saldo_bocadelobo) as totCofre, (SELECT mid(data, 12, 5) FROM financeiro_mov f where datacx='"&ConvData(PernPrinc)&"' and tipo like '(%' and chavej=f.lojabb order by codigo desc limit 0,1) as hr, (select count(codigo) from acbfull where dt_trans = '"&ConvData(PernPrinc)&"' and lojabb=f.lojabb and convenio='49093') as qt, (select mid(max(dh_operacao),12,8) from acbfull where dt_trans = '"&ConvData(PernPrinc)&"' and lojabb=f.lojabb and convenio='49093') as ult FROM financeiro_lj f where datacx='"&ConvData(PernPrinc)&"' and std_cx='Conferido' and (saldo_comp + saldo_cofreadm + saldo_bocadelobo) > 0  order by totCofre desc"
set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF
%>
  <tr>
    <td align="left" nowrap><%=dadosACi("lojabb")%> - <%=NomeLojaBB(dadosACi("lojabb"), "nome")%>(<%=NomeLojaBB(dadosACi("lojabb"), "uf")%>)</td>
    <td nowrap><%=Currenc(dadosACi("Pri"))%></td>
    <td nowrap><%=Currenc(dadosACi("Seg"))%></td>
    <td nowrap><%=Currenc(dadosACi("Ter"))%></td>
    <td nowrap><%=Currenc(dadosACi("Qua"))%></td>
    <td nowrap><%=Currenc(dadosACi("totCofre"))%></td>
    <td nowrap><%=Currenc(dadosACi("saldo_comp"))%></td>
    <td nowrap><%=Currenc(dadosACi("saldo_cofreadm"))%></td>
    <td nowrap><%=Currenc(dadosACi("saldo_bocadelobo"))%></td>
    <td nowrap align="center"><%=dadosACi("hr")%></td>
    <td nowrap align="center"><%=dadosACi("qt")%></td>
    <td nowrap><%=Currenc(dadosACi("rec"))%></td>
    <td nowrap align="center"><%=dadosACi("ult")%></td>
  </tr>
<%
dadosACi.MoveNext
Wend
end if
%>	
</table>
</td>
  </tr>
</table>
<%
PernPrinc = PernQua
PernQua = PernTer
PernTer = PernSeg
PernSeg = PernPri
PernPri = PernPriA 
%>
<!-- Paginacao -->
<table width="98%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td><table border="0" cellspacing="0" cellpadding="0" bordercolor="#999999">
  <tr>
    <td><div class="verticalText" ><br><br><%=PernPrinc%></div></td>
  </tr>
</table>
</td>
    <td valign="top"><table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#999999">
  <tr>
    <td rowspan="2" align="center">LOJA</td>
    <td colspan="5" align="center">SÉRIE HISTÓRICA</td>
    <td colspan="3" align="center">ARMAZENAMENTO DE NUMERARIO</td>
    <td rowspan="2" align="center" nowrap>HORARIO DE <br>
      RECOLHIMENTO</td>
    <td rowspan="2" align="center" nowrap>QUANT.<br>
TRANSAÇÕES</td>
    <td rowspan="2" align="center" nowrap>VALOR<br>
      TRANSAÇÕES</td>
    <td rowspan="2" align="center" nowrap>ULTIMA<br>
      TRANSAÇÃO</td>
  </tr>
  <tr>
    <td><%=PernPri%></td>
    <td><%=PernSeg%></td>
    <td><%=PernTer%></td>
    <td><%=PernQua%></td>
    <td><%=PernPrinc%></td>
    <td align="right">COMPUSAFE</td>
    <td align="right">ADMINISTRATIVO</td>
    <td align="right">BOCA DE LOBO</td>
    </tr>
<%
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT f.*, (select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernPri)&"' ) as Pri, (select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernSeg)&"' ) as Seg,(select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernTer)&"' ) as Ter,(select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernQua)&"' ) as Qua, (f.saldo_comp + f.saldo_cofreadm + f.saldo_bocadelobo) as totCofre, (SELECT mid(data, 12, 5) FROM financeiro_mov f where datacx='"&ConvData(PernPrinc)&"' and tipo like '(%' and chavej=f.lojabb order by codigo desc limit 0,1) as hr, (select count(codigo) from acbfull where dt_trans = '"&ConvData(PernPrinc)&"' and lojabb=f.lojabb  and convenio='49093') as qt, (select mid(max(dh_operacao),12,8) from acbfull where dt_trans = '"&ConvData(PernPrinc)&"' and lojabb=f.lojabb  and convenio='49093') as ult FROM financeiro_lj f where datacx='"&ConvData(PernPrinc)&"' and std_cx='Conferido' and (saldo_comp + saldo_cofreadm + saldo_bocadelobo) > 0 order by totCofre desc"
set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF
%>
  <tr>
    <td align="left" nowrap><%=dadosACi("lojabb")%> - <%=NomeLojaBB(dadosACi("lojabb"), "nome")%>(<%=NomeLojaBB(dadosACi("lojabb"), "uf")%>)</td>
    <td><%=Currenc(dadosACi("Pri"))%></td>
    <td><%=Currenc(dadosACi("Seg"))%></td>
    <td><%=Currenc(dadosACi("Ter"))%></td>
    <td><%=Currenc(dadosACi("Qua"))%></td>
    <td><%=Currenc(dadosACi("totCofre"))%></td>
    <td><%=Currenc(dadosACi("saldo_comp"))%></td>
    <td><%=Currenc(dadosACi("saldo_cofreadm"))%></td>
    <td><%=Currenc(dadosACi("saldo_bocadelobo"))%></td>
    <td align="center"><%=dadosACi("hr")%></td>
    <td align="center"><%=dadosACi("qt")%></td>
    <td><%=Currenc(dadosACi("rec"))%></td>
    <td align="center"><%=dadosACi("ult")%></td>
  </tr>
<%
dadosACi.MoveNext
Wend
end if
%>	
</table>
</td>
  </tr>
</table>
<%
PernPrinc = PernQua
PernQua = PernTer
PernTer = PernSeg
PernSeg = PernPri
PernPri = PernPriB  
%>
<!-- Paginacao -->

<table width="98%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td><table border="0" cellspacing="0" cellpadding="0" bordercolor="#999999">
  <tr>
    <td><div class="verticalText" ><br><br><%=PernPrinc%></div></td>
  </tr>
</table>
</td>
    <td valign="top"><table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#999999">
  <tr>
    <td rowspan="2" align="center">LOJA</td>
    <td colspan="5" align="center">SÉRIE HISTÓRICA</td>
    <td colspan="3" align="center">ARMAZENAMENTO DE NUMERARIO</td>
    <td rowspan="2" align="center" nowrap>HORARIO DE <br>
      RECOLHIMENTO</td>
    <td rowspan="2" align="center" nowrap>QUANT.<br>
TRANSAÇÕES</td>
    <td rowspan="2" align="center" nowrap>VALOR<br>
      TRANSAÇÕES</td>
    <td rowspan="2" align="center" nowrap>ULTIMA<br>
      TRANSAÇÃO</td>
  </tr>
  <tr>
    <td><%=PernPri%></td>
    <td><%=PernSeg%></td>
    <td><%=PernTer%></td>
    <td><%=PernQua%></td>
    <td><%=PernPrinc%></td>
    <td align="right">COMPUSAFE</td>
    <td align="right">ADMINISTRATIVO</td>
    <td align="right">BOCA DE LOBO</td>
    </tr>
<%
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT f.*, (select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernPri)&"' ) as Pri, (select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernSeg)&"' ) as Seg,(select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernTer)&"' ) as Ter,(select (saldo_comp + saldo_cofreadm + saldo_bocadelobo) from financeiro_lj where lojabb=f.lojabb and datacx='"&ConvData(PernQua)&"' ) as Qua, (f.saldo_comp + f.saldo_cofreadm + f.saldo_bocadelobo) as totCofre, (SELECT mid(data, 12, 5) FROM financeiro_mov f where datacx='"&ConvData(PernPrinc)&"' and tipo like '(%' and chavej=f.lojabb order by codigo desc limit 0,1) as hr, (select count(codigo) from acbfull where dt_trans = '"&ConvData(PernPrinc)&"' and lojabb=f.lojabb  and convenio='49093') as qt, (select mid(max(dh_operacao),12,8) from acbfull where dt_trans = '"&ConvData(PernPrinc)&"' and lojabb=f.lojabb  and convenio='49093') as ult FROM financeiro_lj f where datacx='"&ConvData(PernPrinc)&"' and std_cx='Conferido' and (saldo_comp + saldo_cofreadm + saldo_bocadelobo) > 0 order by totCofre desc"
set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF
%>
  <tr>
    <td align="left" nowrap><%=dadosACi("lojabb")%> - <%=NomeLojaBB(dadosACi("lojabb"), "nome")%>(<%=NomeLojaBB(dadosACi("lojabb"), "uf")%>)</td>
    <td><%=Currenc(dadosACi("Pri"))%></td>
    <td><%=Currenc(dadosACi("Seg"))%></td>
    <td><%=Currenc(dadosACi("Ter"))%></td>
    <td><%=Currenc(dadosACi("Qua"))%></td>
    <td><%=Currenc(dadosACi("totCofre"))%></td>
    <td><%=Currenc(dadosACi("saldo_comp"))%></td>
    <td><%=Currenc(dadosACi("saldo_cofreadm"))%></td>
    <td><%=Currenc(dadosACi("saldo_bocadelobo"))%></td>
    <td align="center"><%=dadosACi("hr")%></td>
    <td align="center"><%=dadosACi("qt")%></td>
    <td><%=Currenc(dadosACi("rec"))%></td>
    <td align="center"><%=dadosACi("ult")%></td>
  </tr>
<%
dadosACi.MoveNext
Wend
end if
%>	
</table>
</td>
  </tr>
</table>
</div>

<div style="page-break-before: right;"> </div>
      <div class="row"><br></div>
    <div class="row">
        <div class="col-lg-12 table-bordered">
    	    <div class="row text-xs-center"><b>ALÍVIO DE NUMERÁRIO</b></div>
		</div>
    </div>

<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#999999">
	 <tr>
	    <td align="center" nowrap> LOJA</td>
		<td align="center" nowrap>GTV</td>
		<td align="center" nowrap>VALOR APURADO</td>
		<td align="center" nowrap>DIFERENÇA</td>
		<td align="center" nowrap>DEPOSITO (AGENCIA)</td>
		<td align="center" nowrap>DEPOSITO (CSO)</td>
		<td align="center" nowrap>ALIVIO SENSIBILIZADO</td>
		<td align="center" nowrap>DIFERENCA DE ALIVIO</td>
		<td aling="center" nowrap><center>OBSERVACAO</center></td>
	</tr>
<%
dt = ""
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT * FROM financeiro_lj_gtv where observacao <> '' and datacx >= DATE_ADD('"&dtPesq&"',INTERVAL -22 DAY) order by datacx desc, lojabb"
set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF
%>
<% if dt <> dadosACi("datacx") then %>
	<tr>
		<td colspan="9" align="center" nowrap><b><%=dadosACi("datacx")%></b></td>
	</tr>
<%
dt = dadosACi("datacx")
end if
%>

	<tr>
		<td align="left" nowrap><%=dadosACi("lojabb")%> - <%=NomeLojaBB(dadosACi("lojabb"), "nome")%>(<%=NomeLojaBB(dadosACi("lojabb"), "uf")%>)</td>
		<td nowrap><%=formatcurrency(dadosACi("total_gtv"),2)%></td>
		<td nowrap><%=formatcurrency(dadosACi("valor_apurado"),2)%></td>
		<td nowrap><%=formatcurrency(dadosACi("diferenca"),2)%></td>
		<td nowrap><%=formatcurrency(dadosACi("deposito_agencia"),2)%></td>
		<td nowrap><%=formatcurrency(dadosACi("deposito_cso"),2)%></td>
		<td nowrap><%=formatcurrency(dadosACi("alivio"),2)%></td>
		<td nowrap><%=formatcurrency(dadosACi("diferenca_alivio"),2)%></td>
		<td align="center"><%=dadosACi("observacao")%></td>
	</tr>

<%
dadosACi.MoveNext
Wend
end if
%>	

</table>
<BR>

  </div>
</div>
<%end if%>
<%end if%>