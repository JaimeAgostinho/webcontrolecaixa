       <!--Importa as bibliotecas necessárias-->
        <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
        <script src="http://tablesorter.com/__jquery.tablesorter.min.js" type="text/javascript"></script> 

<% if request("log") = "s" then%>


<%end if%>
  


<div class="row">
    <div class="col-sm-12">
		<%=CabecalhoUrl("Posição Diária Detalhada - New")%> 
    </div>
</div>

<form id="acesso" name="acesso" method="post" action="adm.asp?tc=<%=response.Write(request("tc"))%>&url=cleanup_final&Orb=var&log=s">
<div class="row">

    <div class="col-sm-2">
	    <input class="form-control input-sm" type="text" name="data" maxlength="10" onKeyDown="MascaraForm(this,Data);" onKeyPress="MascaraForm(this,Data);" onKeyUp="MascaraForm(this,Data);" id="data" placeholder="Data:" value="<% if request("log") = "s" then%><%=request.form("data")%><%else%><%=date()%><%end if%>"/>
    </div>
    <div class="col-sm-4">
	    <input class="form-control input-sm" type="text" name="nome" value="<%=request("nome")%>" placeholder="Informe a Loja!"/>
    </div>

    <div class="col-sm-2 btn-group-sm">
        <select class="form-control input-sm" id="orderby" name="orderby">
              <option value="Nlojabb" <% if request("orderby") = "Nlojabb" then%>selected<%end if%>>Loja</option>
              <option value="Ndata" <% if request("orderby") = "Ndata" then%>selected<%end if%>>Hora</option>
              <option value="std_cx" <% if request("orderby") = "std_cx" then%>selected<%end if%>>Status</option>
              <option value="saldo_in" <% if request("orderby") = "saldo_in" then%>selected<%end if%>>Remanescente</option>
              <option value="rec" <% if request("orderby") = "rec" then%>selected<%end if%>>Recebimento</option>
              <option value="pag" <% if request("orderby") = "pag" then%>selected<%end if%>>Pagamento</option>
              <option value="saldo_comp" <% if request("orderby") = "saldo_comp" then%>selected<%end if%>>CompuSafe</option>
              <option value="saldo_cofreadm" <% if request("orderby") = "saldo_cofreadm" then%>selected<%end if%>>Cofre ADM</option>
              <option value="saldo_bocadelobo" <% if request("orderby") = "saldo_bocadelobo" then%>selected<%end if%>>Boca Lobo</option>
            </select>                      
    </div>

    <div class="col-sm-1 btn-group-sm">
		<input class="btn btn-primary btn-lg btn-block" type="submit" name="bt" id="bt" value="Pesquisar" />
    </div>

</div>
</form>
<br>
<% if request("log") = "s" then%>

<div class="row">
<div>
    <div class="col-sm-0"></div>
    <div class="col-sm-11">
    <table class="table table-striped table-bordered ">
	<thead>
        <tr> 
            <th>Loja</th>	
            <th NOWRAP>Nome</th>	
            <th NOWRAP align="right">Ult.OP</th>	
            <th NOWRAP align="right">Fech.</th>	
            <th NOWRAP align="right">Conf.</th>	
            <th NOWRAP align="right">Status</th>	
            <th NOWRAP align="right">Reman.</th>	
            <th align="right">Receb.</th>	
            <th align="right">Pagam.</th>	
            <th NOWRAP align="right">Alivios&nbsp;BB</th>	
            <th NOWRA Palign="right">Saldo&nbsp;Parcial</th>	
            <th NOWRAP align="right">GTV's</th>	
            <th NOWRAP align="right">Valor&nbsp;Caixa</th>	
            <th NOWRAP align="right">Dif.&nbsp;Caixa</th>
            <th align="right">Dif.&nbsp;Credora</th>	
            <th align="right">CompuSafe</th>	
            <th align="right">Cofre&nbsp;ADM</th>	
            <th align="right">BocaLobo</th>	
            <th align="right">Retaguarda</th>	
        </tr>
        </thead>
         <tbody>
<%
where = ""
	where = where&" nome like '%"&request.Form("nome")&"%'"
if request.form("lojabb") <> "" then
	where = where&" and lojabb = '"&request.Form("lojabb")&"'"
end if 
if request.form("tipo_loja") <> "" then
	where = where&" and tipo_loja = '"&request.Form("tipo_loja")&"'"
end if 

%>         
<%
Tsaldo_in = 0
Trec = 0
Tpag = 0
Talivio = 0
TSldAntCon = 0
TNgtv = 0
TSldCx = 0
Ttot_difcxmenor = 0
Ttot_difcxmaior = 0
Tsaldo_comp = 0
Tsaldo_cofreadm = 0
Tsaldo_bocadelobo = 0
TSldreta = 0

Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT fl.codigo as CdFc, c.lojabb as Nlojabb, c.nome, mid(f.data, 12, 5) as Ndata, mid(fl.data, 12, 5) as Fdata, mid(fl.obs_conf, 12, 5) as Cdata, fl.std_cx, if(fl.saldo_in is NULL, f.saldo_in, fl.saldo_in) as saldo_in, fl.saldo_cofreadm, fl.saldo_bocadelobo,"
instrucao_sqlACi = instrucao_sqlACi &" if(fl.rec is NULL, f.rec, fl.rec) as rec, if(fl.pag is NULL, f.pag, fl.pag) as pag, @alivio:=(select alivio from tot_posicao where codigo=(select max(codigo) from tot_posicao where convenio='49093' and lojabb=f.lojabb and posicao=f.datacx)) as alivio,  fl.tot_gtv as Ngtv, @SldCx:=(select sum(saldo_fn) from financeiro where lojabb=f.lojabb and datacx=f.datacx and local <> f.lojabb ) as SldCx, "
instrucao_sqlACi = instrucao_sqlACi &" fl.tot_difcxmenor, fl.tot_difcxmaior, fl.saldo_comp "
instrucao_sqlACi = instrucao_sqlACi &"  FROM cad_lojabb c  LEFT  JOIN financeiro_lj fl on c.lojabb=fl.lojabb and fl.datacx='"&ConvData(request.Form("data"))&"'  LEFT  JOIN financeiro f on c.lojabb=f.lojabb and f.datacx='"&ConvData(request.Form("data"))&"' and f.local =c.lojabb where "&where&" order by "&request.Form("orderby")&""
set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF
if dadosACi("Ndata") <> "" then
	SldAntCon = ((ccur(dadosACi("saldo_in"))*1)+(ccur(dadosACi("rec"))*1)+(ccur(dadosACi("pag"))*1))-(ccur(dadosACi("alivio"))*1)
	Sldreta = ((ccur(dadosACi("saldo_comp"))*1)+(ccur(dadosACi("saldo_cofreadm"))*1)+(ccur(dadosACi("saldo_bocadelobo"))*1))
else
	SldAntCon = ""
	Sldreta = ""
end if
if dadosACi("Fdata") <> "" then
	Sldreta = ((ccur(dadosACi("saldo_comp"))*1)+(ccur(dadosACi("saldo_cofreadm"))*1)+(ccur(dadosACi("saldo_bocadelobo"))*1))
else
	Sldreta = ""
end if


Tsaldo_in = Tsaldo_in + SumFormCcur(dadosACi("saldo_in"))
Trec = Trec + SumFormCcur(dadosACi("rec"))
Tpag = Tpag + SumFormCcur(dadosACi("pag"))
Talivio = Talivio + SumFormCcur(dadosACi("alivio"))
TSldAntCon = TSldAntCon + SumFormCcur(SldAntCon)
TNgtv = TNgtv + SumFormCcur(dadosACi("Ngtv"))
TSldCx = TSldCx + SumFormCcur(dadosACi("SldCx"))
Ttot_difcxmenor = Ttot_difcxmenor + SumFormCcur(dadosACi("tot_difcxmenor"))
Ttot_difcxmaior = Ttot_difcxmaior + SumFormCcur(dadosACi("tot_difcxmaior"))
Tsaldo_comp = Tsaldo_comp + SumFormCcur(dadosACi("saldo_comp"))
Tsaldo_cofreadm = Tsaldo_cofreadm + SumFormCcur(dadosACi("saldo_cofreadm"))
Tsaldo_bocadelobo = Tsaldo_bocadelobo + SumFormCcur(dadosACi("saldo_bocadelobo"))
TSldreta = TSldreta + SumFormCcur(Sldreta)
%>
          <tr>
            <td><%=dadosACi("Nlojabb")%></td>	
            <td NOWRAP><%=dadosACi("nome")%></td>	
            <td><%=dadosACi("Ndata")%></td>	
            <td><%=dadosACi("Fdata")%></td>	
            <td><%=dadosACi("Cdata")%></td>	
            <td NOWRAP>
            <% if dadosACi("std_cx") <> "" then %>
                <% if dadosACi("std_cx") = "Em Conferência" then%><a href="rel_fech_loja.asp?cd=<%=dadosACi("CdFc")%>" target="new"><div style="color:#8000FF">Em Conferência</div></a><% end if%>
                <% if dadosACi("std_cx") = "Anexar" then%><a href="rel_fech_loja.asp?cd=<%=dadosACi("CdFc")%>" target="new"><div style="color:#F60">Em Fechamento </div></a><% end if%>
                <% if dadosACi("std_cx") = "Fechado" then%><a href="rel_fech_loja.asp?cd=<%=dadosACi("CdFc")%>" target="new"><div style="color:#00F">Fechado </div></a><% end if%>
                <% if dadosACi("std_cx") = "Conferido" then%><a href="rel_fech_loja.asp?cd=<%=dadosACi("CdFc")%>" target="new"><div style="color:#090">Conferido </div></a><% end if%>
            <%else%>
				<% if dadosACi("Ndata") <> "" then %>
                    <div style="color:#F00">Aberto </div>
	            <%else%>
    	            <div style="color:#F00">Sem Operação</div>
                <%end if%>    
            <%end if%>    
            </td>	
            <td align="right" NOWRAP><%=formatcurrency(dadosACi("saldo_in"),2)%></td>	
            <td align="right" NOWRAP><%=formatcurrency(dadosACi("rec"),2)%></td>	
            <td align="right" NOWRAP><%=formatcurrency(dadosACi("pag"),2)%></td>	
            <td align="right" NOWRAP><%=formatcurrency(dadosACi("alivio"),2)%></td>	
            <td align="right" NOWRAP><%=FormCcur(SldAntCon)%></td>	
            <td align="right" NOWRAP><%=FormCcur(dadosACi("Ngtv"))%></td>	
            <td align="right" NOWRAP><%=FormCcur(dadosACi("SldCx"))%></td>	
            <td align="right" NOWRAP><%=FormCcur(dadosACi("tot_difcxmenor"))%></td>
            <td align="right" NOWRAP><%=FormCcur(dadosACi("tot_difcxmaior"))%></td>	
            <td align="right" NOWRAP><%=FormCcur(dadosACi("saldo_comp"))%></td>	
            <td align="right" NOWRAP><%=FormCcur(dadosACi("saldo_cofreadm"))%></td>	
            <td align="right" NOWRAP><%=FormCcur(dadosACi("saldo_bocadelobo"))%></td>	
            <td align="right" NOWRAP><%=FormCcur(Sldreta)%></td>	
        </tr>
<%
dadosACi.MoveNext
Wend
end if
%>       	</tbody>
        <tr>
            <td colspan="6" align="right">Total Lojas:</td>
            <td align="right" NOWRAP><%=FormCcur(Tsaldo_in)%></td>
            <td align="right" NOWRAP><%=FormCcur(Trec)%></td>
            <td align="right" NOWRAP><%=FormCcur(Tpag)%></td>
            <td align="right" NOWRAP><%=FormCcur(Talivio)%></td>
            <td align="right" NOWRAP><%=FormCcur(TSldAntCon)%></td>
            <td align="right" NOWRAP><%=FormCcur(TNgtv)%></td>
            <td align="right" NOWRAP><%=FormCcur(TSldCx)%></td>
            <td align="right" NOWRAP><%=FormCcur(Ttot_difcxmenor)%></td>
            <td align="right" NOWRAP><%=FormCcur(Ttot_difcxmaior)%></td>
            <td align="right" NOWRAP><%=FormCcur(Tsaldo_comp)%></td>
            <td align="right" NOWRAP><%=FormCcur(Tsaldo_cofreadm)%></td>
            <td align="right" NOWRAP><%=FormCcur(Tsaldo_bocadelobo)%></td>
            <td align="right" NOWRAP><%=FormCcur(TSldreta)%></td>
        </tr>
    </table>
    </div>
  
</div>
</div>
<%
'response.Write(instrucao_sqlACi)
%>  
<%end if%>