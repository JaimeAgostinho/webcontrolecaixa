﻿<!--#include file="global.asp"-->
<%
if request("op") = "ex" then
	MysqlInsert("DELETE FROM financeiro_lj where codigo='"&request.QueryString("cd")&"' and lojabb='"&request.QueryString("loja")&"' ")
	MysqlInsert("UPDATE financeiro SET std = 'A' where local='"&request.QueryString("loja")&"' and lojabb='"&request.QueryString("loja")&"' and datacx='"&request.QueryString("datacx")&"'")
	response.Redirect("rel_fech_loja.asp?cd="&request.QueryString("cd")&"&op=gr&exc=sim")
end if

if request("op") = "ca" then
	MysqlInsert("UPDATE financeiro_lj SET std_cx = 'Anexar', obs_conf='' where codigo='"&request.QueryString("cd")&"' and lojabb='"&request.QueryString("loja")&"'")
	MysqlInsert("UPDATE financeiro SET std = 'A' where local='"&request.QueryString("loja")&"' and lojabb='"&request.QueryString("loja")&"' and datacx='"&request.QueryString("datacx")&"'")
	response.Redirect("rel_fech_loja.asp?cd="&request.QueryString("cd")&"&op=gr&exc=sim")
end if

if request("op") = "gr" and request("log") = "s" then

    if request.form("statusConf") = "Conferido" then
        MysqlInsert("UPDATE financeiro_lj SET std_cx = 'Conferido', obs_conf='"&request("operador")&" >> "&request("obs")&"' where codigo='"&request.QueryString("cd")&"' and lojabb='"&request.QueryString("loja")&"'")
        MysqlInsert("UPDATE financeiro SET std = 'C' where local='"&request.QueryString("loja")&"' and lojabb='"&request.QueryString("loja")&"' and datacx='"&request.QueryString("datacx")&"'")
    else
        MysqlInsert("UPDATE financeiro_lj SET std_cx = 'Inconclusivo', obs_conf='"&request("operador")&" >> "&request("obs")&"' where codigo='"&request.QueryString("cd")&"' and lojabb='"&request.QueryString("loja")&"'")
        MysqlInsert("UPDATE financeiro SET std = 'I' where local='"&request.QueryString("loja")&"' and lojabb='"&request.QueryString("loja")&"' and datacx='"&request.QueryString("datacx")&"'")
    end if
    
    response.Redirect("rel_fech_loja.asp?cd="&request.QueryString("cd")&"")
end if



%>

<html lang="pt-br">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Sys PDV">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Jaime Agostinho">

    <title>Fechamento de Caixa Loja</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <style type="text/css">
   body{margin-top:10px;}
   .glyphicon { margin-right:6px; }
   .card-block a{text-decoration: none;}
   .collapse .card-block.disabled a{color:#c5c5c5;}
   .collapse .card-block.disabled, .collapse .card-block.disabled span{color:#c5c5c5;}
    </style>
</head>
<body>
<%
if request("exc") <> "sim" then
Set objconnAC = Server.CreateObject("ADODB.Connection")
objconnAC.open = StringConexaoBDCons()
instrucao_sqlAC = "SELECT * FROM financeiro_lj where codigo='"&request.QueryString("cd")&"'"
set dadosAC = objconnAC.execute(instrucao_sqlAC)
if dadosAC.EOF then
else
dadosAC.MoveFirst
While Not dadosAC.EOF
%>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-9 table-bordered">
        <div class="row text-xs-center"><b>FECHAMENTO DE CAIXA LOJA</b></div>

        <div class="row">
            <div class="col-lg-8 table-bordered">Usuario: <%=dadosAC("gerente")%></div>
            <div class="col-lg-4 table-bordered">Data: <%=dadosAC("datacx")%></div>
        </div>
        <div class="row">&nbsp;</div>
        <div class="row">
        	<div class="text-xs-center"><b>Indentificação de Dependência</b></div>
        </div>
        <div class="row">
            <div class="col-lg-4 table-bordered">Loja: <%=dadosAC("lojabb")%> - <b><%=NomeLojaBB(dadosAC("lojabb"), "nome")%></b></div>
            <div class="col-lg-4 table-bordered">GEREG: <%=NomeLojaBB(dadosAC("lojabb"), "regional")%></div>
        </div>
        <div class="row">
            <div class="col-lg-8 table-bordered">Gerente: <%=PesqResp("SELECT nome as Resp FROM usuario_operador where lojabb='"&dadosAC("lojabb")&"' and funcao='23' limit 0,1")%></div>
        </div>
        <div class="row">
            <div class="col-lg-3 table-bordered">Saldo&nbsp;Remanescente: </div>
            <div class="col-lg-3 table-bordered">Recebimento&nbsp;Dia: </div>
            <div class="col-lg-2 table-bordered">Pagamento&nbsp;Dia:</div>
            <div class="col-lg-2 table-bordered">Alivio&nbsp;Dia:</div>
            <div class="col-lg-2 table-bordered">Saldo&nbsp;Parcial:</div>
        </div>
        <div class="row">
            <div class="col-lg-3 table-bordered"><%=formatcurrency(dadosAC("saldo_in"),2)%></div>
            <div class="col-lg-3 table-bordered"><%=formatcurrency(dadosAC("rec"),2)%></div>
            <div class="col-lg-2 table-bordered"><%=formatcurrency(dadosAC("pag"),2)%></div>
            <div class="col-lg-2 table-bordered"><%=formatcurrency(dadosAC("alivio"),2)%></div>
            <div class="col-lg-2 table-bordered"><%=formatcurrency(dadosAC("saldo_parcial"),2)%></div>
        </div>
        <div class="row">&nbsp;</div>
        <div class="row">
        	<div class="col-lg-6 text-xs-center table-bordered"><b>GTV / Alivio Pendente</b></div>
        	<div class="col-lg-6 text-xs-center table-bordered"><b>Anexar Documentos</b></div>
        </div>
        <div class="row">
        	<div class="col-lg-6 table-bordered">
		        <div class="row">
		        	<div class="col-lg-4 table-bordered">Nrº GTV:</div>
		        	<div class="col-lg-4 table-bordered">Horário:</div>
		        	<div class="col-lg-4 table-bordered">Valor:</div>
                </div>
<%
Set objconnACi = Server.CreateObject("ADODB.Connection")
objconnACi.open = StringConexaoBDCons()
instrucao_sqlACi = "SELECT * FROM financeiro_mov f where chavej='"&dadosAC("lojabb")&"' and datacx='"&ConvData(dadosAC("datacx"))&"'"
set dadosACi = objconnACi.execute(instrucao_sqlACi)
if dadosACi.EOF then
else
dadosACi.MoveFirst
While Not dadosACi.EOF
%>
          <div class="row">
           <div class="col-lg-4 table-bordered"><%=dadosACi("n_doc")%></div>
           <div class="col-lg-4 table-bordered"><%=mid(dadosACi("data"),1,5)%>&nbsp;<%=mid(dadosACi("data"),12,5)%></div>
           <div class="col-lg-4 table-bordered"><%=formatcurrency(dadosACi("total"),2)%></div>
                </div>
<%
dadosACi.MoveNext
Wend
end if
%>		        <div class="row">
		        	<div class="col-lg-8 table-bordered"></div>
		        	<div class="col-lg-4 table-bordered"><%=formatcurrency(dadosAC("tot_gtv"),2)%></div>
                </div>
                <div class="row"><hr></div>
                <div class="row">
              		<div class="col-lg-12 text-xs-center table-bordered"><b> Valor de Pernoite</b></div>
                </div>
		        <div class="row">
		        	<div class="col-lg-4 table-bordered">CompuSafe:</div>
		        	<div class="col-lg-4 table-bordered">Boca de Lobo:</div>
		        	<div class="col-lg-4 table-bordered"><a href="#" title="Soma= [{Saldo Parcial} - {GTV} - {CompuSafe} - {Boca de Lobo} - {Dif. Caixa Menor}] + {Dif. Caixa Maior}">Administrativo:</a></div>
                </div>
		        <div class="row">
		        	<div class="col-lg-4 table-bordered"><%=formatcurrency(dadosAC("saldo_comp"),2)%></div>
		        	<div class="col-lg-4 table-bordered"><%=formatcurrency(dadosAC("saldo_bocadelobo"),2)%></div>
		        	<div class="col-lg-4 table-bordered"><%=formatcurrency(dadosAC("saldo_cofreadm"),2)%></div>
                </div>
                <div class="row"><hr></div>
		        <div class="row">
		        	<div class="col-lg-4 table-bordered">Dif.&nbsp;Caixa&nbsp;Menor:</div>
		        	<div class="col-lg-4 table-bordered">Dif.&nbsp;Caixa&nbsp;Maior:</div>
		        	<div class="col-lg-4 table-bordered">S.A.O:</div>
                </div>
		        <div class="row">
		        	<div class="col-lg-4 table-bordered"><%=formatcurrency(dadosAC("tot_difcxmenor"),2)%></div>
		        	<div class="col-lg-4 table-bordered"><%=formatcurrency(dadosAC("tot_difcxmaior"),2)%></div>
		        	<div class="col-lg-4 table-bordered">_</div>
                </div>
                <div class="row"><hr></div>
		        <div class="row">
		        	<div class="col-lg-12 table-bordered">Valor Total Retido (Em poder do Gerente / Loja):</div>
                </div>
		        <div class="row">
		        	<div class="col-lg-12 table-bordered"><%=formatcurrency(dadosAC("saldo_loja"),2)%></div>
                </div>
                <div class="row"><hr></div>
		        <div class="row">
		        	<div class="col-lg-12 table-bordered"><b>Observação de Fechamento:</b></div>
                </div>
		        <div class="row">
		        	<div class="col-lg-12 table-bordered"><%=dadosAC("obs")%></div>
                </div>
                <div class="row"><hr></div>
                <% if request("op") <> "gr" and dadosAC("std_cx") <> "Conferido" then%>
                <%
				if dadosAC("std_cx") = "Fechado" then
					MysqlInsert("UPDATE  financeiro_lj SET std_cx = 'Em Conferência'  where std_cx = 'Fechado' and codigo='"&request.QueryString("cd")&"' and lojabb='"&dadosAC("lojabb")&"' ")
				end if
				%>
                <% if  dadosAC("std_cx") <> "Inconclusivo" then%>
                <div class="row">
                    <div class="col-lg-3" table-bordered align="center"><a class="btn btn-danger" href="rel_fech_loja.asp?cd=<%=request.QueryString("cd")%>&datacx=<%=ConvData(dadosAC("datacx"))%>&loja=<%=dadosAC("lojabb")%>&op=ex" role="button"> Excluir</a></div>
                    <div class="col-lg-3" table-bordered align="center"><a class="btn btn-warning" href="rel_fech_loja.asp?cd=<%=request.QueryString("cd")%>&datacx=<%=ConvData(dadosAC("datacx"))%>&loja=<%=dadosAC("lojabb")%>&op=ca" role="button"> Cancelar</a></div>
                    <div class="col-lg-6" table-bordered align="center"><a class="btn btn-success" href="rel_fech_loja.asp?cd=<%=request.QueryString("cd")%>&datacx=<%=ConvData(dadosAC("datacx"))%>&op=gr" role="button"> Conferir Caixa </a></div>
               </div>
               <%else%>
                <div class="row">
                     <div class="col-lg-12"><b>Conferência GECON: INCONCLUSIVO </b></div>
                     <div class="col-lg-12"><div style="color:red"><%=dadosAC("obs_conf")%></div></div>
                </div>
               <% end if %>
	            <%
				else
				if dadosAC("std_cx") <> "Conferido" and dadosAC("std_cx") <> "Inconclusivo" then
				%>
				<form id="acesso" name="acesso" method="post" action="rel_fech_loja.asp?cd=<%=request.QueryString("cd")%>&datacx=<%=ConvData(dadosAC("datacx"))%>&loja=<%=dadosAC("lojabb")%>&op=gr&log=s">
                <div class="row">
                     <div class="col-lg-12  input-group-sm"><input type="text" class="form-control" value="<%=response.Write(now())%> - <%=request.cookies("sismy")("nome_usu")%>" readonly name="operador" id="operador"  aria-describedby="sizing-addon2"></div>
                     <div class="col-lg-12  input-group-sm">
                        <select class="form-control input-group-sm" id="statusConf" name="statusConf">
                            <option value="Conferido" >Conferido</option>
                            <option value="Inconclusivo" >Inconclusivo</option>
                        </select>                      
                     </div>
                     <div class="col-lg-12  input-group-sm"><textarea class="form-control" rows="5" id="obs"  name="obs"  placeholder="Informe com riqueza de detalhes a descrição da conferência!"></textarea></div>
					 <div class="col-lg-12  btn-group-sm"><input class="btn btn-success btn-lg btn-block" type="submit" name="bt" id="bt" value="Gravar Conferência de Caixa!" /></div>                
                </div>
                </form>
                <%else%>
                <div class="row">
                     <div class="col-lg-12"><b>Conferência GECON:</b></div>
                     <div class="col-lg-12"><div style="color:#090"><%=dadosAC("obs_conf")%></div></div>
                </div>
                <%
				end if
				end if
				%>
            </div>
        	<div class="col-lg-6 table-bordered">
		        <div class="row"><a href="../<%=Response.Write("Upload\L-"&dadosAC("lojabb")&"-"&ConvData(dadosAC("datacx"))&"-"&dadosAC("codigo")&"-GTV.pdf")%>" target="_blank"><img src="../imagem/pdf.png" width="14" height="14"></a> GTV's - Guia de Trafego de Valores</div>
		        <div class="row">&nbsp;&nbsp;&nbsp;&nbsp;Termo de Conferência de Alívio de Numerário</div>
		        <div class="row"><a href="../<%=Response.Write("Upload\L-"&dadosAC("lojabb")&"-"&ConvData(dadosAC("datacx"))&"-"&dadosAC("codigo")&"-COMP.pdf")%>" target="_blank"><img src="../imagem/pdf.png" width="14" height="14"> </a> Comprovante do valor inserido Cofre CompuSafe</div>
		        <div class="row">
							<%
                            Set ScriptObject = Server.CreateObject("Scripting.FileSystemObject")
                            endereco =  Server.MapPath(".")
                            endereco = replace(endereco, "\PDV", "\Upload\L-"&dadosAC("lojabb")&"-"&ConvData(dadosAC("datacx"))&"-"&dadosAC("codigo")&"-SAO.pdf")
                            if ScriptObject.FileExists(endereco) then
							'stdbt = stdbt&"0"
                            %>
                            <a href="../<%=Response.Write("Upload\L-"&dadosAC("lojabb")&"-"&ConvData(dadosAC("datacx"))&"-"&dadosAC("codigo")&"-SAO.pdf")%>" target="new"><img src="../imagem/pdf.png" width="14" height="14"> </a> 
							<%else%>
                            	&nbsp;&nbsp;&nbsp;&nbsp;
                            <%end if%>

                
                Formulário de Controle de Processamento de S.A.O</div>
		        <div class="row">&nbsp;&nbsp;&nbsp;&nbsp;Formulário de Apuração de Fatos</div>
		        <div class="row"><a href="../<%=Response.Write("Upload\L-"&dadosAC("lojabb")&"-"&ConvData(dadosAC("datacx"))&"-"&dadosAC("codigo")&"-LOJA.pdf")%>" target="_blank"><img src="../imagem/pdf.png" width="14" height="14"> </a> Formulário de Apuração Pernoite</div>
		        <div class="row">&nbsp;&nbsp;&nbsp;&nbsp;Outros</div>
                <div class="row"><hr></div>
                <div class="row">
					<div class="col-lg-12 text-xs-center table-bordered"><b>Descrição das Ocorrências (Apuração de Fatos)</b></div>
				</div>
		        <div class="row">
		        	<div class="col-lg-2 table-bordered">Hora:</div>
		        	<div class="col-lg-2 table-bordered">Tipo:</div>
		        	<div class="col-lg-2 table-bordered">ChaveJ:</div>
		        	<div class="col-lg-4 table-bordered">Motivo:</div>
		        	<div class="col-lg-2 table-bordered" align="right">Valor:</div>
                </div>
				<%
				sumVlrT = 0
                Set objconnACi = Server.CreateObject("ADODB.Connection")
                objconnACi.open = StringConexaoBDCons()
                instrucao_sqlACi = "SELECT c.*, if(tipo = 'Devedor', 'A Menor', 'Credor') as std FROM dif_caixa c where datacx='"&ConvData(dadosAC("datacx"))&"' and lojabb='"&dadosAC("lojabb")&"'"
                set dadosACi = objconnACi.execute(instrucao_sqlACi)
                if dadosACi.EOF then
                else
                dadosACi.MoveFirst
                While Not dadosACi.EOF
                %>
		        <div class="row">
		        	<div class="col-lg-2 table-bordered">
                    <% if dadosACi("std") = "Credor" then %>
						<a href="../<%=Response.Write("Upload\difCre-"&dadosACi("codigo")&".pdf")%>" target="_blank"><img src="../imagem/pdf.png" width="14" height="14"></a>
                    <% else %>
						<a href="../<%=Response.Write("Upload\difdev-"&dadosACi("codigo")&".pdf")%>" target="_blank"><img src="../imagem/pdf.png" width="14" height="14"></a>
                    <% end if %>
					<%=mid(dadosACi("data"),12,5)%>
                    </div>
		        	<div class="col-lg-2 table-bordered"><%=replace(dadosACi("std")," ", "&nbsp;")%></div>
		        	<div class="col-lg-2 table-bordered"><%=dadosACi("local")%></div>
		        	<div class="col-lg-4 table-bordered"><%=replace(dadosACi("motivo")," ", "&nbsp;")%></div>
		        	<div class="col-lg-2 table-bordered" align="right"><%=formatcurrency(dadosACi("valor"),2)%></div>
                </div>
				<%
				if dadosACi("std") = "Credor" then
					sumVlrT = sumVlrT + formatcurrency(dadosACi("valor"),2)
				else
					sumVlrT = sumVlrT + formatcurrency("-"&dadosACi("valor"),2)
				end if
                dadosACi.MoveNext
                Wend
                end if
                %>
		        <div class="row">
		        	<div class="col-lg-10 table-bordered" align="right">Total:</div>
		        	<div class="col-lg-2 table-bordered" align="right"><%=formatcurrency(sumVlrT,2)%></div>
                </div>
            </div>
            
        </div>
        
        </div>

        <div class="col-lg-3 table-bordered">
	        <div class="row text-xs-center"><b>Evolução de Caixa Menor/Credora</b></div>
            <div class="row">
	            <div class="col-lg-4 table-bordered">Data:</div>
	            <div class="col-lg-4 table-bordered">R$&nbsp;A&nbsp;Menor:</div>
	            <div class="col-lg-4 table-bordered">R$&nbsp;Credora:</div>
            </div>
<%
UltimoDia = DAY(DateAdd("d",-1,DateAdd("m",1,"1" & "/" & mid(dadosAC("datacx"),4,2) & "/" & mid(dadosAC("datacx"),7,4))))
	 x= 0
	 while x < UltimoDia
LoopData = dateadd("d",x,("01/"&  mid(dadosAC("datacx"),4,2) &"/"&mid(dadosAC("datacx"),7,4)))
Week = weekday(LoopData)
x=x+1
if Week <> "1" and Week <> "7" then
dataNv = LoopData
dataNv = ConvData(dataNv)
'response.Write(dataNv)
%>
            <div class="row">
	            <div class="col-lg-4 table-bordered"><%=response.Write(LoopData)%></div>
	            <div class="col-lg-4 table-bordered" align="right">
				<%
				vlr = PesqResp("SELECT tot_difcxmenor as Resp FROM financeiro_lj where lojabb = '"&dadosAC("lojabb")&"' and datacx='"&dataNv&"'")
				if vlr <> "" then
				response.Write(replace(formatcurrency(vlr,2), " ","&nbsp;"))
				else
				response.Write("")
				end if				
				%></div>
	            <div class="col-lg-4 table-bordered" align="right">
                <%
				vlr = PesqResp("SELECT tot_difcxmaior as Resp FROM financeiro_lj where lojabb = '"&dadosAC("lojabb")&"' and datacx='"&dataNv&"'")
				if vlr <> "" then
				response.Write(replace(formatcurrency(vlr,2), " ","&nbsp;"))
				else
				response.Write("")
				end if				
				%></div>
            </div>        
<% 
end if
	 wend
%>            
         </div>
      </div>
    </div>
<%
dadosAC.MoveNext
Wend
end if
else
%>

    <div class="container">
      <div class="row" align="center">
        <div class="col-sm-4"></br></br></br>
        <div style="color:#F00"><h5>Caixa Alterado com Sucesso!</h5></div>
		</div>
      </div>
    </div>

<%end if%>
</br></br>
</br></br>
</body>
</html>
