<% if request("nt") = "s" then%>
<html lang="pt-br">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Sys PDV">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Jaime Agostinho">

    <title>Fechamento de Caixa Loja</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <style type="text/css">
   body{margin-top:10px;}
   .glyphicon { margin-right:6px; }
   .card-block a{text-decoration: none;}
   .collapse .card-block.disabled a{color:#c5c5c5;}
   .collapse .card-block.disabled, .collapse .card-block.disabled span{color:#c5c5c5;}
    </style>
</head>
<body>
<% end if %>
<% if request("nt") <> "s" then%>
<div class="row">
    <div class="col-sm-12">
		<%=CabecalhoUrl("Relatório Diário - Pontos Próprios e Substabelecidos")%> 
    </div>
</div>
<form id="acesso" name="acesso" method="post" action="adm.asp?tc=<%=response.Write(request("tc"))%>&url=rel_SituacaoRedeEst&Orb=var&log=s">
<div class="row">

    <div class="col-sm-2">
	    <input class="form-control input-sm" type="text" name="data" id="data" value="<% if request("log") = "s" then%><%=request.form("data")%><%else%><%=response.Write(mid(now(),1,10))%><%end if%>"/>
    </div>
    <div class="col-sm-1 btn-group-sm">
		<input class="btn btn-primary btn-lg btn-block" type="submit" name="bt" id="bt" value="Pesquisar" />
    </div>

</div>
</form>
<br>
<%end if%>
<% if request("log") = "s" then%>
<br><br>
<div class="row">
        <div class="col-lg-12">
        <div class="row text-xs-center"><b><a href="/new_SituacaoRedeEst.asp?tc=s&nt=s&log=s&dt=<% = request("data")%>" target="new">GERAR RALATÓRIO</a></b></div>
		</div>
    </div>
</div>
<%end if%>