CREATE DATABASE  IF NOT EXISTS `bdsistema` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bdsistema`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: controlecaixa.c0m0rtpl64jd.us-west-2.rds.amazonaws.com    Database: bdsistema
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acbfull`
--

DROP TABLE IF EXISTS `acbfull`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acbfull` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `convenio` int(9) NOT NULL DEFAULT '0',
  `lojabb` int(4) NOT NULL DEFAULT '0',
  `pdvbb` int(4) NOT NULL DEFAULT '0',
  `produto` varchar(3) NOT NULL DEFAULT '',
  `dt_trans` date NOT NULL DEFAULT '0000-00-00',
  `dt_movim` date NOT NULL DEFAULT '0000-00-00',
  `dh_operacao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nsu` int(4) NOT NULL DEFAULT '0',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cdbarra` varchar(44) NOT NULL DEFAULT '',
  `nraut` varchar(20) NOT NULL DEFAULT '',
  `chavej` varchar(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `key_coban` (`dt_trans`,`convenio`,`lojabb`,`nsu`,`valor`),
  KEY `Key_ind` (`convenio`,`dt_trans`,`dt_movim`),
  KEY `Index_4` (`convenio`,`dt_trans`)
) ENGINE=InnoDB AUTO_INCREMENT=7860140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `acbfull_tmp`
--

DROP TABLE IF EXISTS `acbfull_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acbfull_tmp` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `convenio` int(9) NOT NULL DEFAULT '0',
  `lojabb` int(4) NOT NULL DEFAULT '0',
  `pdvbb` int(4) NOT NULL DEFAULT '0',
  `produto` varchar(3) NOT NULL DEFAULT '',
  `dt_trans` date NOT NULL DEFAULT '0000-00-00',
  `dt_movim` date NOT NULL DEFAULT '0000-00-00',
  `dh_operacao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nsu` int(4) NOT NULL DEFAULT '0',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cdbarra` varchar(44) NOT NULL DEFAULT '',
  `nraut` varchar(20) NOT NULL DEFAULT '',
  `chavej` varchar(8) NOT NULL DEFAULT '',
  `tmp` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `key_coban` (`dt_trans`,`convenio`,`lojabb`,`nsu`,`valor`),
  KEY `Key_ind` (`convenio`,`dt_trans`,`dt_movim`),
  KEY `Index_4` (`convenio`,`dt_trans`),
  KEY `Index_5` (`lojabb`)
) ENGINE=InnoDB AUTO_INCREMENT=11137061 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_MAC`
--

DROP TABLE IF EXISTS `cad_MAC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_MAC` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dths` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` varchar(12) NOT NULL,
  `descricao` varchar(45) NOT NULL DEFAULT '',
  `autorizado` enum('solicitado','autorizado','negado') NOT NULL DEFAULT 'solicitado',
  `convenio` varchar(5) DEFAULT NULL,
  `lojaACB` varchar(4) DEFAULT NULL,
  `agenciaACB` varchar(4) DEFAULT NULL,
  `terminalACB` varchar(8) DEFAULT NULL,
  `operador` varchar(45) DEFAULT NULL,
  `chavej` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_cadastro`
--

DROP TABLE IF EXISTS `cad_cadastro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_cadastro` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `menu` int(11) DEFAULT NULL COMMENT 'Menu;;;sql:SELECT s.codigo_s as A, concat(m.descricao_m, '' > '', s.descricao_s, '' >'') as B FROM m_submenu s , m_menu m where s.codigo_m=m.codigo_m  group by codigo_s order by B;',
  `descricao` varchar(80) NOT NULL COMMENT 'Descriçao; Titulo do Cadastro;',
  `tabela` varchar(20) DEFAULT NULL COMMENT 'Tabela;Tabela de Referencia;',
  `campos` varchar(255) DEFAULT NULL COMMENT 'Campos;;;',
  `log` varchar(30) DEFAULT 'arquivos/' COMMENT 'Log; *Onde o Log de Registro Sera Gerado;',
  `status` enum('Ativo','Inativo') DEFAULT 'Ativo' COMMENT 'Status;',
  `config` longtext COMMENT 'Configuraçoes;',
  `campo_pesq` varchar(15) DEFAULT NULL COMMENT 'Campo de Pesquisa;Informe o campo chave;',
  `Link` varchar(80) DEFAULT NULL COMMENT 'Link;;;',
  `pesquisa` longtext COMMENT 'Sql;Sql do Relatorio;',
  `visualizar` enum('','Sim','Nao') DEFAULT NULL COMMENT 'Visualizar;',
  `alterar` enum('','Sim','Nao') DEFAULT NULL COMMENT 'Alterar;',
  `excluir` enum('','Sim','Nao') DEFAULT NULL COMMENT 'Excluir;',
  `referenciacad` varchar(45) DEFAULT NULL COMMENT 'Tela de Cadastro;;;sql:SELECT codigo as A, descricao as B FROM cad_cadastro where tabela <> "";',
  `cadastrar` enum('s','n') NOT NULL DEFAULT 'n' COMMENT 'Cadastrar;',
  `relatorio` enum('s','n') NOT NULL DEFAULT 'n' COMMENT 'Relatorio;',
  `menudinamico` longtext,
  `rel_dinamico` enum('s','n') NOT NULL DEFAULT 'n',
  `sql_dinamico` longtext,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=1075 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_coban`
--

DROP TABLE IF EXISTS `cad_coban`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_coban` (
  `usuario` int(5) DEFAULT NULL,
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `consultor` varchar(5) DEFAULT NULL,
  `codigo_coban` int(6) DEFAULT NULL,
  `mci` int(9) DEFAULT NULL,
  `convenio` varchar(11) DEFAULT NULL,
  `conta_alivio` varchar(20) DEFAULT NULL,
  `agbb` varchar(9) DEFAULT NULL,
  `ccbb` varchar(9) DEFAULT NULL,
  `LojaBB` varchar(4) DEFAULT NULL,
  `data` varchar(10) DEFAULT NULL,
  `time` varchar(10) DEFAULT NULL,
  `razao_social` varchar(99) DEFAULT NULL,
  `cnpj` varchar(25) DEFAULT NULL,
  `nome_fantasia` varchar(60) DEFAULT NULL,
  `ramo_atividade` varchar(60) DEFAULT NULL,
  `data_fundacao` varchar(12) DEFAULT NULL,
  `estab_proprio` char(3) DEFAULT NULL,
  `endereco` varchar(60) DEFAULT NULL,
  `numero` varchar(5) DEFAULT NULL,
  `complemento` varchar(80) DEFAULT NULL,
  `bairro` varchar(60) DEFAULT NULL,
  `cidade` varchar(60) DEFAULT NULL,
  `estado` varchar(60) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `tempo_local` varchar(12) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `site` varchar(60) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `telefone_ramal` varchar(10) DEFAULT NULL,
  `telefone2` varchar(20) DEFAULT NULL,
  `telefone_ramal2` varchar(10) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `outros` varchar(20) DEFAULT NULL,
  `contato_nome` varchar(60) DEFAULT NULL,
  `contato_cargo` varchar(30) DEFAULT NULL,
  `contato_telefone` varchar(20) DEFAULT NULL,
  `contato_telefone_ramal` varchar(10) DEFAULT NULL,
  `contato_celular` varchar(20) DEFAULT NULL,
  `contato_email` varchar(30) DEFAULT NULL,
  `conta_banco` varchar(50) DEFAULT NULL,
  `conta_agencia` varchar(10) DEFAULT NULL,
  `conta_conta` varchar(20) DEFAULT NULL,
  `conta_favorecido` varchar(99) DEFAULT NULL,
  `conta_cpfcnpj` varchar(20) DEFAULT NULL,
  `conta_tipo` char(3) DEFAULT NULL,
  `repasse` varchar(4) DEFAULT NULL,
  `repasse_hora` varchar(6) DEFAULT NULL,
  `observacoes` longtext,
  `status` varchar(19) DEFAULT NULL,
  `cod_tabpreco` varchar(4) DEFAULT NULL,
  `limite_recebimento` varchar(9) DEFAULT NULL,
  `limite_gaveta` varchar(9) DEFAULT NULL,
  `limite_recebimentocc` varchar(10) DEFAULT NULL,
  `limite_gavetacc` varchar(10) DEFAULT NULL,
  `desc_vlrfixo` decimal(10,2) DEFAULT NULL,
  `desc_seg` decimal(10,2) DEFAULT NULL,
  `inf_minpag` decimal(5,4) DEFAULT NULL,
  `cob_juros` enum('sim_sld','sim_com','nao') DEFAULT NULL,
  `assegurado` decimal(10,2) DEFAULT NULL,
  `assegurado_por` decimal(10,2) DEFAULT NULL,
  `bon_vlr` decimal(10,2) DEFAULT NULL,
  `bon_perc` decimal(10,2) DEFAULT NULL,
  `carrofort_vlr` decimal(10,2) DEFAULT NULL,
  `carrofort_perc` decimal(10,2) DEFAULT NULL,
  `senha_ura` varchar(12) DEFAULT NULL,
  `codigo_cedente` varchar(12) DEFAULT NULL,
  `conta_agencia_dv` varchar(1) DEFAULT '0',
  `conta_conta_dv` varchar(2) DEFAULT '0',
  `conta_conta_op` varchar(3) DEFAULT NULL,
  `NSUpdv` int(11) DEFAULT '0',
  `chavej` varchar(30) DEFAULT NULL,
  `login` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `convenio_gestor` varchar(5) DEFAULT NULL,
  `regional` varchar(25) DEFAULT NULL,
  `rede` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `Index_1` (`codigo_coban`),
  KEY `Index_3` (`mci`,`LojaBB`)
) ENGINE=InnoDB AUTO_INCREMENT=1550 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_coban_term`
--

DROP TABLE IF EXISTS `cad_coban_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_coban_term` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `dths` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `codigo_coban` int(6) DEFAULT NULL,
  `codigo_terminal` int(6) DEFAULT NULL,
  `Descricao` varchar(30) DEFAULT ' ',
  `indentificador` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Index_2` (`codigo_terminal`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_funcao`
--

DROP TABLE IF EXISTS `cad_funcao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_funcao` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_gestor`
--

DROP TABLE IF EXISTS `cad_gestor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_gestor` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dths` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mci` varchar(9) NOT NULL,
  `nome` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_lojabb`
--

DROP TABLE IF EXISTS `cad_lojabb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_lojabb` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `convenio` int(10) unsigned NOT NULL DEFAULT '0',
  `cnpj` varchar(25) DEFAULT NULL,
  `mci` varchar(9) DEFAULT NULL,
  `agencia` varchar(6) DEFAULT NULL,
  `lojabb` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(100) DEFAULT NULL,
  `skype` varchar(100) DEFAULT NULL,
  `regional` varchar(100) DEFAULT NULL,
  `inauguracao` date DEFAULT NULL,
  `tipo_loja` varchar(30) DEFAULT NULL,
  `qt_pdv` int(11) DEFAULT NULL,
  `sao` enum('Sim','Nao') DEFAULT NULL,
  `turno` enum('Sim','Nao') DEFAULT NULL,
  `telefone` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `logradouro` varchar(150) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cep` varchar(12) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  `seg_pat` enum('Sim','Nao') DEFAULT NULL,
  `seg_ala` enum('Sim','Nao') DEFAULT NULL,
  `cftv` enum('Sim','Nao') DEFAULT NULL,
  `cofre_int` enum('Sim','Nao') DEFAULT NULL,
  `cofre_boc` enum('Sim','Nao') DEFAULT NULL,
  `cofre_adm` enum('Sim','Nao') DEFAULT NULL,
  `limite_receb` decimal(10,2) DEFAULT NULL,
  `limite_cofre_boc` decimal(10,2) DEFAULT NULL,
  `limite_cofre_adm` decimal(10,2) DEFAULT NULL,
  `cofre_obs` varchar(240) DEFAULT NULL,
  `hora_exp` varchar(20) DEFAULT NULL,
  `oper_sabado` enum('Sim','Nao') DEFAULT NULL,
  `hora_exp_sab` varchar(20) DEFAULT NULL,
  `transp_valor` varchar(50) DEFAULT NULL,
  `transp_regional` varchar(50) DEFAULT NULL,
  `transp_email` varchar(150) DEFAULT NULL,
  `transp_telefone` varchar(100) DEFAULT NULL,
  `transp_obs` longtext,
  `transp_hr_aliv` varchar(16) DEFAULT NULL,
  `agencia_obs` longtext,
  `observacoes` longtext,
  `transp_hr_alivseg` varchar(16) DEFAULT NULL,
  `transp_hr_alivsab` varchar(16) DEFAULT NULL,
  KEY `Index_3` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_modulo`
--

DROP TABLE IF EXISTS `cad_modulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_modulo` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `versao` varchar(10) NOT NULL DEFAULT '',
  `nome` varchar(45) NOT NULL DEFAULT '',
  `baixar` tinyint(1) NOT NULL DEFAULT '1',
  `overwrite` tinyint(1) NOT NULL DEFAULT '0',
  `path` varchar(100) NOT NULL DEFAULT 'C:/orbistec/',
  `descricao` longtext,
  `processo` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_produto`
--

DROP TABLE IF EXISTS `cad_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_produto` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `produto_bb` int(11) NOT NULL DEFAULT '0' COMMENT 'Produto BB;;;;',
  `tipo` varchar(2) NOT NULL DEFAULT '' COMMENT 'Tipo;;;;',
  `convenio` varchar(5) DEFAULT NULL COMMENT 'Cod.Convenio;;;;',
  `segmento` varchar(2) DEFAULT NULL COMMENT 'Segmento;;;;',
  `nome` varchar(50) NOT NULL DEFAULT '' COMMENT 'Nome;;;;',
  `vlr_fixo` int(11) DEFAULT NULL,
  `vlr_r` decimal(10,4) DEFAULT NULL,
  `vlr_p` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `NewIndex` (`produto_bb`,`convenio`,`segmento`,`tipo`,`codigo`),
  KEY `Index_BB` (`produto_bb`),
  KEY `Index_4` (`convenio`,`produto_bb`,`tipo`,`segmento`)
) ENGINE=InnoDB AUTO_INCREMENT=1330 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_relat_email`
--

DROP TABLE IF EXISTS `cad_relat_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_relat_email` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `para` varchar(200) DEFAULT NULL COMMENT 'Para; Informe e-mail.;',
  `assunto` varchar(100) DEFAULT NULL COMMENT 'Assunto;;;;',
  `corpo` longtext NOT NULL COMMENT 'Corpo de E-mail;;;;',
  `sql_` longtext COMMENT 'Sql de Consulta;;;;',
  `horas` varchar(100) NOT NULL DEFAULT ';all;' COMMENT 'Horario de Envio;;;;',
  `status_` enum('Ativo','Inativo') DEFAULT NULL COMMENT 'Estatus;;;;',
  `usuario` varchar(5) DEFAULT NULL,
  `dia` varchar(90) NOT NULL DEFAULT ';all;' COMMENT 'Dias de Envio;;;;',
  `semana` varchar(90) NOT NULL DEFAULT ';Sun;Mon;Tue;Wed;Thu;Fri;Sat;' COMMENT 'Dias da Semana;;;;',
  `banco` varchar(15) NOT NULL DEFAULT 'bdsistema' COMMENT 'Banco de Dados;;;;',
  `de` varchar(90) NOT NULL DEFAULT 'Orbistec - Sistema<sistemas@orbistec.com.br>' COMMENT 'E-mail Origem;;;;',
  `anexo` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_setor`
--

DROP TABLE IF EXISTS `cad_setor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_setor` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_setup`
--

DROP TABLE IF EXISTS `cad_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_setup` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL DEFAULT '',
  `descricao` longtext,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_sql`
--

DROP TABLE IF EXISTS `cad_sql`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_sql` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dths` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descricao` varchar(45) NOT NULL DEFAULT '' COMMENT 'Descricao;;;;',
  `sql_` longtext NOT NULL COMMENT 'Sql;;;;',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cad_urlfire`
--

DROP TABLE IF EXISTS `cad_urlfire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cad_urlfire` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` varchar(12) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `descricao` varchar(30) DEFAULT NULL,
  `st` varchar(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Index_2` (`mac`,`url`,`st`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cbf801_dia`
--

DROP TABLE IF EXISTS `cbf801_dia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cbf801_dia` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `gestor` int(1) DEFAULT '0',
  `mci` int(10) DEFAULT '0',
  `lojabb` int(4) DEFAULT NULL,
  `statusbb` int(1) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `quant` int(7) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `UNICO` (`gestor`,`mci`,`lojabb`,`statusbb`,`data`) USING BTREE,
  UNIQUE KEY `key_coban` (`mci`,`data`,`gestor`,`lojabb`,`statusbb`),
  KEY `Index_4` (`mci`,`data`)
) ENGINE=InnoDB AUTO_INCREMENT=19113 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cbf801full`
--

DROP TABLE IF EXISTS `cbf801full`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cbf801full` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `mci` int(9) NOT NULL DEFAULT '0',
  `dt_trans` date NOT NULL DEFAULT '0000-00-00',
  `dt_movim` date NOT NULL DEFAULT '0000-00-00',
  `dh_operacao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `agencia` int(4) NOT NULL DEFAULT '0',
  `chavej` varchar(8) NOT NULL DEFAULT '0',
  `nsu` int(4) NOT NULL DEFAULT '0',
  `produto` varchar(3) NOT NULL DEFAULT '',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `lojabb` int(4) NOT NULL DEFAULT '0',
  `pdvbb` int(4) NOT NULL DEFAULT '0',
  `liquidacao` int(2) NOT NULL DEFAULT '0',
  `statusbb` int(3) NOT NULL DEFAULT '0',
  `statuscb` int(5) NOT NULL DEFAULT '0',
  `gestor` int(1) NOT NULL DEFAULT '0',
  `cdbarra` varchar(44) NOT NULL DEFAULT '',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `key_coban` (`dt_movim`,`mci`,`chavej`,`nsu`,`valor`),
  KEY `Key_ind` (`mci`,`dt_trans`,`dt_movim`),
  KEY `Index_4` (`mci`,`dt_trans`),
  KEY `Index_5` (`mci`,`lojabb`,`dt_trans`) USING BTREE,
  KEY `agrupDia` (`mci`,`gestor`,`dt_movim`,`lojabb`,`statusbb`)
) ENGINE=InnoDB AUTO_INCREMENT=17872730 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cbf801repasse`
--

DROP TABLE IF EXISTS `cbf801repasse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cbf801repasse` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  `dthr` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mci` int(9) NOT NULL DEFAULT '0',
  `lojabb` int(4) NOT NULL DEFAULT '0',
  `agencia` int(4) NOT NULL DEFAULT '0',
  `nsu` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `NewIndex` (`mci`,`lojabb`,`data`,`nsu`,`valor`),
  KEY `Key_coban` (`mci`,`lojabb`,`data`)
) ENGINE=InnoDB AUTO_INCREMENT=310666 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuracoes`
--

DROP TABLE IF EXISTS `configuracoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuracoes` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) DEFAULT NULL,
  `valor` varchar(50) DEFAULT NULL,
  `campo` longtext,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dif_caixa`
--

DROP TABLE IF EXISTS `dif_caixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dif_caixa` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `local` varchar(15) DEFAULT NULL,
  `lojabb` varchar(6) DEFAULT NULL,
  `data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `datacx` date NOT NULL DEFAULT '0000-00-00',
  `tipo` varchar(7) NOT NULL DEFAULT '',
  `motivo` varchar(50) NOT NULL DEFAULT '',
  `obs` longtext NOT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `status` varchar(30) NOT NULL DEFAULT '',
  `arquivo` enum('s','n') NOT NULL DEFAULT 'n',
  `origem` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Index_2` (`local`,`lojabb`,`tipo`,`datacx`,`valor`,`data`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=22278 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `financeiro`
--

DROP TABLE IF EXISTS `financeiro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `financeiro` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `local` varchar(15) DEFAULT NULL,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `saldo_in` decimal(10,2) DEFAULT NULL,
  `rec` decimal(10,2) DEFAULT NULL,
  `pag` decimal(10,2) DEFAULT NULL,
  `saldo_fn` decimal(10,2) DEFAULT NULL,
  `std` varchar(1) NOT NULL DEFAULT 'A',
  `dh_fec` datetime DEFAULT NULL,
  `retirada` decimal(10,2) DEFAULT NULL,
  `datacx` date NOT NULL DEFAULT '0000-00-00',
  `dh_abe` datetime DEFAULT NULL,
  `lojabb` int(11) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Index_2` (`local`,`datacx`,`lojabb`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=139829 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `financeiro_lj`
--

DROP TABLE IF EXISTS `financeiro_lj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `financeiro_lj` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `std_cx` varchar(25) NOT NULL DEFAULT 'A',
  `lojabb` int(11) DEFAULT NULL,
  `gerente` varchar(60) NOT NULL DEFAULT '',
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datacx` date NOT NULL DEFAULT '0000-00-00',
  `saldo_in` decimal(10,2) DEFAULT NULL,
  `rec` decimal(10,2) DEFAULT NULL,
  `pag` decimal(10,2) DEFAULT NULL,
  `alivio` decimal(10,2) DEFAULT NULL,
  `saldo_parcial` decimal(10,2) DEFAULT NULL,
  `tot_gtv` decimal(10,2) DEFAULT NULL,
  `obs_gtv` varchar(200) NOT NULL DEFAULT '',
  `tot_difcxmenor` decimal(10,2) DEFAULT NULL,
  `obs_difcxmenor` varchar(200) NOT NULL DEFAULT '',
  `saldo_loja` decimal(10,2) DEFAULT NULL,
  `saldo_pdv` decimal(10,2) DEFAULT NULL,
  `saldo_comp` decimal(10,2) DEFAULT NULL,
  `saldo_cofreadm` decimal(10,2) DEFAULT NULL,
  `saldo_bocadelobo` decimal(10,2) DEFAULT NULL,
  `obs` varchar(200) DEFAULT NULL,
  `obs_conf` longtext,
  `tot_difcxmaior` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Index_2` (`lojabb`,`datacx`)
) ENGINE=InnoDB AUTO_INCREMENT=6418 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `financeiro_lj_gtv`
--

DROP TABLE IF EXISTS `financeiro_lj_gtv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `financeiro_lj_gtv` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lojabb` int(11) DEFAULT NULL,
  `datacx` date NOT NULL DEFAULT '0000-00-00',
  `total_gtv` decimal(10,2) DEFAULT NULL,
  `valor_apurado` decimal(10,2) DEFAULT NULL,
  `diferenca` decimal(10,2) DEFAULT NULL,
  `deposito_agencia` decimal(10,2) DEFAULT NULL,
  `deposito_cso` decimal(10,2) DEFAULT NULL,
  `residuo` decimal(10,2) DEFAULT NULL,
  `alivio` decimal(10,2) DEFAULT NULL,
  `diferenca_alivio` decimal(10,2) DEFAULT NULL,
  `observacao` varchar(200) DEFAULT NULL,
  `std` varchar(20) DEFAULT NULL,
  `protocolo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Index_2` (`lojabb`,`datacx`)
) ENGINE=InnoDB AUTO_INCREMENT=1556 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `financeiro_mov`
--

DROP TABLE IF EXISTS `financeiro_mov`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `financeiro_mov` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cred` decimal(10,2) DEFAULT '0.00',
  `deb` decimal(10,2) DEFAULT '0.00',
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `chavej` varchar(8) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT '0.00',
  `tipo` varchar(20) DEFAULT NULL,
  `val` char(1) DEFAULT NULL,
  `n_doc` varchar(40) DEFAULT NULL,
  `loja` varchar(6) DEFAULT NULL,
  `aux` varchar(31) DEFAULT NULL,
  `datacx` date DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `unico` (`chavej`,`data`)
) ENGINE=InnoDB AUTO_INCREMENT=364064 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hist_retaguarda`
--

DROP TABLE IF EXISTS `hist_retaguarda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hist_retaguarda` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario` varchar(15) DEFAULT NULL,
  `funcao` varchar(15) DEFAULT NULL,
  `descricao` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=21852 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `imp_arrec`
--

DROP TABLE IF EXISTS `imp_arrec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imp_arrec` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quant` int(11) DEFAULT NULL,
  `valor` int(11) DEFAULT NULL,
  `data_arq` date DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Index_3` (`nome`,`valor`),
  KEY `NewIndex` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=681 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_controle`
--

DROP TABLE IF EXISTS `m_controle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_controle` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `codigo_l` int(11) DEFAULT NULL,
  `codigo_u` int(11) DEFAULT NULL,
  `stat` char(1) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=10024 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_controle_func`
--

DROP TABLE IF EXISTS `m_controle_func`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_controle_func` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `codigo_l` int(11) DEFAULT NULL,
  `codigo_f` int(11) DEFAULT NULL,
  `stat` char(1) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_link`
--

DROP TABLE IF EXISTS `m_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_link` (
  `codigo_l` mediumint(9) NOT NULL AUTO_INCREMENT,
  `codigo_m` int(11) DEFAULT NULL,
  `codigo_s` int(11) DEFAULT NULL,
  `descricao_l` varchar(60) DEFAULT NULL,
  `arquivo` varchar(50) DEFAULT 'url',
  `status` char(1) DEFAULT 's',
  `link_ini` varchar(100) DEFAULT 'FrameAdministracao.asp?url=',
  `link` varchar(60) DEFAULT '&SigRede=var',
  `fuc_link` varchar(80) DEFAULT 'target="_top"',
  `arq_aspx` varchar(25) DEFAULT NULL,
  `tipo` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo_l`),
  KEY `NewIndex` (`codigo_l`,`codigo_m`,`codigo_s`)
) ENGINE=InnoDB AUTO_INCREMENT=213 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_menu`
--

DROP TABLE IF EXISTS `m_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_menu` (
  `codigo_m` mediumint(9) NOT NULL AUTO_INCREMENT,
  `descricao_m` varchar(25) DEFAULT NULL,
  `status` varchar(1) NOT NULL DEFAULT 's',
  PRIMARY KEY (`codigo_m`),
  KEY `NewIndex` (`codigo_m`,`descricao_m`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_submenu`
--

DROP TABLE IF EXISTS `m_submenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_submenu` (
  `codigo_s` mediumint(9) NOT NULL AUTO_INCREMENT,
  `codigo_m` int(11) DEFAULT NULL,
  `descricao_s` varchar(26) DEFAULT NULL,
  PRIMARY KEY (`codigo_s`),
  KEY `NewIndex` (`codigo_m`,`codigo_s`,`descricao_s`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mov_cc`
--

DROP TABLE IF EXISTS `mov_cc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mov_cc` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `chaveJ` varchar(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `tipo` int(3) DEFAULT NULL,
  `agencia` int(5) DEFAULT NULL,
  `conta` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `key_uni` (`chaveJ`,`data`,`conta`)
) ENGINE=InnoDB AUTO_INCREMENT=3052 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mov_chavej`
--

DROP TABLE IF EXISTS `mov_chavej`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mov_chavej` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chavej` varchar(15) DEFAULT NULL,
  `data` date NOT NULL DEFAULT '0000-00-00',
  `valor` decimal(10,2) DEFAULT NULL,
  `qt_ope` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `unico` (`chavej`,`data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mov_emprestimo`
--

DROP TABLE IF EXISTS `mov_emprestimo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mov_emprestimo` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `chaveJ` varchar(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `movimento` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tipo` int(3) DEFAULT NULL,
  `registro` varchar(90) DEFAULT NULL,
  `parcelas` int(11) DEFAULT NULL,
  `linha` varchar(4) DEFAULT NULL,
  `convenio` int(11) DEFAULT NULL,
  `lib` int(11) DEFAULT NULL,
  `proposta` int(11) DEFAULT NULL,
  `troco` decimal(10,2) DEFAULT NULL,
  `canal` int(11) DEFAULT '1',
  `promotora` int(11) DEFAULT '0',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `key_uni` (`chaveJ`,`data`,`registro`,`tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movim_exc`
--

DROP TABLE IF EXISTS `movim_exc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movim_exc` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `data_movimento` date NOT NULL DEFAULT '0000-00-00',
  `coban` int(6) NOT NULL DEFAULT '0',
  `nsu` int(7) NOT NULL DEFAULT '0',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tipo` char(2) DEFAULT NULL,
  `usuario` int(3) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Index_2` (`data_movimento`,`coban`,`nsu`)
) ENGINE=InnoDB AUTO_INCREMENT=47774 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimfull`
--

DROP TABLE IF EXISTS `movimfull`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimfull` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `dt_trans` date DEFAULT NULL,
  `dt_movim` date DEFAULT NULL,
  `dh_operacao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `coban` varchar(8) DEFAULT '0',
  `terminal` varchar(8) DEFAULT '0',
  `aut` int(7) NOT NULL DEFAULT '0',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tipo` varchar(2) DEFAULT 'CV',
  `produto` int(4) NOT NULL DEFAULT '0',
  `convenio` int(4) NOT NULL DEFAULT '0',
  `seg` varchar(1) DEFAULT '0',
  `chbb` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `key_coban` (`dt_movim`,`coban`,`terminal`,`aut`,`tipo`,`produto`,`valor`),
  KEY `Key_ind` (`coban`,`dt_trans`,`dt_movim`),
  KEY `Index_4` (`coban`,`dt_trans`)
) ENGINE=InnoDB AUTO_INCREMENT=17872460 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movimtemp`
--

DROP TABLE IF EXISTS `movimtemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimtemp` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `dt_trans` date DEFAULT NULL,
  `dt_movim` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `coban` varchar(8) DEFAULT '0',
  `terminal` varchar(8) DEFAULT '0',
  `aut` int(7) NOT NULL DEFAULT '0',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tipo` varchar(2) DEFAULT 'CV',
  `produto` int(4) NOT NULL DEFAULT '0',
  `convenio` int(4) NOT NULL DEFAULT '0',
  `seg` varchar(1) DEFAULT '0',
  `chbb` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `key_coban` (`dt_movim`,`coban`,`terminal`,`aut`,`tipo`,`produto`,`valor`),
  KEY `Key_ind` (`coban`,`dt_trans`,`dt_movim`),
  KEY `Index_4` (`coban`,`dt_trans`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rel_gerencial`
--

DROP TABLE IF EXISTS `rel_gerencial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_gerencial` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dths` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome` varchar(20) NOT NULL DEFAULT '',
  `data` date NOT NULL DEFAULT '0000-00-00',
  `valor` decimal(10,2) DEFAULT NULL,
  `qt` int(11) DEFAULT NULL,
  `perc` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Index_2` (`nome`,`data`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=8210 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rel_solic_segund`
--

DROP TABLE IF EXISTS `rel_solic_segund`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_solic_segund` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dths` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data` date NOT NULL DEFAULT '0000-00-00',
  `solic` int(11) DEFAULT NULL,
  `autor` int(11) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Index_2` (`data`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repasse`
--

DROP TABLE IF EXISTS `repasse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repasse` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `seq` int(11) DEFAULT NULL,
  `coban` int(6) NOT NULL DEFAULT '0',
  `nrboleto` int(6) DEFAULT NULL,
  `banco` int(4) DEFAULT NULL,
  `agencia` int(6) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `NewIndex` (`coban`,`data`,`seq`,`valor`),
  KEY `Key_coban` (`coban`,`data`)
) ENGINE=InnoDB AUTO_INCREMENT=1194 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repasse_bb`
--

DROP TABLE IF EXISTS `repasse_bb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repasse_bb` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `convenio` int(6) NOT NULL DEFAULT '0',
  `data` date DEFAULT NULL,
  `alivio` decimal(10,2) DEFAULT NULL,
  `saldo` decimal(10,2) DEFAULT NULL,
  `datahora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `arrec` decimal(10,2) DEFAULT NULL,
  `lojabb` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `NewIndex` (`convenio`,`data`,`alivio`,`saldo`,`arrec`,`lojabb`),
  KEY `Key_coban` (`convenio`,`data`,`alivio`,`saldo`),
  KEY `Index_4` (`convenio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `status_mac`
--

DROP TABLE IF EXISTS `status_mac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_mac` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dths` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mac` varchar(12) NOT NULL DEFAULT '',
  `descricao` varchar(45) NOT NULL DEFAULT '',
  `convenio` int(11) DEFAULT NULL,
  `loja` int(11) DEFAULT NULL,
  `pdv` int(11) DEFAULT NULL,
  `chavej` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE KEY `Index_2` (`mac`,`chavej`)
) ENGINE=InnoDB AUTO_INCREMENT=4909359 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `teste_tmp`
--

DROP TABLE IF EXISTS `teste_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teste_tmp` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `chavej` varchar(8) NOT NULL DEFAULT '0',
  `obs` varchar(250) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=3530230 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tot_operador`
--

DROP TABLE IF EXISTS `tot_operador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tot_operador` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `convenio` int(6) DEFAULT NULL,
  `lojabb` int(4) DEFAULT NULL,
  `pdvbb` int(4) DEFAULT NULL,
  `chavej` varchar(8) DEFAULT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rec` decimal(10,2) DEFAULT NULL,
  `tot_rec` int(11) DEFAULT NULL,
  `pag` decimal(10,2) DEFAULT NULL,
  `tot_pag` int(11) DEFAULT NULL,
  `canc_rec` decimal(10,2) DEFAULT NULL,
  `canc_pag` decimal(10,2) DEFAULT NULL,
  `comprovante` longtext,
  `tot_canc_rec` int(10) unsigned DEFAULT NULL,
  `tot_canc_pag` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `Index_2` (`chavej`),
  KEY `Index_3` (`convenio`,`chavej`,`data`)
) ENGINE=InnoDB AUTO_INCREMENT=199212 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tot_posicao`
--

DROP TABLE IF EXISTS `tot_posicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tot_posicao` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `convenio` int(6) DEFAULT NULL,
  `lojabb` int(6) DEFAULT NULL,
  `pdvbb` int(6) DEFAULT NULL,
  `posicao` date DEFAULT NULL,
  `recebimento` decimal(10,2) DEFAULT NULL,
  `pagamento` decimal(10,2) DEFAULT NULL,
  `alivio` decimal(10,2) DEFAULT NULL,
  `final` decimal(10,2) DEFAULT NULL,
  `comprovante` longtext,
  `limite_enc` decimal(10,2) DEFAULT NULL,
  `limite_uti` decimal(10,2) DEFAULT NULL,
  `saldo_ini` decimal(10,2) DEFAULT NULL,
  `chavej` varchar(8) DEFAULT NULL,
  `acert_debito` decimal(10,2) DEFAULT NULL,
  `acert_credito` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `Index_2` (`data`),
  KEY `Index_3` (`codigo`),
  KEY `Index_4` (`lojabb`),
  KEY `Index_5` (`data`,`lojabb`,`convenio`,`chavej`)
) ENGINE=InnoDB AUTO_INCREMENT=147523 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `loguin` varchar(10) DEFAULT NULL,
  `senha` varchar(10) DEFAULT NULL,
  `matricula` varchar(15) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `alt_senha` char(1) DEFAULT NULL,
  `email` varchar(1550) DEFAULT NULL,
  `status` char(10) NOT NULL DEFAULT 'Ativo',
  `setor` varchar(11) DEFAULT NULL,
  `funcao` varchar(40) DEFAULT '0',
  `acesso` varchar(20) DEFAULT 'consultor',
  `regional` int(2) DEFAULT '0',
  `menu` char(1) DEFAULT 'f',
  `tipo` char(1) DEFAULT 'a',
  `luser` varchar(15) DEFAULT NULL,
  `lsenha` varchar(15) DEFAULT NULL,
  `incluir` char(1) DEFAULT 'n',
  `alterar` char(1) DEFAULT 'n',
  `excluir` char(1) DEFAULT 'n',
  `user` varchar(4) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `data_nasc` date DEFAULT NULL,
  `chavej` varchar(8) DEFAULT '',
  `fusoHorario` varchar(3) DEFAULT '0',
  `bloq` enum('s','n') DEFAULT 'n',
  `mostrar` varchar(10) DEFAULT NULL,
  `bloqRet` tinyint(1) NOT NULL DEFAULT '0',
  `ag` varchar(6) DEFAULT NULL,
  `cc` varchar(8) DEFAULT NULL,
  `carga_hor_sem` time DEFAULT NULL,
  `carga_hor_sab` time DEFAULT NULL,
  `cp` date NOT NULL DEFAULT '2013-01-01',
  `pis` varchar(45) DEFAULT NULL,
  `emailComp` varchar(10) NOT NULL DEFAULT 'PADRAO',
  PRIMARY KEY (`codigo`,`nome`),
  KEY `NewIndex` (`codigo`,`regional`),
  KEY `Index_3` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=369 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario_operador`
--

DROP TABLE IF EXISTS `usuario_operador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_operador` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `loguin` varchar(40) DEFAULT NULL,
  `senha` varchar(20) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `alt_senha` char(1) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `status` char(10) DEFAULT NULL,
  `convenio` int(6) DEFAULT NULL,
  `lojabb` int(6) DEFAULT NULL,
  `chavej` varchar(8) DEFAULT '',
  `limite_gaveta` decimal(10,2) DEFAULT NULL,
  `status_caixa` varchar(45) DEFAULT NULL COMMENT 'Normal, Bloqueado, ',
  `limite_cxfechamento` decimal(10,2) NOT NULL DEFAULT '0.00',
  `bloqueio` enum('true','false') NOT NULL DEFAULT 'false',
  `funcao` int(11) DEFAULT NULL,
  PRIMARY KEY (`codigo`,`nome`),
  UNIQUE KEY `Index_4` (`chavej`,`lojabb`),
  KEY `Index_3` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=1254 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario_sis`
--

DROP TABLE IF EXISTS `usuario_sis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_sis` (
  `codigo` mediumint(9) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `loguin` varchar(10) DEFAULT NULL,
  `senha` varchar(10) DEFAULT NULL,
  `matricula` varchar(15) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `alt_senha` char(1) DEFAULT NULL,
  `email` varchar(240) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `setor` varchar(11) DEFAULT NULL,
  `funcao` varchar(40) DEFAULT '0',
  `acesso` varchar(20) DEFAULT 'consultor',
  `regional` varchar(40) DEFAULT '0',
  `menu` char(1) DEFAULT 'f',
  `tipo` char(1) DEFAULT 'a',
  `luser` varchar(15) DEFAULT NULL,
  `lsenha` varchar(15) DEFAULT NULL,
  `incluir` char(1) DEFAULT 'n',
  `alterar` char(1) DEFAULT 'n',
  `excluir` char(1) DEFAULT 'n',
  `senha_int` varchar(10) DEFAULT NULL,
  `alt_tabela` char(1) NOT NULL DEFAULT 'n',
  `sol_master` char(1) NOT NULL DEFAULT 'n',
  `solicitacao` varchar(60) DEFAULT NULL,
  `ip` varchar(45) DEFAULT 'Livre',
  `convenio` varchar(5) DEFAULT NULL,
  `loja` int(11) DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `NewIndex` (`codigo`,`regional`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-28 17:35:47
